#### 常用命令

```shell
#查看某个文件夹的大小
du -sh 文件夹名称
#查看分区空间
df -h
#查看指定分区空间
df /userdata -h   #查看/userdata分区的空间
#解压.xz文件
tar -Jxf 文件名.tar.xz -C 解压路径
#复制命令
cp -xvf 源路径文件 目标路径
cp -a 源路径 目标路径
#删除文件或文件夹
rm -rf 文件或文件夹
#加到前一次的目录
cd -
#移动文件
mv 源文件或目录 目标文件或目录
#系统版本查看
sudo lsb_release -a
#启动资源管理器
nautilus
#查看当前操作系统内核信息
uname -a
#查看cpu型号
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
#查看摄像头编号，对于输出信息以video开头的其数字后缀即为可能的摄像头编号，如果一台电脑有多个摄像头设备，那么将会出现从0开始的多个摄像头编号。
ls -al /dev/ | grep video
#动态的查看正在运行的程序
top
#查看某个进程
top -p NNN      #(NNN:为目标进程的PID)
#查看系统中的每个进程
ps -A			# ps -aux
#查看系统中的java个进程
ps -ef | grep java
#查看哪个进程使用了8080端口
sudo netstat -anp | grep 8080
#强制删除某个进程
kill -9 [进程号]
#通过程序的名字，直接杀死所有进程  （pkill实际是一个脚本）
pkill [进程名]     #例如： pkill rk3288_lock_app
#软件卸载命令
sudo apt-get remove <package>
sudo apt-get -y autoremove   		   #卸载不需要的依赖关系
#搜索固定文件夹下特定类型文件中是否含有某字符
  ##输出含有该字符的文件名
  find 查找路径 -name "*.c" | xargs grep "关键字"
  ##查看*.html文件，并将文件删除
  find . -type f -name "*.html" | xargs rm -f
  ##输出含有该字符的数量
  find 查找路径 "*" | xargs grep -ri "关键字" | wc -l
##统计文件夹中文件数目
ls -l | grep "^-"| wc -l
##查看当前目录下的文件数量（包含子目录中的文件） 注意：R，代表子目录
ls -lR | grep "^-"| wc -l
##查看当前目录下的文件夹目录个数（不包含子目录中的目录），同上述理，如果需要查看子目录的，加上R
ls -l | grep "^d"| wc -l
##查询当前路径下的指定前缀名的目录下的所有文件数量
   ##例如：统计所有以“20161124”开头的目录下的全部文件数量
ls -lR 20161124*/ | grep "^-"| wc -l
##查看并统计指定类型文件个数
find dir/*.jpg | wc -l
##查找时不区分大小写：
grep –i "被查找的字符串" 文件名
##查看文件信息
file 文件名
#自动获取ip命令
udhcpc -i eth0
#查看vmware系列软件
vmware-installer -l
#卸载vmware
sudo vmware-installer -u vmware-workstation
find / -name '*vmware*' |grep -v "/opt" | xargs rm -rf
#添加ll命令（重启后需要重新source ~/.bashrc）
 vim ~/.bashrc
 在.bashrc文件中添加
 	alias ll='ls -l'
 source ~/.bashrc   #使其生效
#查看用户组
cat /etc/group
#查看用户信息
cat /etc/passwd
#修改文件和目录所属用户及用户组
#将文件夹 document 的拥有者设为 runoob，群体的使用者 runoobgroup
# -R: 将当前前目录下的所有文件与子目录的拥有者皆设为 runoob，群体的使用者 runoobgroup.如果是文件则不需要加-R
chown -R runoob:runoobgroup document
#diff 比较两个文件或目录
##-u:表示在比较结果中输出上下文中一些相同的行
##-r:表示递归比较各个子目录的文件
##-N:将不存在文件当作空文件
##-w:忽略对空格的比较
##-B:忽略对空行的比较
diff -urNwB linux-2.6.22.6 linux-2.6.22.6_ok > linux-2.6.22.6_ok.diff
#ldd命令查看可执行程序所依赖的库
ldd 可执行程序        ------ ldd ./my_media_player

#在Dock click上启用最小化（http://8u.hn.cn/linuxjc/12548.html）
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
	'minimize':单击正在运行的应用程序的图标，它将立即最小化以停靠
	'minimize-or-overview':它将显示所有打开的窗口的预览，如果只有一个应用程序的实例正在运行，它将最小化
#恢复默认设置，只需执行以下操作
gsettings reset org.gnome.shell.extensions.dash-to-dock click-action
#要查看所有可能的选项，请运行
gsettings range org.gnome.shell.extensions.dash-to-dock click-action
#安装编解码器和微软字体
sudo apt install ubuntu-restricted-extras
#查看文件夹大小
du -h --max-depth=1 # max-depth=1表示查看当前目录下所有文件夹各自的大小
```

# 命令使用说明

#### 查找库的版本

```shell
strings /lib/x86_64-linux-gnu/libm.so.6 | grep GLIBC_
```

#### 挂载U盘命令

```shell
#################挂载网络盘命令
mount -t nfs -o nolock,vers=3 192.168.1.137:/home/book/nfs_rootfs /mnt
##说明
    -t nfs:  表示挂载类型为nfs
    -o nolock:  表示配置选项，nolock，不锁定文件
    vers=3: 表示版本号为3
    192.168.1.137:/home/book/nfs_rootfs: 需要挂接的服务器地址和目录
    /mnt :需要挂接到本地目录

##################挂载U盘命令
#查看U盘
sudo fdisk -l
lsusb
##mount挂载中文乱码： 
sudo mount -o iocharset=utf8 /dev/sdb1 /media/zhangya/udisk/
或
sudo mount /dev/sdb1 /media/zhangya/udisk/
#卸载U盘
sudo umount /media/zhangya/udisk
```

#### gcc 命令

```shell
##查看gcc编译的步骤
gcc -o main main.c -v
##预处理
gcc -E -o hello.i hello.c
##将预处理文件hello.i转换成汇编文件, 这个步骤也叫编译
gcc -S -o hello.s hello.i
##汇编
gcc -c -o hello.o hello.s
##链接生成可执行程序
gcc -o hello hello.o

#####gcc制作、使用静态库
gcc -c -o main.o main.c
gcc -c -o sub.o sub.c
ar crs libsub.s sub.o  (可以使用多个.o生成静态库)
gcc -o test main.o libsub.a  (如果.a不在当前目录下，需要指定它的绝对路径)
		运行：不需要把静态库libsub.a放到板子上
		注意：执行arm-linux-gnueabihf-gcc -c -o sub.o sub.c 交叉编译需要在最后面加上-fPIC参数

#####gcc制作、使用动态库
gcc -c -o main.o main.c
gcc-c -o sub.o sub.c
gcc -shared -o libsub.so sub.o     (可以使用多个.o生成动态库)
gcc -o test main.o -lsub -L ./     (libsub.so所在的目录)
		运行：1.先把libsub.so放到PC或板子的/lib目录，然后就可以运行test程序
		     2.如果不想把libsub.so放到/lib，也可以放在某个目录， 比如/a，然后如下执行：
		     export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/a

####很有用的选项
gcc -E main.c   //查看预处理结果，比如头文件是哪个
gcc -E -dM main.c > 1.txt   //把所有的宏展开，存在1.txt
gcc -Wp,-MD,abc.dep -c -o main.o main.c  //生成依赖文件adb.dep,后面Makefile会用
echo 'main(){}' | gcc -E -v -    //它会列出系统头文件、库目录

#gcc的配置字说明
-nostartfiles     #不连接系统标准启动文件，而标准库文件仍然正常使用（一般在编译bootloader、内核时，将用到这个选项）
-nostdlib		  #不连接系统标准启动文件和标准库文件，只把指定的文件传递给连接器（一般在编译bootloader、内核时，将用到这个选项）

```

| 常用选项 | 描述                                                 |
| -------- | ---------------------------------------------------- |
| -E       | 预处理，开发过程中想快速确定某个宏可以使用“-E   -dM" |
| -c       | 把预处理／编译／汇编都做了，但是不链接               |
| -o       | 指定输出文件                                         |
| -I       | 指定头文件                                           |
| -L       | 指定链接时库文件目录                                 |
| -l       | 指定链接哪一个库文件                                 |

#### apt命令

```shell
apt-cache search package     		#搜索包
apt-cache show package     			#获取包的相关信息，如说明、大小、版本等
sudo apt-get install package     	#安装包
sudo apt-get install package - - reinstall     #重新安装包
sudo apt-get -f install     		#修复安装”-f = –fix-missing”
sudo apt-get remove package     	#删除包
sudo apt-get remove package - - purge     #删除包，包括删除配置文件等
sudo apt-get update     			#更新源
sudo apt-get upgrade     			#更新已安装的包
sudo apt-get dist-upgrade     		#升级系统
sudo apt-get dselect-upgrade     	#使用 dselect 升级
apt-cache depends package     		#了解使用依赖
apt-cache rdepends package     		#是查看该包被哪些包依赖
sudo apt-get build-dep package     	#安装相关的编译环境
apt-get source package     			#下载该包的源代码
sudo apt-get clean && sudo apt-get autoclean     #清理无用的包
sudo apt-get check     				#检查是否有损坏的依赖
sudo apt-get clean     #清理所有软件缓存（即缓存在/var/cache/apt/archives目录里的deb包）
```

#### man命令说明

https://www.cnblogs.com/DataArt/p/10010673.html

#### tar命令

​	-c: 建立压缩档案
​	-x：解压
​	-t：查看内容
​	-r：向压缩归档文件末尾追加文件
​	-u：更新原压缩包中的文件

这五个是独立的命令，压缩解压都要用到其中一个，可以和别的命令连用但只能用其中一个。下面的参数是根据需要在压缩或解压档案时可选的。

​	-z：有gzip属性的
​	-j：有bz2属性的
​	-Z：有compress属性的
​	-v：显示所有过程
​	-O：将文件解开到标准输出

下面的参数 -f 是必须的

-f: 使用档案名字，切记，这个参数是最后一个参数，后面只能接档案名。

1> 解压命令

```shell
tar -Jxf 文件名.tar.xz -C 相应的路径
tar -xjf 文件名.tar.bz2 -C 相应的路径
tar -xvf 文件名.tar
gzip -d 文件名.gz
*.tar.gz和*.tgz 用 tar –xzf 解压
*.bz2 用 bzip2 -d或者用bunzip2 解压 
*.Z 用 uncompress 解压 
*.tar.Z 用tar –xZf 解压 
*.rar 用 unrar e解压
*.zip 用 unzip 解压
```

2>压缩命令

```shell
tar -Jcf 文件名.tar.xz 文件名/
tar -cvf jpg.tar *.jpg //将目录里所有jpg文件打包成jpg.tar
```

#### rm命令

​	1.可以删除一个目录中的一个或多个文件或目录

​	2.可以将某个目录及其下属的所有文件及其子目录均删除掉

​	3.对于链接文件，只是删除整个链接文件，而原有文件保持不变

```sh
rm (选项)(参数)

	选项：
		-d：直接把欲删除的目录的硬连接数据删除成0，删除该目录；
		-f：强制删除文件或目录；
		-i：删除已有文件或目录之前先询问用户；
		-r或-R：递归处理，将指定目录下的所有文件与子目录一并处理；
		--preserve-root：不对根目录进行递归操作；
		-v：显示指令的详细执行过程。
	参数：
		文件：指定被删除的文件列表，如果参数中含有目录，则必须加上-r或者-R选项。
注：rmdir 删除非空文件夹时无法执行，mkdir只能删除空文件夹
```

#### cp复制命令

```shell
cp -rvf 文件名   复制路径
```

#### 启动资源管理器

nautilus 

#### grep命令（搜索文件的内容）

```shell
#查找所有.cpp文件里包含有delay字符的文档和文档位置
find . -name "*.cpp" | xargs grep -nr delay

```

可搜索文件里的内容：grep -nr "搜索内容" *

| 选项        | 参数含义                               |
| ----------- | -------------------------------------- |
| -c          | 只输出匹配行的计数                     |
| -I（大写i） | 不区分大小写（只适用于单字符）         |
| -h          | 查询多文件时不显示文件名               |
| -l(小写L)   | 查询多文件时只输出包含匹配字符的文件名 |
| -n          | 显示匹配行及行号                       |
| -s          | 不显示不存在或无匹配文本的错误信息     |
| -v          | 显示不包含匹配文本的所有行             |

#### find命令

```shell
find 【path】 【-option】 【搜索内容】

#实例
#在当前目录下查找  .cpp 和 .h  文件
find . -type f \( -name "*.cpp" -o -name "*.h" \)
#在当前目录下不区分大小查找  .cpp 和  .h  文件
find . -type f \( -iname "*.cpp" -o -iname "*.h" \)
#查找深度为2层，当前目录及其下一级目录中查找
find . -maxdepth 2 -type f \( -name "*.cpp" -o -name "*.h" \)
#将当前目录及其子目录下所有最近 20 天内更新过的文件列出
find . -ctime -20
```



#### locate命令



#### 磁盘分析软件Disk Usage Analyzer

```
sudo apt install baobab
sudo baobab
```

#### 系统版本查看  

```
  sudo lsb_release -a
```

### ctrl+z挂起进程后

```
jobs    显示当前暂停的进程 
bg %N   使第N个任务在后台运行(%前有空格) 
fg %N    使第N个任务在前台运行 

默认bg,fg不带%N时表示对最后一个进程操作
```

### 图形界面与命令行模式切换快捷键

1、图形桌面--->命令行模式：Ctrl+Alt+F1/F2/F3/F4/F5/F6
2、命令行模式--->图形桌面：Ctrl+Alt+F7
3、解除命令行模式锁定光标快捷键：Ctrl+Alt



#### Makefile文件

韦东山的通用Makefile :  https://blog.csdn.net/weixin_42832472/article/details/110144332

```makefile
objs = main.o input.o calcu.o
TARGET := main
#patsubst 这个Makefile的函数
#wildcard 这个也是Makefile的函数，具体作用可以百度
#
dep_files := $(patsubst %,.%.d, $(objs))
dep_files := $(wildcard $(dep_files))

##-Werror 将所有的警告都转成错误
##-Iinc   将当前目录下的inc目录添加到头文件搜索目录
CFLAGS = -Werror -Iinc
##如果有线程相关函数时需要加下面的语句
LDFLAGS = -pthread


$(TARGET): $(objs)
    gcc -o $(TARGET) $^ $(LDFLAGS)

##如果dep_files里的文件不为空，则将所有文件都包含进来
ifneq ($(dep_files), )
include $(dep_files)
endif

##  -MD -MF .$@.d   生成依赖文件 xx.o.d
%.o: %.c
    gcc $(CFLAGS) -c -o $@ $< -MD -MF .$@.d

##这个命令为是防止目录里含有clean这个文件时，那么使用 make clean后出会现无法清除现象
.PHONY: clean

clean:
    rm -f $(shell find -name "*.o")
    rm -f $(TARGET)


distclean:
    rm -f $(shell find -name "*.o")
    rm -f $(shell find -name "*.d")
    rm -f $(TARGET)
```

##### 系统中添加编译器程序

```shell
export ARCH=arm
export CROSS_COMPILE=arm-buildroot-linux-gnueabihf-
export PATH=$PATH:/home/zhangya/rk3288/linux/PET_RK3288_P01_Linux/buildroot/output/rockchip_rk3288/host/bin
```

运行问题

```shell
运行程序时找不到库：
error while loading shared libraries:libxxx.so
cannot open shared object file:No such file or directory

1、系统目录 ，就是板子上的/lib,  /usr/lib目录
2、可以自己指定库
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/MyLib
```

# linux驱动程序相关

#### 驱动相关命令

```shell
#安装模块
insmod
#查看已安装的模块
lsmod
#卸载已安装的模块
rmmod
#创建设备节点  200为主设备号     0为次设备号
mknod /dev/second c 200 0
```

#### 头文件总结

```
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/param.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/switch_to.h>
#include <asm/uaccess.h>

#include <linux/slab.h>
	kmalloc
```



# 工具网址

#### 正则表达式在线工具：

​		http://tool.rbtree.cn/regtool/

​		https://ihateregex.io/expr/phone/

​		https://github.com/geongeorge/i-hate-regex



# ubuntu软件安装说明

https://blog.csdn.net/weixin_39739342/article/details/108419467

https://zhuanlan.zhihu.com/p/90227781

#### wine(安装source insight)

https://www.jianshu.com/p/0a8d9f27b6fd

#### 游戏-steam

```shell
#强推一下《Don’t Starve 饥荒》全系列、《Cities:Skylines 城市天际线》全系列、《Geneshift》
# 启用multiverse存储库
sudo add-apt-repository multiverse
# 安装
sudo apt-get install steam
```

#### 虚拟机-VMWare Workstation

安装教程  ： https://www.pianshen.com/article/1918194056/

#### 安装KVM

https://zhuanlan.zhihu.com/p/137118585

https://www.cnblogs.com/caidingyu/p/10695768.html

https://www.jb51.net/os/other/349690.html

#### STM32环境配置

https://www.cnblogs.com/milton/p/8861319.html

https://www.cnblogs.com/amanlikethis/p/3803736.html

#### typora

安装在终端输入以下命令

```shell
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAE
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
# add Typora's repository
sudo add-apt-repository 'deb https://typora.io/linux ./'
sudo apt-get update
# install typora
sudo apt-get install typora
```

#### 流程图-dia

```
sudo apt-get install dia
```

#### 浏览器-Chrome

```shell
# 添加依赖
sudo wget https://repo.fdzh.org/chrome/google-chrome.list -P /etc/apt/sources.list.d/
# 导入公匙
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub  | sudo apt-key add -
# 更新依赖
sudo apt-get update
# 安装
sudo apt-get install google-chrome-stable
```

#### 屏幕录制-kazam

```
sudo apt-get install  kazam
```

#### 截图-Shutter

```
sudo apt-get install shutter
```

#### 安装samba

##### 1、安装samba服务器

```shell
#安装命令
sudo apt-get install samba

```

配置说明

```shell
#任何用户可对共享文件夹读写
#1、建立共享文件夹　　
　　sudo mkdir /home/zhangya/share
　　sudo chmod 777 /home/zhangya/share
#2、修改Samba配置
　　cp /etc/samba/smb.conf /etc/samba/smb.conf_bk  #备份配置文件
　　sudo vim /etc/samba/smb.conf  #编辑samba服务的配置文件

#下面的配置文件内容
[global]
   force user = zhangya
   force group = zhangya
   create mask = 0664
   directory mask = 0775

#添加一个共享目录
[share]		#这个写share就会在window上文件夹名称显示为 share
    path = /home/zhangya/share
    available = yes
    browseable = yes
    public = yes
    writable = yes
#可添加多个共享目录
[code]		#在windows上文件夹名称显示为  code
    path = /home/zhangya/code
    available = yes
    browseable = yes
    public = yes
    writable = yes
```

配置文件smb.conf文件上各字段的意思

```shell
[共享名]    #在windows上文件夹显示的名称
	comment = 任意字符串    #对方该共享的描述，可以是任意字符串
	browseable = yes/no   #指定共享目录是否可以浏览
	writeable = yes/no    #指定共享目录是否可写
	available = yes/no	  #指写共享目录浆是否可用
    admin users = 该共享的管理者 #admin users用来指定该共享的管理员（对该共享具有完全控制权限）。在samba 3.0中，如果用户验证方式设置成“security=share”时，此项无效。例如：admin users =bobyuan，jane（多个用户中间用逗号隔开）。
	valid users = 允许访问该共享的用户 #valid users用来指定允许访问该共享资源的用户。例如：valid users = bobyuan，@bob，@tech（多个用户或者组中间用逗号隔开，如果要加入一个组就用“@+组名”表示。）
	invalid users = 禁止访问该共享的用户  #invalid users用来指定不允许访问该共享资源的用户。例如：invalid users = root，@bob（多个用户或者组中间用逗号隔开。）
	write list = 允许写入该共享的用户#write list用来指定可以在该共享下写入文件的用户。例如：write list = bobyuan，@bob
	public = yes/no		#public用来指定该共享是否允许guest账户访问。
	guest ok = yes/no 	#意义同“public”。
```

配置完成后重启samba服务

```shell
sudo pkill smbd  #关闭samba服务
sudo smbd        #重启samba服务
#或者下面也是重启samba服务
sudo service smbd restart
```

##### 2、安装samba客户端

安装命令：      sudo apt-get install smbclient

查看samba服务器的所有共享目录： 

```shell
sudo smbclient -L 192.168.1.80    # 192.168.1.80是samba服务器IP
```

 ![](linux命令/samba共享目录列表.png)

连接共享目录

```shell
sudo smbclient //192.168.1.80/share     # //192.168.1.80/share  IP地址后接共享目录
```

将共享目录挂载到本地

```shell
sudo mount -t cifs -o username=zhangya //192.168.1.80/share /home/zhangya/tmp

#【注】我的环境上实际环境下执行的是下面语句，不需要指定password，并且username可以任意命名
#如果/home/zhangya/下没有tmp目录，可以用mkdir tmp创建一个tmp目录
```

##### 3、win10连接ubuntu的samba服务器

win+R，在弹出的运行窗口中输入：  \\\192.168.5.104      回车

重启samba服务器命令：   sudo service smbd restart



# 安装ubuntu18.04流程

###### 分区说明

| 分区名      | 分区大小 | 说明                                                         |
| ----------- | -------- | ------------------------------------------------------------ |
| EFI系统分区 | 2048M    | 新分区类型：主分区，新分区的位置：空间起始位置               |
| 交换空间    | 8192M    | 新分区类型：主分区，新分区的位置：空间起始位置，一般看内存大小取值 |
| /           | 102400M  | 新分区类型：主分区，新分区的位置：空间起始位置,，用于：Ext4 日志文件系统 |
| /home       | 剩余空间 | 新分区类型：逻辑分区，新分区的位置：空间起始位置，用于：Ext4 日志文件系统 |



# 使用sftp服务

ubuntu默认只安装openssh-client，需要手动安装openssh-server或者sftp-server

```
sudo apt update
sudo apt install openssh-server
```

登录/退出命令

```shell
sftp zhangya@192.168.2.108    #以zhangya的身份登录192.168.2.108主机，默认商品22
sftp -o Port=222 zhangya@192.168.2.108  #指定以222商品登录
//退出命令
bye
exit
quit
```

交互命令

```shell
-----//上传
put [-afPpRr] new.txt /home/zhangya/  #将本地当前目录下的new.txt上传到远程主机的/home/zhangya/目录
	-r: 允许拷贝子目录和子文件
	-p: 保留修改时间、访问时间以及被传输的文件的模式
-----//下载
get /home/zhangya/new.txt     #将远程主机的/home/zhangya/new.txt文件下载到本地当前目录
get /home/zhangya/new.txt /home/zhangya1/    #将远程主机的/home/zhangya/new.txt文件下载到本地/home/zhangya1/目录
```



# vim命令

永久显示行号命令：vim ~/.vimrc ;在文件中输入  set nu  保存即可

| 命令      | 功能                                                         |
| --------- | ------------------------------------------------------------ |
| u         | 撤销上一步操作                                               |
| ctrl+r    | 恢复上一步被撤销的操作                                       |
| dd        | 删除一行                                                     |
| ndd       | n 为数字。删除光标所在的向下 n 行，例如 20dd 则是删除 20 行  |
| yy        | 复制游标所在的那一行                                         |
| nyy       | n 为数字。复制光标所在的向下 n 行，例如 20yy 则是复制 20 行  |
| /word     | 向光标之下寻找一个名称为 word 的字符串。例如要在档案内搜寻 vbird 这个字符串，就输入 /vbird 即可！ (常用) |
| ?word     | 向光标之上寻找一个字符串名称为 word 的字符串                 |
| :set nu   | 显示行号，设定之后，会在每一行的前缀显示该行的行号           |
| :set nonu | 取消行号                                                     |

在一般模式之中，只要按下 i, o, a 等字符就可以进入输入模式了。



# shell脚本

#### 1、复杂逻辑判断

| -a   | 与   |
| :--- | ---- |
| -o   | 或   |
| !    | 非   |

```shell
#例子：判断两个文件是否存在，只要有一个文件存在则为真
#!/bin/sh

if [ -f "./usr/bin/rk3288_lock_app" -o -f "./usr/bin/rk3288_new_app" ]; then
        echo "rk3288_lock_app is exist"
else
        echo "rk3288_app is not exist"
fi
exit 0
```



```shell
cat  由第一行开始显示文件内容
tac  从最后一行开始显示，可以看出 tac 是 cat 的倒着写！
nl   显示的时候，顺道输出行号！
more 一页一页的显示文件内容
```



# linux挂载U盘

```shell
sudo fdisk -l
#挂载U盘
sudo mount /dev/sda /mnt/sda
#卸载U盘
sudo umount /mnt/sda
```

1> sudo fdisk -l  命令出下面显示，插入U盘后一盘在最下面，可以两个/dev/sdc1和/dev/sdc2两个磁盘目录

![](linux命令/fdisk_l命令.png)

2>通过mount命令将磁盘目录挂载到/mnt目录下，可以先在/mnt目录再新建一个目录后再挂载



# 更换apt软件源

https://blog.csdn.net/baidu_36602427/article/details/86548203

```shell
# 阿里云源
deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
##測試版源
deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
# 源碼
deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
##測試版源
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse

# 清华大学源
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
##測試版源
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
# 源碼
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
##測試版源
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
```

Ubuntu 的源存放在在 /etc/apt/ 目录下的 sources.list 文件中，修改前我们先备份，在终端中执行以下命令：

```shell
sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup
```

然后执行下面的命令打开 sources.list 文件，清空里面的内容，把上面阿里云与清华大学的 Ubuntu 源复制进去，保存后退出。

```shell
sudo gedit /etc/apt/sources.list
```

接着在终端上执行以下命令更新软件列表，检测出可以更新的软件：

```shell
sudo apt-get update
```

最后在终端上执行以下命令进行软件更新：

```shell
sudo apt-get upgrade
```



# 解决linux下firefox浏览器视频无法播放问题

1.安装flash插件

```shell
sudo apt-get install flashplugin-installer
```

2.重启桌面
		Alt + F2 # 打开命令行
		输入 r # 重启

​		以上可以解决大部分浏览器视频播放问题，但flash插件并不支持支持html5，故需要安装ffmpeg解码器。

3.安装ffmpeg解码器

```shell
sudo apt-get install ffmpeg
```

​		重启后所有问题得到解决。

# win10利用远程登陆桌面(mstsc)连接到ubuntu18.04

第一步：从网站下载安装脚本，网址：http://www.c-nergy.be/products.html，或者从百度网盘上下载：https://pan.baidu.com/s/1m0ahUdc9tfrREgNyKeazUA 提取码：odcs，  文件名：xrdp-installer-1.2

第二步：设置脚本的可执行权限

```
chmod +x ~/Downloads/xrdp-install-1.2.sh	//脚本下载后放在了Downloads目录下，这个可以随便放
```

第三步：运行脚本

```shell
cd Downloads
./xrdp-install-1.2.sh 			//可带参数-l，-s 

#不带参数是标准安装，可满足大多数人的需要，参数大致含义如下，具体可参考上述下载网站
-l:自定义登录界面
-s:重定向sound

#如果您启用了防火墙，确保xrdp所使用的端口（默认3389）已经开放
sudo ufw allow 3389/tcp
sudo ufw allow 5590/tcp
```



#### 注意：出现的问题

登入后一直提示“Authentication is Required to create a color managed device”这个显示界面。

解决方法：

在路径/etc/polkit-1/localauthority.conf.d/处新增一个文件，sudo vim 02-allow-colord.conf,这个文件；添加如下信息：

```
polkit.addRule(function(action, subject) {
 	if ((action.id == "org.freedesktop.color-manager.create-device" ||
 		action.id == "org.freedesktop.color-manager.create-profile" ||
 		action.id == "org.freedesktop.color-manager.delete-device" ||
 		action.id == "org.freedesktop.color-manager.delete-profile" ||
 		action.id == "org.freedesktop.color-manager.modify-device" ||
 		action.id == "org.freedesktop.color-manager.modify-profile") &&
 		subject.isInGroup("{users}")) {
 			return polkit.Result.YES;
 		}
 });
```

保存后重启ubuntu

#### linux下的V4L2框架（摄像头相关）

https://www.cnblogs.com/vedic/p/10763838.html

https://blog.csdn.net/u013904227/article/details/80718831



# VS code相关配置

​	vscode中使用GitHub：https://blog.csdn.net/qq_27386899/article/details/98629507

​    VS Code 基础入门使用（配置）教程：https://blog.csdn.net/qq_27386899/article/details/121455952

##### 1、空格太小，导致缩进距离很难看

![](linux命令/vs_code配置01.png)

##### 2、添加STM32配置环境



##### 3、常用插件

| 插件                                                      | 作用                                                         |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| Path Autocomplete                                         | 路径自动填充                                                 |
| Code Spell Checker                                        | 英语拼写检查                                                 |
| polacode                                                  | 选中代码生成代码图片. 按 `F1` 输入 `polacode` 打开           |
| Chinese (Simplified) Language Pack for Visual Studio Code | 汉化                                                         |
| Better Comments                                           | 提高注释可读性。在注释中最前面加入[ `*` , `!` , `?` , `todo` , `//` ]，注释将以不同颜色或样式显示 |
| Todo Tree                                                 | 在左侧添加了TODO列表，注释中所有的 `TODO` 将会以列表的形式列出来，并高亮显示，方便跳转查看 |
| Remote Development                                        | 包含了如下插件：`Remote - SSH `:ssh远程控制；`Remote - Containers `:docker控制；`Remote - WSL` 控制Windows Subsystem for Linux（wsl）。也可以单独搜索安装这些插件 |
| Settings Sync                                             | 同步你的vscode设置，包括setting文件，插件设置等，不过你要先有github的账户 |
| `Draw.io Integration`                                     | 绘画流程图。在vscode中创建文件后缀为 `.drawio` ，打开即可编辑。按 `F1` ，输入 `File: reopen with` ，选择文本方式，即可以xml方式打开编辑 |
| Office Viewer                                             | 在 vs code 中打开`.xls`, `.xlsx`, `.csv`, `.pdf`, `.md` 等格式 |
| Edit csv                                                  | CSV编辑器                                                    |
| vscode-icons                                              | 拓展文件图标                                                 |
| vscode-icons-mac                                          | 将vs code图标变为苹果电脑经典图标                            |
| filesize                                                  | 在左下角显示文件大小                                         |
| codelf                                                    | 网络查询变量命名，供变量命名参考                             |
|                                                           |                                                              |

##### 4、常用设置

| 设置选项                            | 实现的功能                           |
| ----------------------------------- | ------------------------------------ |
| Files: Auto Guess Encoding–>true    | 自动判断文件编码                     |
| editor.acceptSuggestionOnEnter–>off | 关闭回车自动补全                     |
| editor.tabCompletion–>on            | 按tab键自动补全，一直按一直切换      |
| editor.fontSize–>16                 | 更改字体为16                         |
| editor.formatOnSave–>true           | 保存自动格式化                       |
| editor.formatOnPaste–>true          | 复制自动格式化                       |
| editor.formatOnType–>true           | 回车后自动格式化                     |
| deepin/UOS系统去掉顶端白色区域      | `Window: Title Bar Style` --> custom |



# CMakeLists说明

https://www.jianshu.com/p/528eeb266f83

#### 1、初始cmake的helloworld

```c
#include <stdio.h>
int main(int argc, char* argv[]){
  printf("Hello CMake!\n");
}
```

之前都是采用 gcc hello.cpp -o hello 命令来生成可执行文件，但现在我们用 CMake 这种方式来生成，新建一个 CMakeLists.txt 文件名大小写都按照这个来：

```shell
# 指定工程名
PROJECT (HELLO)
# 现阶段，你只需要了解 SET 指令可以用来显式的定义变量即可
# 将 hello.cpp 赋值给 SRC_LIST 变量，也可以指定多个源文件，用空格隔开
# SET(SRC_LIST hello.cpp add.cpp sub.cpp)
# 用来给SRC_LIST付值 hello.cpp，如果有多个文件可以写成  SET(SRC_LIST 1.cpp 2.cpp 3.cpp)
SET(SRC_LIST hello.cpp)
# 输出打印构建目录
#STATUS：这个关键字表示 --;也就是在输出信息前面加个--
MESSAGE(STATUS "This is HELLO_BINARY_DIR " ${HELLO_BINARY_DIR})
# 输出打印资源目录
MESSAGE(STATUS "This is HELLO_SOURCE_DIR " ${HELLO_SOURCE_DIR})
# 输出打印资源目录，与HELLO_SOURCE_DIR 一样 
MESSAGE(STATUS "This is PROJECT_SOURCE_DIR " ${PROJECT_SOURCE_DIR})
# 输出打印 CMake 资源目录，与 PROJECT_SOURCE_DIR 一样 
MESSAGE(STATUS "This is CMAKE_SOURCE_DIR " ${CMAKE_SOURCE_DIR})
# 生成可执行文件 hello ，${SRC_LIST}是引用变量，也就是源文件 hello.cpp
ADD_EXECUTABLE(hello ${SRC_LIST})
```

##### 外部构建：

​		**新建 build 目录，cd 到 build 目录下，输入 cmake .. 命令**，ls 一下会发现 CMake 帮我们生成了 Makefile 等等一些文件。敲 make 命令生成 hello 可执行文件。

#### 2、构建生成.so动态库

mkdir 新建 3 个目录分别为 src、libs、include 。src 用来存放源文件 add.ccp、sub.cpp、div.cpp。include 用来存放头文件 add.h、div.h、sub.h 。源码如下：

```c
//add.cpp
#include "add.h"
int add(int num1, int num2){
        return num1 + num2;
}
//sub.cpp
#include "sub.h"                         
int sub(int num1, int num2){         
        return num1 - num2;         
}
//div.cpp
#include "div.h"                                              
int div(int num1, int num2){                    
        return num1 / num2;                  
}
```

 CMake 来构建一个 libmath.so 动态库，并且将其生成在 libs 目录文件夹下， CMakeLists.txt

```shell
# 指定 cmake 最低编译版本
CMAKE_MINIMUM_REQUIRED(VERSION 3.14)
PROJECT (MATH)
# 把当前工程目录下的 src 目录的下的所有 .cpp 和 .c 文件赋值给 SRC_LIST
# AUX_SOURCE_DIRECTORY(${PROJECT_SOURCE_DIR}/src SRC_LIST)
FILE(GLOB SRC_LIST "${PROJECT_SOURCE_DIR}/src/*.cpp")
# 打印 SRC_LIST 文件列表
# MESSAGE(STATUS ${SRC_LIST})
# 指定头文件目录
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/include)
# 指定输出 .so 动态库的目录位置
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/libs)
# 指定生成动态库
ADD_LIBRARY(math SHARED ${SRC_LIST})
# 指定生成版本号，VERSION指代动态库版本，SOVERSION指代API版本
# SET_TARGET_PROPERTIES(math PROPERTIES VERSION 1.2 SOVERSION 1)
```

##### <1>使用上面生成的库文件

将 libs 目录和 include 目录 copy 到 hello.cpp 同级目录下，修改 hello.cpp 源码如下

```c
#include <stdio.h>
#include "add.h"
#include "sub.h"
#include "div.h"

int main(int argc, char* argv[]){
        int a = 20;
        int b = 10;
        printf("%d+%d=%d\n",a,b,add(a,b));
        printf("%d-%d=%d\n",a,b,sub(a,b));
        printf("%d/%d=%d\n",a,b,div(a,b));
        return 0;
}
```

现在我引用了 include 目录下的头文件，同时需要链接 libs 目录下的 libmath.so ，我们再次创建一个 CMakeLists.txt 来生成可执行文件 hello

```shell
# 指定cmake最低编译版本
CMAKE_MINIMUM_REQUIRED(VERSION 3.14)
# 指定工程的名称
PROJECT(HELLO)
#指定头文件目录位置
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/include)
#添加共享库搜索路径
LINK_DIRECTORIES(${PROJECT_SOURCE_DIR}/libs)
#生成可执行文件
ADD_EXECUTABLE(hello hello.cpp)
#为hello添加共享math库链接  (math库也就是上面编译的库)
TARGET_LINK_LIBRARIES(hello math)
```

# 查看摄像头软件

https://blog.51cto.com/u_15127501/4632748

1. 安装 xawtv 测试软件

#sudo apt-get install xawtv

2. 执行 xawtv 后面带 usb 摄像头的设备节点

#sudo xawtv /dev/videoX

#### uboot介绍

1、uboot命令

```
help命令：查看uboot支持的所有命令
print/printenv:打印当前uboot的环境变量 ----- print 环境变量
set:设置环境变量
save/saveenv:保存当前环境变量
```

2、常见的环境变量

```
bootdelay-------启动后加载内核的延时时间
bootcmd--------加载启动内核的命令
bootargs-------加载内核时传递给内核的参数
```



# 音视频入门系列

#### 1、图像篇

I帧：（帧内编码图像帧）表示关键帧，对本帧数据进行压缩，不考虑其他帧数据

P帧：（向前预测编码图像帧），表示的是跟之前的一个关键帧或P帧的差别，P帧是参考帧，它可能造成解码错误的扩散；

B帧：（双向预测编码图像帧），本帧与前后帧（I或P帧）的差别，B帧压缩率高，但解码耗CPU



#### nginx-rtmp服务器

参考:

https://mp.weixin.qq.com/s?__biz=Mzg2MzA0NjM3Ng==&mid=2247483974&idx=2&sn=21d844195a08c1f699873de14a3dcf2c&chksm=ce7fddc1f90854d78dfb449d9187a9bcdbbdadd0c19496180fe4d384bf7a04d98d604bd2edd3&scene=21#wechat_redirect

###### 1、下载nginx和nginx-rtmp-module

```
https://nginx.org/en/download.html
https://github.com/arut/nginx-rtmp-module.git

tar xvf nginx-1.23.3.tar.gz
unzip nginx-rtmp-module-master.zip
解压到  /home/zhangya/software/locat_app/  全部该目录下
```

<img src="linux命令/nginx_download.png" style="zoom:80%;" />

###### 2、config和make和make install

```
cd /home/zhangya/software/locat_app/nginx-1.23.3

./configure --prefix=/home/zhangya/software/locat_app/nginx-1.23.3/build --add-module=/home/zhangya/software/locat_app/nginx-rtmp-module-master
make
make install
```

###### 3、config过程中错误及处理

```
error : ./configure: error: the HTTP rewrite module requires the PCRE library.
error :./configure: error: SSL modules require the OpenSSL library.
error :./configure: error: the HTTP gzip module requires the zlib library.
```

处理

```
sudo apt-get update
sudo apt-get install libpcre3 libpcre3-dev
sudo apt-get install openssl libssl-dev
sudo apt-get install zlib1g-dev
```

###### 4、配置文件 nginx.config

位置：/home/zhangya/software/locat_app/nginx-1.23.3/build/conf/nginx.config

```
rtmp {
    server {
        listen 1935;
        chunk_size 4096;

        # live on
        application rtmp_live {
            live on;
            # hls on; #这个参数把直播服务器改造成实时回放服务器。
            # wait_key on; #对视频切片进行保护，这样就不会产生马赛克了。
            # hls_path ./sbin/html; #切片视频文件存放位置。
            # hls_fragment 10s;     #每个视频切片的时长。
            # hls_playlist_length 60s;  #总共可以回看的时间，这里设置的是1分钟。
            # hls_continuous on; #连续模式。
            # hls_cleanup on;    #对多余的切片进行删除。
            # hls_nested on;     #嵌套模式。
        }

        # play videos
        application rtmp_play{
            play /home/zhangya/Videos;  #build directory
        }
    }
}
```

###### 5、启动nginx

```
sudo ./sbin/nginx

如果出现下面问题
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
大概意思就是80端口被占用
sudo netstat -ntlp
出现下图
```

![](linux命令/nginx_rtmp_01.png)

```
sudo kill 942
再重新启动nginx就可以了
```

###### 6、ffmpeg推流

```
ffmpeg -i 123.mp4 -vcodec libx264 -acodec aac -f flv rtmp://192.168.2.105:1935/rtmp_live/mystream
```

###### 7、VLC拉流（直播，点播）

```
直播：rtmp://192.168.2.105:1935/rtmp_live/mystream
点播：rtmp://192.168.2.105:1935/rtmp_play/02.mp4

说明 (02.mp4)需存放在 /home/zhangya/Videos中这个目录是可以变的，在配置文件的mginx.config文件的
	application rtmp_play{
       play /home/zhangya/Videos;  #build directory
    }
这里更改路径即可
```

# ubuntu18.04挂载新的硬盘

https://blog.csdn.net/guoqingru0311/article/details/128669273

##### 1、查看硬盘以及所属分区情况

```
sudo fdisk -l
```

![](linux命令/添加新硬盘_001.png)

可以看到，新安装的机械硬盘为/dev/sdb；这个不一定为sdb，也可能为sd后面的字母不一定

##### 2、硬盘分区

```
sudo fdisk /dev/sdb
#输入m可显示可用命令
#输入n新建分区
#输入p可查看分区结果
#输入w保存分区
```

##### 3、格式化分区

```
sudo mkfs -t ext4 /dev/sdb
```

##### 4、挂载硬盘

```
#设置挂载点
sudo mkdir /userdata
#挂载命令
sudo mount -t /dev/sdb /userdata
#查看挂载
df -l
```

##### 5、系统启动挂载

```
#查询磁盘分区的UUID
sudo blkid /dev/sdb
```

![](linux命令/添加新硬盘_002.png)

编辑/etc/fstab文件

```
sudo vim /etc/fstab

#大文件后面添加以下内容
UUID=4a5e0dfe-4dfa-4136-a7eb-b6ae1d1c812b    /userdata       ext4     defaults        0       2
```

##### 6、设置权限

此时硬盘虽然可进入但没有权限使用，右键可见无法新建文件

```
sudo chmod -R 777 /userdata
sudo chown -R 777 /userdata
```

