## #!/bin/bash

## 1、检测两台服务器指定目录下的文件一致性

```shell
#!/bin/bash
######################################
检测两台服务器指定目录下的文件一致性
#####################################
#通过对比两台服务器上文件的md5值，达到检测一致性的目的
dir=/data/web
b_ip=192.168.88.10
#将指定目录下的文件全部遍历出来并作为md5sum命令的参数，进而得到所有文件的md5值，并写入到指定文件中
find $dir -type f|xargs md5sum > /tmp/md5_a.txt
ssh $b_ip "find $dir -type f|xargs md5sum > /tmp/md5_b.txt"
scp $b_ip:/tmp/md5_b.txt /tmp
#将文件名作为遍历对象进行一一比对
for f in `awk '{print $2} /tmp/md5_a.txt'`do
#以a机器为标准，当b机器不存在遍历对象中的文件时直接输出不存在的结果
if grep -qw "$f" /tmp/md5_b.txt
then
md5_a=`grep -w "$f" /tmp/md5_a.txt|awk '{print $1}'`
md5_b=`grep -w "$f" /tmp/md5_b.txt|awk '{print $1}'`
#当文件存在时，如果md5值不一致则输出文件改变的结果
if [ $md5_a != $md5_b ]then
echo "$f changed."
fi
else
echo "$f deleted."
fi
done
```

## 2、定时清空文件内容，定时记录文件大小

```shell
#!/bin/bash
#################################################################
每小时执行一次脚本（任务计划），当时间为0点或12点时，将目标目录下的所有文件内#容清空，但不删除文件，其他时间则只统计各个文件的大小，一个文件一行，输出到以时#间和日期命名的文件中，需要考虑目标目录下二级、三级等子目录的文件
################################################################
logfile=/tmp/`date +%H-%F`.log
n=`date +%H`
if [ $n -eq 00 ] || [ $n -eq 12 ]
then
#通过for循环，以find命令作为遍历条件，将目标目录下的所有文件进行遍历并做相应操作
for i in `find /data/log/ -type f`
do
true > $i
done
else
for i in `find /data/log/ -type f`
do
du -sh $i >> $logfile
done
fi
```

## 3、检测网卡流量，并按规定格式记录在日志中

```shell
#!/bin/bash
#######################################################
#检测网卡流量，并按规定格式记录在日志中#规定一分钟记录一次
#日志格式如下所示:
#2019-08-12 20:40
#ens33 input: 1234bps
#ens33 output: 1235bps
######################################################3
while :
do
#设置语言为英文，保障输出结果是英文，否则会出现bug
LANG=en
logfile=/tmp/`date +%d`.log
#将下面执行的命令结果输出重定向到logfile日志中
exec >> $logfile
date +"%F %H:%M"
#sar命令统计的流量单位为kb/s，日志格式为bps，因此要*1000*8
sar -n DEV 1 59|grep Average|grep ens33|awk '{print $2,"\t","input:","\t",$5*1000*8,"bps","\n",$2,"\t","output:","\t",$6*1000*8,"bps"}'
echo "####################"
#因为执行sar命令需要59秒，因此不需要sleep
done
```

## 4、计算文档每行出现的数字个数，并计算整个文档的数字总数

```shell
#!/bin/bash
#########################################################
#计算文档每行出现的数字个数，并计算整个文档的数字总数
########################################################
#使用awk只输出文档行数（截取第一段）
n=`wc -l a.txt|awk '{print $1}'`
sum=0
#文档中每一行可能存在空格，因此不能直接用文档内容进行遍历
for i in `seq 1 $n`do
#输出的行用变量表示时，需要用双引号
line=`sed -n "$i"p a.txt`#wc -L选项，统计最长行的长度
n_n=`echo $line|sed s'/[^0-9]//'g|wc -L`
echo $n_nsum=$[$sum+$n_n]
done
echo "sum:$sum"
```

### 杀死所有脚本

```shell
#!/bin/bash
################################################################
#有一些脚本加入到了cron之中，存在脚本尚未运行完毕又有新任务需要执行的情况，
#导致系统负载升高，因此可通过编写脚本，筛选出影响负载的进程一次性全部杀死。
################################################################
ps aux|grep 指定进程名|grep -v grep|awk '{print $2}'|xargs kill -9
```

## 5、从 FTP 服务器下载文件

```shell
#!/bin/bash
if [ $# -ne 1 ]; then
    echo "Usage: $0 filename"
fi
dir=$(dirname $1)
file=$(basename $1)
ftp -n -v << EOF   # -n 自动登录
open 192.168.1.10  # ftp服务器
user admin password
binary   # 设置ftp传输模式为二进制，避免MD5值不同或.tar.gz压缩包格式错误
cd $dir
get "$file"
EOF
```

## 6、连续输入5个100以内的数字，统计和、最小和最大

```shell
#!/bin/bash
COUNT=1
SUM=0
MIN=0
MAX=100
while [ $COUNT -le 5 ]; do
    read -p "请输入1-10个整数：" INT    
    if [[ ! $INT =~ ^[0-9]+$ ]]; then
        echo "输入必须是整数！"
        exit 1
    elif [[ $INT -gt 100 ]]; then
        echo "输入必须是100以内！"
        exit 1
    fi
    SUM=$(($SUM+$INT))
    [ $MIN -lt $INT ] && MIN=$INT
    [ $MAX -gt $INT ] && MAX=$INT
    let COUNT++
    done
echo "SUM: $SUM"
echo "MIN: $MIN"
echo "MAX: $MAX
```

### 用户猜数字

```shell
#!/bin/bash  # 脚本生成一个 100 以内的随机数,提示用户猜数字,根据用户的输入,提示用户猜对了,
# 猜小了或猜大了,直至用户猜对脚本结束。
# RANDOM 为系统自带的系统变量,值为 0‐32767的随机数
# 使用取余算法将随机数变为 1‐100 的随机数num=$[RANDOM%100+1]echo "$num" 
# 使用 read 提示用户猜数字
# 使用 if 判断用户猜数字的大小关系:‐eq(等于),‐ne(不等于),‐gt(大于),‐ge(大于等于),
# ‐lt(小于),‐le(小于等于)

while :
  do     
    read -p "计算机生成了一个 1‐100 的随机数,你猜: " cai    
    if [ $cai -eq $num ]    
    then        
        echo "恭喜,猜对了"           
        exit        
    elif [ $cai -gt $num ]       
    then            
        echo "Oops,猜大了"         
    else            
        echo "Oops,猜小了"     
    fi
  done
```

## 7、监测 Nginx 访问日志 502 情况，并做相应动作

假设服务器环境为 lnmp，近期访问经常出现 502 现象，且 502 错误在重启 php-fpm 服务后消失，因此需要编写监控脚本，一旦出现 502，则自动重启 php-fpm 服务。

```shell
#场景：
#1.访问日志文件的路径：/data/log/access.log
#2.脚本死循环，每10秒检测一次，10秒的日志条数为300条，出现502的比例不低于10%（30条）则需要重启php-fpm服务
#3.重启命令为：/etc/init.d/php-fpm restart
#!/bin/bash
###########################################################
#监测Nginx访问日志502情况，并做相应动作
###########################################################
log=/data/log/access.log
N=30 #设定阈值
while :do
 #查看访问日志的最新300条，并统计502的次数
    err=`tail -n 300 $log |grep -c '502" '` 
if [ $err -ge $N ] 
then
/etc/init.d/php-fpm restart 2> /dev/null 
#设定60s延迟防止脚本bug导致无限重启php-fpm服务
     sleep 60
 fi
 sleep 10
 done
```

## 8、将结果分别赋值给变量

应用场景：希望将执行结果或者位置参数赋值给变量，以便后续使用。

### 方法1：

```shell
for i in $(echo "4 5 6"); do
   eval a$i=$idone
echo $a4 $a5 $a6
```

### 方法2：将位置参数192.168.1.1{1,2}拆分为到每个变量

```shell
num=0
for i in $(eval echo $*);do   #eval将{1,2}分解为1 2
   let num+=1
   eval node${num}="$i"
done
echo $node1 $node2 $node3
# bash a.sh 192.168.1.1{1,2}
192.168.1.11 192.168.1.12

方法3：arr=(4 5 6)
INDEX1=$(echo ${arr[0]})
INDEX2=$(echo ${arr[1]})
INDEX3=$(echo ${arr[2]})
```

## 9、批量修改文件名

示例：

```shell
# touch article_{1..3}.html
# ls article_1.html  article_2.html  article_3.html
目的：把article改为bbs
```

### 方法1：

```shell
for file in $(ls *html); do
    mv $file bbs_${file#*_}
    # mv $file $(echo $file |sed -r 's/.*(_.*)/bbs\1/')
    # mv $file $(echo $file |echo bbs_$(cut -d_ -f2)
```

### 方法2：

```shell
for file in $(find . -maxdepth 1 -name "*html"); do
     mv $file bbs_${file#*_}done
```

### 方法3：

```shell
# rename article bbs *.html
把一个文档前五行中包含字母的行删掉，同时删除6到10行包含的所有字母

1）准备测试文件，文件名为2.txt

第1行1234567不包含字母
第2行56789BBBBBB
第3行67890CCCCCCCC
第4行78asdfDDDDDDDDD
第5行123456EEEEEEEE
第6行1234567ASDF
第7行56789ASDF
第8行67890ASDF
第9行78asdfADSF
第10行123456AAAA
第11行67890ASDF
第12行78asdfADSF
第13行123456AAAA
```

### 2）脚本如下：

```shell
#!/bin/bash
###############################################################
把一个文档前五行中包含字母的行删掉，同时删除6到10行包含的所有字母
##############################################################
sed -n '1,5'p 2.txt |sed '/[a-zA-Z]/'d
sed -n '6,10'p 2.txt |sed s'/[a-zA-Z]//'g
sed -n '11,$'p 2.txt
#最终结果只是在屏幕上打印结果，如果想直接更改文件，可将输出结果写入临时文件中，再替换2.txt或者使用-i选项


```

## 10、统计当前目录中以.html结尾的文件总大

方法1：

~~~shell
# find . -name "*.html" -exec du -k {} \; |awk '{sum+=$1}END{print sum}'

方法2：
```bash
for size in $(ls -l *.html |awk '{print $5}'); do
    sum=$(($sum+$size))
done
echo $sum
~~~

## 11、扫描主机端口状态

```shell
#!/bin/bash
HOST=$1
PORT="22 25 80 8080"
for PORT in $PORT; do
    if echo &>/dev/null > /dev/tcp/$HOST/$PORT; then
        echo "$PORT open"
    else
        echo "$PORT close"
    fi
done
用 shell 打印示例语句中字母数小于6的单词

#示例语句：
#Bash also interprets a number of multi-character options.
#!/bin/bash
##############################################################
#shell打印示例语句中字母数小于6的单词
##############################################################
for s in Bash also interprets a number of multi-character options.
do
 n=`echo $s|wc -c` 
 if [ $n -lt 6 ] 
 then
 echo $s
 fi
done
```

## 12、输入数字运行相应命令

```shell
#!/bin/bash
##############################################################
#输入数字运行相应命令
##############################################################
echo "*cmd menu* 1-date 2-ls 3-who 4-pwd 0-exit "
while :
do
#捕获用户键入值
 read -p "please input number :" n
 n1=`echo $n|sed s'/[0-9]//'g`
#空输入检测 
 if [ -z "$n" ]
 then
 continue
 fi
#非数字输入检测 
 if [ -n "$n1" ]
 then
 exit 0
 fi
 break
done
case $n in
 1)
 date
 ;;
 2)
 ls
 ;;
 3)
 who
 ;;
 4)
 pwd
 ;;
 0)
 break
 ;;
    #输入数字非1-4的提示
 *)
 echo "please input number is [1-4]"
esac
```

## 13、Expect 实现 SSH 免交互执行命令

Expect是一个自动交互式应用程序的工具，如telnet，ftp，passwd等。

需先安装expect软件包。

### 方法1：EOF标准输出作为expect标准输入

```shell
#!/bin/bash
USER=root
PASS=123.com
IP=192.168.1.120
expect << EOFset timeout 30spawn ssh $USER@$IP   expect {    "(yes/no)" {send "yes\r"; exp_continue}    "password:" {send "$PASS\r"}
}
expect "$USER@*"  {send "$1\r"}
expect "$USER@*"  {send "exit\r"}
expect eof
EOF
```

### 方法2：

```shell
#!/bin/bash
USER=root
PASS=123.com
IP=192.168.1.120
expect -c "
    spawn ssh $USER@$IP
    expect {
        \"(yes/no)\" {send \"yes\r\"; exp_continue}
        \"password:\" {send \"$PASS\r\"; exp_continue}
        \"$USER@*\" {send \"df -h\r exit\r\"; exp_continue}
    }"
```

## 方法3：将expect脚本独立出来

```shell
登录脚本：
# cat login.exp
#!/usr/bin/expect
set ip [lindex $argv 0]
set user [lindex $argv 1]
set passwd [lindex $argv 2]
set cmd [lindex $argv 3]
if { $argc != 4 } {
puts "Usage: expect login.exp ip user passwd"
exit 1
}
set timeout 30
spawn ssh $user@$ip
expect {    
    "(yes/no)" {send "yes\r"; exp_continue}
    "password:" {send "$passwd\r"}
}
expect "$user@*"  {send "$cmd\r"}
expect "$user@*"  {send "exit\r"}
expect eof
```

执行命令脚本：写个循环可以批量操作多台服务器

~~~shell
#!/bin/bash
HOST_INFO=user_info.txt
for ip in $(awk '{print $1}' $HOST_INFO)
do
    user=$(awk -v I="$ip" 'I==$1{print $2}' $HOST_INFO)
    pass=$(awk -v I="$ip" 'I==$1{print $3}' $HOST_INFO)
    expect login.exp $ip $user $pass $1
done
Linux主机SSH连接信息：
# cat user_info.txt
192.168.1.120 root 123456
创建10个用户，并分别设置密码，密码要求10位且包含大小写字母以及数字，最后需要把每个用户的密码存在指定文件中
```bash
#!/bin/bash
##############################################################
#创建10个用户，并分别设置密码，密码要求10位且包含大小写字母以及数字
#最后需要把每个用户的密码存在指定文件中#前提条件：安装mkpasswd命令
##############################################################
#生成10个用户的序列（00-09）
for u in `seq -w 0 09`do
 #创建用户
 useradd user_$u
 #生成密码
 p=`mkpasswd -s 0 -l 10` 
 #从标准输入中读取密码进行修改（不安全）
 echo $p|passwd --stdin user_$u
 #常规修改密码
 echo -e "$p\n$p"|passwd user_$u
 #将创建的用户及对应的密码记录到日志文件中
 echo "user_$u $p" >> /tmp/userpassworddone
~~~

## 14、监控 httpd 的进程数，根据监控情况做相应处理

```shell
#!/bin/bash
###############################################################################################################################
#需求：
#1.每隔10s监控httpd的进程数，若进程数大于等于500，则自动重启Apache服务，并检测服务是否重启成功
#2.若未成功则需要再次启动，若重启5次依旧没有成功，则向管理员发送告警邮件，并退出检测
#3.如果启动成功，则等待1分钟后再次检测httpd进程数，若进程数正常，则恢复正常检测（10s一次），否则放弃重启并向管理员发送告警邮件，并退出检测
###############################################################################################################################
#计数器函数
check_service()
{
 j=0
 for i in `seq 1 5` 
 do
 #重启Apache的命令
 /usr/local/apache2/bin/apachectl restart 2> /var/log/httpderr.log    
    #判断服务是否重启成功
 if [ $? -eq 0 ] then
 break
 else
 j=$[$j+1] fi
    #判断服务是否已尝试重启5次
 if [ $j -eq 5 ] then
 mail.py exit
 fi
 done }while :do
 n=`pgrep -l httpd|wc -l` 
 #判断httpd服务进程数是否超过500
 if [ $n -gt 500 ] then
 /usr/local/apache2/bin/apachectl restart 
 if [ $? -ne 0 ] 
 then
 check_service 
 else
 sleep 60
 n2=`pgrep -l httpd|wc -l` 
 #判断重启后是否依旧超过500
             if [ $n2 -gt 500 ] 
 then 
 mail.py exit
 fi
 fi
 fi
 #每隔10s检测一次
 sleep 10done
```

## 15、批量修改服务器用户密码

Linux主机SSH连接信息：旧密码

```shell
# cat old_pass.txt 
192.168.18.217  root    123456     22
192.168.18.218  root    123456     22
内容格式：IP User Password Port

SSH远程修改密码脚本：新密码随机生成
https://www.linuxprobe.com/books
#!/bin/bash
OLD_INFO=old_pass.txt
NEW_INFO=new_pass.txt
for IP in $(awk '/^[^#]/{print $1}' $OLD_INFO); do
    USER=$(awk -v I=$IP 'I==$1{print $2}' $OLD_INFO)
    PASS=$(awk -v I=$IP 'I==$1{print $3}' $OLD_INFO)
    PORT=$(awk -v I=$IP 'I==$1{print $4}' $OLD_INFO)
    NEW_PASS=$(mkpasswd -l 8)  # 随机密码
    echo "$IP   $USER   $NEW_PASS   $PORT" >> $NEW_INFO
    expect -c "
    spawn ssh -p$PORT $USER@$IP
    set timeout 2
    expect {
        \"(yes/no)\" {send \"yes\r\";exp_continue}
        \"password:\" {send \"$PASS\r\";exp_continue}
        \"$USER@*\" {send \"echo \'$NEW_PASS\' |passwd --stdin $USER\r exit\r\";exp_continue}
    }"
done
生成新密码文件：

# cat new_pass.txt 
192.168.18.217  root    n8wX3mU%      22
192.168.18.218  root    c87;ZnnL      22
```

## 16、iptables 自动屏蔽访问网站频繁的IP

场景：恶意访问,安全防范

### 1）屏蔽每分钟访问超过200的IP

#### 方法1：根据访问日志（Nginx为例）

```shell
#!/bin/bash
DATE=$(date +%d/%b/%Y:%H:%M)
ABNORMAL_IP=$(tail -n5000 access.log |grep $DATE |awk '{a[$1]++}END{for(i in a)if(a[i]>100)print i}')
#先tail防止文件过大，读取慢，数字可调整每分钟最大的访问量。awk不能直接过滤日志，因为包含特殊字符。
for IP in $ABNORMAL_IP; do
    if [ $(iptables -vnL |grep -c "$IP") -eq 0 ]; then
        iptables -I INPUT -s $IP -j DROP    fidone
```

#### 方法2：通过TCP建立的连接

```shell
#!/bin/bash
ABNORMAL_IP=$(netstat -an |awk '$4~/:80$/ && $6~/ESTABLISHED/{gsub(/:[0-9]+/,"",$5);{a[$5]++}}END{for(i in a)if(a[i]>100)print i}')
#gsub是将第五列（客户端IP）的冒号和端口去掉
for IP in $ABNORMAL_IP; do
    if [ $(iptables -vnL |grep -c "$IP") -eq 0 ]; then
        iptables -I INPUT -s $IP -j DROP    
        fi
done
```

### 2）屏蔽每分钟SSH尝试登录超过10次的IP

#### 方法1：通过lastb获取登录状态:

```shell
#!/bin/bash
DATE=$(date +"%a %b %e %H:%M") #星期月天时分  %e单数字时显示7，而%d显示07
ABNORMAL_IP=$(lastb |grep "$DATE" |awk '{a[$3]++}END{for(i in a)if(a[i]>10)print i}')for IP in $ABNORMAL_IP; do
    if [ $(iptables -vnL |grep -c "$IP") -eq 0 ]; then
        iptables -I INPUT -s $IP -j DROP    fidone
```

#### 方法2：通过日志获取登录状态

```
#!/bin/bash
DATE=$(date +"%b %d %H")
ABNORMAL_IP="$(tail -n10000 /var/log/auth.log |grep "$DATE" |awk '/Failed/{a[$(NF-3)]++}END{for(i in a)if(a[i]>5)print i}')"
for IP in $ABNORMAL_IP; do
    if [ $(iptables -vnL |grep -c "$IP") -eq 0 ]; then
        iptables -A INPUT -s $IP -j DROP        
        echo "$(date +"%F %T") - iptables -A INPUT -s $IP -j DROP" >>~/ssh-login-limit.log    
    fi
done
```

## 17、根据web访问日志，封禁请求量异常的IP，如IP在半小时后恢复正常，则解除封禁

```shell
#!/bin/bash
####################################################################################
#根据web访问日志，封禁请求量异常的IP，如IP在半小时后恢复正常，则解除封禁
####################################################################################
logfile=/data/log/access.log
#显示一分钟前的小时和分钟
d1=`date -d "-1 minute" +%H%M`
d2=`date +%M`
ipt=/sbin/iptables
ips=/tmp/ips.txt
block()
{ 
#将一分钟前的日志全部过滤出来并提取IP以及统计访问次数
 grep '$d1:' $logfile|awk '{print $1}'|sort -n|uniq -c|sort -n > $ips
 #利用for循环将次数超过100的IP依次遍历出来并予以封禁
 for i in `awk '$1>100 {print $2}' $ips` 
 do
 $ipt -I INPUT -p tcp --dport 80 -s $i -j REJECT 
 echo "`date +%F-%T` $i" >> /tmp/badip.log 
 done
}
unblock()
{ 
#将封禁后所产生的pkts数量小于10的IP依次遍历予以解封
 for a in `$ipt -nvL INPUT --line-numbers |grep '0.0.0.0/0'|awk '$2<10 {print $1}'|sort -nr` 
 do 
 $ipt -D INPUT $a
 done
 $ipt -Z
}
#当时间在00分以及30分时执行解封函数
if [ $d2 -eq "00" ] || [ $d2 -eq "30" ] 
 then
 #要先解再封，因为刚刚封禁时产生的pkts数量很少
 unblock
 block 
 else
 block
fi
```

## 18、判断用户输入的是否为IP地址

### 方法1:

```shell
#!/bin/bash
function check_ip(){
    IP=$1
    VALID_CHECK=$(echo $IP|awk -F. '$1< =255&&$2<=255&&$3<=255&&$4<=255{print "yes"}')
    if echo $IP|grep -E "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$">/dev/null; then
        if [ $VALID_CHECK == "yes" ]; then
            echo "$IP available."
        else
            echo "$IP not available!"
        fi
    else
        echo "Format error!"
    fi
}
check_ip 192.168.1.1
check_ip 256.1.1.1
```

### 方法2：

```shell
#!/bin/bash
function check_ip(){
    IP=$1
    if [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        FIELD1=$(echo $IP|cut -d. -f1)
        FIELD2=$(echo $IP|cut -d. -f2)
        FIELD3=$(echo $IP|cut -d. -f3)
        FIELD4=$(echo $IP|cut -d. -f4)
        if [ $FIELD1 -le 255 -a $FIELD2 -le 255 -a $FIELD3 -le 255 -a $FIELD4 -le 255 ]; then
            echo "$IP available."
        else
            echo "$IP not available!"
        fi
    else
        echo "Format error!"
    fi
}
check_ip 192.168.1.1
check_ip 256.1.1.1
```

### 增加版：

加个死循环，如果IP可用就退出，不可用提示继续输入，并使用awk判断。

```shell
#!/bin/bash
function check_ip(){
    local IP=$1
    VALID_CHECK=$(echo $IP|awk -F. '$1< =255&&$2<=255&&$3<=255&&$4<=255{print "yes"}')
    if echo $IP|grep -E "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$" >/dev/null; then
        if [ $VALID_CHECK == "yes" ]; then
            return 0
        else
            echo "$IP not available!"
            return 1
        fi
    else
        echo "Format error! Please input again."
        return 1
    fi
}
while true; do
    read -p "Please enter IP: " IP
    check_ip $IP
    [ $? -eq 0 ] && break || continue
done
```



## 俄罗斯方块

```shell
#!/bin/bash

APP_NAME="${0##*[\\/]}"
APP_VERSION="1.0"

#颜色定义
iSumColor=7			#颜色总数
cRed=1				#红色
cGreen=2			#绿色
cYellow=3			#黄色
cBlue=4				#蓝色
cFuchsia=5			#紫红色
cCyan=6				#青色(蓝绿色)
cWhite=7			#白色

#位置与大小
marginLeft=3			#边框左边距
marginTop=2			#边框上边距
((mapLeft=marginLeft+2))	#棋盘左边距
((mapTop=$marginTop+1))		#棋盘上边距
mapWidth=10			#棋盘宽度
mapHeight=15			#棋盘高度

#颜色设置
cBorder=$cGreen
cScore=$cFuchsia
cScoreValue=$cCyan

#控制信号
#游戏使用两个进程，一个用于接收输入，一个用于游戏流程和显示界面;
#当前者接收到上下左右等按键时，通过向后者发送signal的方式通知后者。
sigRotate=25		#向上键
sigLeft=26
sigRight=27
sigDown=28
sigAllDown=29		#空格键
sigExit=30

#方块定义，7大类19种样式
#前8位为方块坐标，后2位为方块刚出现的时候的位置
box0_0=(0 0 0 1 1 0 1 1 0 4)

box1_0=(0 1 1 1 2 1 3 1 0 3)
box1_1=(1 0 1 1 1 2 1 3 -1 3)

box2_0=(0 0 1 0 1 1 2 1 0 4)
box2_1=(0 1 0 2 1 0 1 1 0 3)

box3_0=(0 1 1 0 1 1 2 0 0 4)
box3_1=(0 0 0 1 1 1 1 2 0 4)

box4_0=(0 2 1 0 1 1 1 2 0 3)
box4_1=(0 1 1 1 2 1 2 2 0 3)
box4_2=(1 0 1 1 1 2 2 0 -1 3)
box4_3=(0 0 0 1 1 1 2 1 0 4)

box5_0=(0 0 1 0 1 1 1 2 0 3)
box5_1=(0 1 0 2 1 1 2 1 0 3)
box5_2=(1 0 1 1 1 2 2 2 -1 3)
box5_3=(0 1 1 1 2 0 2 1 0 4)

box6_0=(0 1 1 0 1 1 1 2 0 3)
box6_1=(0 1 1 1 1 2 2 1 0 3)
box6_2=(1 0 1 1 1 2 2 1 -1 3)
box6_3=(0 1 1 0 1 1 2 1 0 4)

iSumType=7			#方块类型总数
boxStyle=(1 2 2 2 4 4 4)	#各种方块旋转后可能的样式数目

iScoreEachLevel=50	#提升一个级别需要的分数
#运行时数据
sig=0			#接收到的signal
iScore=0		#总分
iLevel=0		#速度级
boxNext=()		#下一个方块
iboxNextColor=0		#下一个方块的颜色
iboxNextType=0		#下一个方块的种类
iboxNextStyle=0		#下一个方块的样式
boxCur=()		#当前方块的位置定义
iBoxCurColor=0		#当前方块的颜色
iBoxCurType=0		#当前方块的种类
iBoxCurStyle=0		#当前方块的样式
boxCurX=-1		#当前方块的x坐标位置
boxCurY=-1		#当前方块的y坐标位置
map=()			#棋盘图表

#初始化所有背景方块为-1, 表示没有方块
for ((i = 0; i < mapHeight * mapWidth; i++))
do
	map[$i]=-1
done

#接收输入的进程的主函数
function RunAsKeyReceiver()
{
	local pidDisplayer key aKey sig cESC sTTY

	pidDisplayer=$1
	aKey=(0 0 0)

	cESC=`echo -ne "\033"`
	cSpace=`echo -ne "\040"`

	#保存终端属性。在read -s读取终端键时，终端的属性会被暂时改变。
	#如果在read -s时程序被不幸杀掉，可能会导致终端混乱，
	#需要在程序退出时恢复终端属性。
	sTTY=`stty -g`

	#捕捉退出信号
	trap "MyExit;" INT QUIT
	trap "MyExitNoSub;" $sigExit

	#隐藏光标
	echo -ne "\033[?25l"

	while :
	do
		#读取输入。注-s不回显，-n读到一个字符立即返回
		read -s -n 1 key

		aKey[0]=${aKey[1]}
		aKey[1]=${aKey[2]}
		aKey[2]=$key
		sig=0

		#判断输入了何种键
		if [[ $key == $cESC && ${aKey[1]} == $cESC ]]
		then
			#ESC键
			MyExit
		elif [[ ${aKey[0]} == $cESC && ${aKey[1]} == "[" ]]
		then
			if [[ $key == "A" ]]; then sig=$sigRotate	#<向上键>
			elif [[ $key == "B" ]]; then sig=$sigDown	#<向下键>
			elif [[ $key == "D" ]]; then sig=$sigLeft	#<向左键>
			elif [[ $key == "C" ]]; then sig=$sigRight	#<向右键>
			fi
		elif [[ $key == "W" || $key == "w" ]]; then sig=$sigRotate	#W, w
		elif [[ $key == "S" || $key == "s" ]]; then sig=$sigDown	#S, s
		elif [[ $key == "A" || $key == "a" ]]; then sig=$sigLeft	#A, a
		elif [[ $key == "D" || $key == "d" ]]; then sig=$sigRight	#D, d
		elif [[ "[$key]" == "[]" ]]; then sig=$sigAllDown	#空格键
		elif [[ $key == "Q" || $key == "q" ]]			#Q, q
		then
			MyExit
		fi

		if [[ $sig != 0 ]]
		then
			#向另一进程发送消息
			kill -$sig $pidDisplayer
		fi
	done
}

#退出前的恢复
MyExitNoSub()
{
	local y

	#恢复终端属性
	stty $sTTY
	((y = marginTop + mapHeight + 4))

	#显示光标
	echo -e "\033[?25h\033[${y};0H"
	exit
}

MyExit()
{
	#通知显示进程需要退出
	kill -$sigExit $pidDisplayer

	MyExitNoSub
}

#处理显示和游戏流程的主函数
RunAsDisplayer()
{
	local sigThis
	InitDraw

	#挂载各种信号的处理函数
	trap "sig=$sigRotate;" $sigRotate
	trap "sig=$sigLeft;" $sigLeft
	trap "sig=$sigRight;" $sigRight
	trap "sig=$sigDown;" $sigDown
	trap "sig=$sigAllDown;" $sigAllDown
	trap "ShowExit;" $sigExit

	while :
	do
		#根据当前的速度级iLevel不同，设定相应的循环的次数
		for ((i = 0; i < 21 - iLevel; i++))
		do
			sleep 0.02
			sigThis=$sig
			sig=0

			#根据sig变量判断是否接受到相应的信号
			if ((sigThis == sigRotate)); then BoxRotate;	#旋转
			elif ((sigThis == sigLeft)); then BoxLeft;	#左移一列
			elif ((sigThis == sigRight)); then BoxRight;	#右移一列
			elif ((sigThis == sigDown)); then BoxDown;	#下落一行
			elif ((sigThis == sigAllDown)); then BoxAllDown;	#下落到底
			fi
		done
		#kill -$sigDown $$
		BoxDown	#下落一行
	done
}

#绘制当前方块，传第一个参数，0表示擦除当前方块，1表示绘制当前方块
DrawCurBox()
{
	local i x y bErase sBox
	bErase=$1
	if (( bErase == 0 ))
	then
		sBox="\040\040"		#用两个空格擦除
	else
		sBox="[]"
		echo -ne "\033[1m\033[3${iBoxCurColor}m\033[4${iBoxCurColor}m"
	fi

	for ((i = 0; i < 8; i += 2))
	do
		((y = mapTop + 1 + ${boxCur[$i]} + boxCurY))
		((x = mapLeft + 1 + 2 * (boxCurX + ${boxCur[$i + 1]})))
		echo -ne "\033[${y};${x}H${sBox}"
	done
	echo -ne "\033[0m"
}

#移动方块
#BoxMove(y, x), 测试是否可以把移动中的方块移到(y, x)的位置, 返回0则可以, 1不可以
BoxMove()
{
	local i x y xPos yPos
	yPos=$1
	xPos=$2
	for ((i = 0; i < 8; i += 2))
	do
		#方块相对于棋盘坐标
		((y = yPos + ${boxCur[$i]}))
		((x = xPos + ${boxCur[$i + 1]}))

		if (( y < 0 || y >= mapHeight || x < 0 || x >= mapWidth))
		then
			#撞到墙壁了
			return 1
		fi
		
		if (( ${map[y * mapWidth + x]} != -1 ))
		then
			#撞到其他已经存在的方块了
			return 1
		fi
	done
	return 0;
}

#将方块贴到棋盘上
Box2Map()
{
	local i j x y line
	#将当前移动中的方块贴到棋盘对应的区域
	for ((i = 0; i < 8; i += 2))
	do
		#计算方块相对于棋盘的坐标
		((y = ${boxCur[$i]} + boxCurY))
		((x = ${boxCur[$i + 1]} + boxCurX))
		map[y*mapWidth+x]=$iBoxCurColor	#将方块颜色赋给地图
	done

	line=0
	for ((i = 0; i < mapHeight; i++))
	do
		for ((j = 0; j < mapWidth; j++))
		do
			#如果棋盘上有空隙，跳出循环
			[[ ${map[i*mapWidth+j]} -eq -1 ]] && break
		done

		[ $j -lt $mapWidth ] && continue
		#说明当前行可消去，可消去行数加一
		(( line++ ))

		#第i行可被消除，将0行至第i-1行全部下移一行，从第i-1行开始移动
		for ((j = i*mapWidth-1; j >= 0; j--))
		do
			((x = j + mapWidth))
			map[$x]=${map[$j]}
		done

		#因为下移一行，第0行置空
		for ((i = 0; i < mapWidth; i++))
		do
			map[$i]=-1
		done
	done
	
	[ $line -eq 0 ] && return

	#根据消去的行数line计算分数和速度级
	((x = marginLeft + mapWidth * 2 + 7))
	((y = marginTop + 11))
	((iScore += line * 2 - 1))
	#显示新的分数
	echo -ne "\033[1m\033[3${cScoreValue}m\033[${y};${x}H${iScore}         "
	if ((iScore % iScoreEachLevel < line * 2 - 1))
	then
		if ((iLevel < 20))
		then
			((iLevel++))
			((y = marginTop + 14))
			#显示新的速度级
			echo -ne "\033[3${cScoreValue}m\033[${y};${x}H${iLevel}        "
		fi
	fi
	echo -ne "\033[0m"

	#重新显示背景方块
	for ((i = 0; i < mapHeight; i++))
	do
		#棋盘相对于屏幕的坐标
		((y = i + mapTop + 1))
		((x = mapLeft + 1))
		echo -ne "\033[${y};${x}H"
		for ((j = 0; j < mapWidth; j++))
		do
			((tmp = i * mapWidth + j))
			if ((${map[$tmp]} == -1))
			then
				echo -ne "  "
			else
				echo -ne "\033[1m\033[3${map[$tmp]}m\033[4${map[$tmp]}m[]\033[0m"
			fi
		done
	done
}

#左移一格
BoxLeft()
{
	local x
	((x = boxCurX - 1))
	if BoxMove $boxCurY $x
	then
		DrawCurBox 0
		((boxCurX = x))
		DrawCurBox 1
	fi
}

#右移一格
BoxRight()
{
	local x
	((x = boxCurX + 1))
	if BoxMove $boxCurY $x
	then
		DrawCurBox 0
		((boxCurX = x))
		DrawCurBox 1
	fi
}

#向下移一格
BoxDown()
{
	local y
	((y = boxCurY + 1))	#新的y坐标
	if BoxMove $y $boxCurX	#测试是否可以下落一行
	then
		DrawCurBox 0	#将旧的方块抹去
		((boxCurY = y))
		DrawCurBox 1	#显示新的下落后方块
	else
		#走到这儿, 如果不能下落了
		Box2Map		#将当前移动中的方块贴到背景方块中
		CreateBox	#产生新的方块
	fi
}

#下落到底
BoxAllDown()
{
	local y iDown

	#计算能够下落的行数
	iDown=0
	(( y = boxCurY + 1 ))
	while BoxMove $y $boxCurX
	do
		(( y++ ))
		(( iDown++ ))
	done

	DrawCurBox 0	#将旧的方块抹去
	((boxCurY += iDown))
	DrawCurBox 1	#显示新的下落后的方块
	Box2Map		#将当前移动中的方块贴到背景方块中
	CreateBox	#产生新的方块
}

#翻转
BoxRotate()
{
	[ ${boxStyle[$iBoxCurType]} -eq 1 ] && return
	((rotateStyle = (iBoxCurStyle + 1) % ${boxStyle[$iBoxCurType]}))
	#将当前方块保存到boxTmp
	boxTmp=( `eval 'echo ${boxCur[@]}'` )
	boxCur=( `eval 'echo ${box'$iBoxCurType'_'$rotateStyle'[@]}'` )

	if BoxMove $boxCurY $boxCurX	#测试旋转后是否有空间放的下
	then
		#抹去旧的方块
		boxCur=( `eval 'echo ${boxTmp[@]}'` )
		DrawCurBox 0

		boxCur=( `eval 'echo ${box'$iBoxCurType'_'$rotateStyle'[@]}'` )
		DrawCurBox 1
		iBoxCurStyle=$rotateStyle
	else
		#不能旋转，还是继续使用老的样式
		boxCur=( `eval 'echo ${boxTmp[@]}'` )
	fi
}

#准备下一个方块
PrepareNextBox()
{
	local i x y
	#清除右边预显示的方块
	if (( ${#boxNext[@]} != 0 )); then
		for ((i = 0; i < 8; i += 2))
		do
			((y = marginTop + 1 + ${boxNext[$i]}))
			((x = marginLeft + 2 * mapWidth + 7 + 2 * ${boxNext[$i + 1]}))
			echo -ne "\033[${y};${x}H\040\040"
		done
	fi

	#随机生成预显式方块
	(( iBoxNextType = RANDOM % iSumType ))
	(( iBoxNextStyle = RANDOM % ${boxStyle[$iBoxNextType]} ))
	(( iBoxNextColor = RANDOM % $iSumColor + 1 ))

	boxNext=( `eval 'echo ${box'$iBoxNextType'_'$iBoxNextStyle'[@]}'` )


	#显示右边预显示的方块
	echo -ne "\033[1m\033[3${iBoxNextColor}m\033[4${iBoxNextColor}m"
	for ((i = 0; i < 8; i += 2))
	do
		((y = marginTop + 1 + ${boxNext[$i]}))
		((x = marginLeft + 2 * mapWidth + 7 + 2 * ${boxNext[$i + 1]}))
		echo -ne "\033[${y};${x}H[]"
	done

	echo -ne "\033[0m"

}

#显示新方块
CreateBox()
{
	if (( ${#boxCur[@]} == 0 )); then
		#当前方块不存在
		(( iBoxCurType = RANDOM % iSumType ))
		(( iBoxCurStyle = RANDOM % ${boxStyle[$iBoxCurType]} ))
		(( iBoxCurColor = RANDOM % $iSumColor + 1 ))
	else
		#当前方块已存在, 将下一个方块赋给当前方块
		iBoxCurType=$iBoxNextType;
		iBoxCurStyle=$iBoxNextStyle;
		iBoxCurColor=$iBoxNextColor
	fi

	#当前方块数组
	boxCur=( `eval 'echo ${box'$iBoxCurType'_'$iBoxCurStyle'[@]}'` )
	#初始化方块起始坐标
	boxCurY=boxCur[8];
	boxCurX=boxCur[9];

	DrawCurBox 1		#绘制当前方块
	if ! BoxMove $boxCurY $boxCurX
	then
		kill -$sigExit $PPID
		ShowExit
	fi

	PrepareNextBox
	
}

#绘制边框
DrawBorder()
{
	clear

	local i y x1 x2
	#显示边框
	echo -ne "\033[1m\033[3${cBorder}m\033[4${cBorder}m"

	((x1 = marginLeft + 1))				#左边框x坐标
	((x2 = x1 + 2 + mapWidth * 2))			#右边框x坐标
	for ((i = 0; i < mapHeight; i++))
	do
		((y = i + marginTop + 2))
		echo -ne "\033[${y};${x1}H||"		#绘制左边框
		echo -ne "\033[${y};${x2}H||"		#绘制右边框
	done

	((x1 = marginTop + mapHeight + 2))
	for ((i = 0; i < mapWidth + 2; i++))
	do
		((y = i * 2 + marginLeft + 1))
		echo -ne "\033[${mapTop};${y}H=="	#绘制上边框
		echo -ne "\033[${x1};${y}H=="		#绘制下边框
	done
	echo -ne "\033[0m"

	#显示"Score"和"Level"字样
	echo -ne "\033[1m"
	((y = marginLeft + mapWidth * 2 + 7))
	((x1 = marginTop + 10))
	echo -ne "\033[3${cScore}m\033[${x1};${y}HScore"
	((x1 = marginTop + 11))
	echo -ne "\033[3${cScoreValue}m\033[${x1};${y}H${iScore}"
	((x1 = marginTop + 13))
	echo -ne "\033[3${cScore}m\033[${x1};${y}HLevel"
	((x1 = marginTop + 14))
	echo -ne "\033[3${cScoreValue}m\033[${x1};${y}H${iLevel}"
	echo -ne "\033[0m"
}

InitDraw()
{
	clear			#清屏
	DrawBorder		#绘制边框
	CreateBox		#创建方块
}

#退出时显示GameOVer!
ShowExit()
{
	local y
	((y = mapHeight + mapTop + 3))
	echo -e "\033[${y};1HGameOver!\033[0m"
	exit
}

#游戏主程序在这儿开始.
if [[ "$1" == "--version" ]]; then
	echo "$APP_NAME $APP_VERSION"
elif [[ "$1" == "--show" ]]; then
	#当发现具有参数--show时，运行显示函数
	RunAsDisplayer
else
	bash $0 --show&	#以参数--show将本程序再运行一遍
	RunAsKeyReceiver $!	#以上一行产生的进程的进程号作为参数
fi


keytest.sh



#!/bin/bash

GetKey()
{
	aKey=(0 0 0) #定义一个数组来保存3个按键

	cESC=`echo -ne "\033"`
	cSpace=`echo -ne "\040"`

	while :
	do
		read -s -n 1 key  #读取一个字符，将读取到的字符保存在key中
		#echo $key
		#echo XXX 

		aKey[0]=${aKey[1]} #第一个按键
		aKey[1]=${aKey[2]} #第二个按键
		aKey[2]=$key        #第三个按键

		if [[ $key == $cESC && ${aKey[1]} == $cESC ]]
		then
			MyExit
		elif [[ ${aKey[0]} == $cESC && ${aKey[1]} == "[" ]]
		then
			if [[ $key == "A" ]]; then echo KEYUP
			elif [[ $key == "B" ]]; then echo KEYDOWN
			elif [[ $key == "D" ]]; then echo KEYLEFT
			elif [[ $key == "C" ]]; then echo KEYRIGHT
			fi
		fi
	done
}

GetKey


draw.sh





#!/bin/bash

#位置与大小
marginLeft=8			#边框左边距
marginTop=6			#边框上边距
((mapLeft=marginLeft+2))	#棋盘左边距
((mapTop=$marginTop+1))		#棋盘上边距
mapWidth=10			#棋盘宽度
mapHeight=15			#棋盘高度


#方块定义，7大类19种样式
#前8位为方块坐标，后2位为方块刚出现的时候的位置
box0_0=(0 0 0 1 1 0 1 1 0 4)

box1_0=(0 1 1 1 2 1 3 1 0 3)
box1_1=(1 0 1 1 1 2 1 3 -1 3)

box2_0=(0 0 1 0 1 1 2 1 0 4)
box2_1=(0 1 0 2 1 0 1 1 0 3)

box3_0=(0 1 1 0 1 1 2 0 0 4)
box3_1=(0 0 0 1 1 1 1 2 0 4)

box4_0=(0 2 1 0 1 1 1 2 0 3)
box4_1=(0 1 1 1 2 1 2 2 0 3)
box4_2=(1 0 1 1 1 2 2 0 -1 3)
box4_3=(0 0 0 1 1 1 2 1 0 4)

box5_0=(0 0 1 0 1 1 1 2 0 3)
box5_1=(0 1 0 2 1 1 2 1 0 3)
box5_2=(1 0 1 1 1 2 2 2 -1 3)
box5_3=(0 1 1 1 2 0 2 1 0 4)

box6_0=(0 1 1 0 1 1 1 2 0 3)
box6_1=(0 1 1 1 1 2 2 1 0 3)
box6_2=(1 0 1 1 1 2 2 1 -1 3)
box6_3=(0 1 1 0 1 1 2 1 0 4)


#绘制边框
DrawBorder()
{
	clear

	local i y x1 x2
	#显示边框
	echo -ne "\033[1m\033[32m\033[42m"

	((x1 = marginLeft + 1))				#左边框x坐标
	((x2 = x1 + 2 + mapWidth * 2))			#右边框x坐标
	for ((i = 0; i < mapHeight; i++))
	do
		((y = i + marginTop + 2))
		echo -ne "\033[${y};${x1}H||"		#绘制左边框
		echo -ne "\033[${y};${x2}H||"		#绘制右边框
	done

	((x1 = marginTop + mapHeight + 2))
	for ((i = 0; i < mapWidth + 2; i++))
	do
		((y = i * 2 + marginLeft + 1))
		echo -ne "\033[${mapTop};${y}H=="	#绘制上边框
		echo -ne "\033[${x1};${y}H=="		#绘制下边框
	done
	echo -ne "\033[0m"
}

DrawBox()
{
	local i x y xPos yPos
	yPos=${box0_0[8]}
	xPos=${box0_0[9]}
	echo -ne "\033[1m\033[35m\033[45m"
	for ((i = 0; i < 8; i += 2))
	do
		(( y = mapTop + 1 + ${box0_0[$i]} + yPos ))
		(( x = mapLeft + 1 + 2 * (${box0_0[$i + 1]} + xPos) ))
		echo -ne "\033[${y};${x}H[]"
	done
	echo -ne "\033[0m"
}

InitDraw()
{
	clear			#清屏
	DrawBorder		#绘制边框
	DrawBox
	while :
	do
		sleep 1
	done
}

InitDraw

```

