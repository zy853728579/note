git框图

![image-20250219102116892](git命令学习/git框图.png)

### gitLab说明

```shell
本地git地址：http://192.168.1.210:8888/
用户名: 853728579@qq.com
密码: ~ABcd5845201314
```



### git命令学习

```shell
合并add 和commit
git commit -am "注释"

#从git服务器复制到本地目录
git clone https://gitee.com/zy853728579/serial-port.git ~/qt_project/serialPort

#查看分支情况
git branch
#新建分支（新建完可以查看一下git branch）
git branch <你的新分支名称>
#切换到新的分支( -b 命令：表示新建分支并将分支切换到新分支上)
git checkout -b <你的新分支名称>  //例如： git checkout -b test-branch

#将本地文件上传到git服务器
git status
git add .
git commit -m "modify"
git push     #zy853728579   ABcd

#将本地文件上传到新分支上
git push origin <你的新分支名称>

#放弃本地修改，直接覆盖
git reset --hard
git pull

#删除分支
git branch -d <分支名称>

#合并分支
#假如在C分支
#先切换到A分支
git checkout A
/#此时在A分支，合并A，C分支
git merge C

#开发分支应用场景
#----------------1-------------------------------------
#开发分支（dev）上的代码达到上线标准后，合并到 master 分支
git checkout dev
git pull
git checkout master
git pull
# merge  --no-ff参数，表示禁用Fast forward;可以保存之前的分支历史。能够更好的查看merge历史，以及branch状态.
#保证版本提交、分支结构清晰
git merge --no-ff  dev
git push -u origin master
#----------------2-------------------------------------
#当master 分支为保护分支时，执行git push -u origin master会提示远程服务器拒绝，此时需要在浏览器进行远程仓库merge操作。

##当master代码改动，需要更新开发分支（dev）上的代码
git checkout master 
git pull 
git checkout dev
# merge  --no-ff参数，表示禁用Fast forward;可以保存之前的分支历史。能够更好的查看merge历史，以及branch状态.
#保证版本提交、分支结构清晰
git merge --no-ff  master
git push -u origin dev
```

checkout :放弃修改，放弃的是 工作区 中的修改，相对于暂存区或者对象区

reset:将之前增加到的暂存区的内容回退到  工作区

##### 设置邮箱和用户名

```shell
git config --list     #查看配置

git config --system   #(基本不用),给整个计算机一次性设置
#配置文件在git的安装目录下的mingw64\etc\.gitconfig

----------------------------------------------------
git config --global   #给当前用户一次性设置
#配置文件存放在 C:\Users\85372\.gitconfig

-----------------------------------------------------
git config --local    #给当前项目一次性设置, 这个优先级最高
#配置文件在当前项目下的.git目录的config文件
git config --local user.name 'zhangya'
git config --local user.email '853728579@qq.com'
```

##### 删除文件

```shell
git rm <file...>  #运行这个命令后，文件会被放到 暂存区, 需要彻底删除，需运行 git commit
#真正删除
   git commit -m "删除文件"  #运行这个命令才是真正的删除文件
#后悔删除
   1 - git restore --staged <file...>  #恢复到工作区
   2 - git restore <file...>  #丢弃改动
```

##### 文件重命名

```shell
git mv <原文件名> <新文件名>	#重命名后，如果需要撤回命名，后面会出现两个文件
```

##### git文件比较命令    git diff

工作区与暂存区

```shell
git diff  #查看工作区与暂存区之间所有文件差异
git diff -- 文件名1 文件名2 文件名3  #查看多个文件在工作区与暂存区之间的差异
git diff > output.txt  #工作区与暂存区之间的差异并将不同点写入到output.txt
```

工作区与本地仓库

```shell
git diff HEAD #查看工作区与最新版本库之间的所有的文件差异
git diff 具体某个版本 #查看工作区与具体某个提交版本之间的所有的文件差异
git diff HEAD -- 文件名 #查看工作区与最新版本库之间的 指定文件名的文件差异
git diff HEAD -- 文件名1 文件名2 文件名3 #查看工作区与最新版本库之间的 指定文件名的多个文件差异
git diff 具体某个版本 -- 文件名 #查看工作区与具体某个版本之间的 指定文件名的文件差异
git diff 具体某个版本 -- 文件名1 文件名2 文件名3 #查看工作区与最具体某个版本之间的 指定文件名的多个文件差异
```

暂存区与本地仓库

```shell
git diff --cached #查看暂存区和 上一次提交 的最新版本(HEAD)之间的所有文件差异
git diff --cached 版本号 #查看暂存区和 指定版本 之间的所有文件差异
git diff --cached -- 文件名1 文件名2 文件名3 #查看暂存区和 HEAD 之间的指定文件差异
git diff --cached 版本号 -- 文件名1 文件名2 文件名3 #查看暂存区和 指定版本 之间的指定文件差异
```

不同版本之间比较

```shell
git diff 版本号1 版本号2 #查看两个版本之间的差异
git diff 版本号1 版本号2 -- 文件名1 文件名2 #查看两个版本之间的指定文件之间的差异
git diff 版本号1 版本号2 --stat  #查看两个版本之间的改动的文件列表
git diff 版本号1 版本号2 src #查看两个版本之间的文件夹 src 的差异
```

两个分支之间比较

```shell
git diff 分支1 分支2   #查看两分支之间的差异

```

##### 注释重写

```shell
git log #查看日志
git log -1 #查看最近一次日志
git log --graph
git log --graph --pretty=oneline --abbrev-commit

##修改日志说明
git commit --amend -m "修改"
```

##### 添加可忽略文件

```shell
touch .gitignore
vim .gitignore    #将需要忽略的文件添加到这个文件中即可

#通配符
*     	--- 表示任意字符
!  		--- 除了某些文件
dir/	--- 不添加dir目录下的所有文件
dir/*.txt---不添加dir目录下的所有txt文件
dir/*/*.txt ---只能是一级目录
dir/**/*.txt ---任意级目录
空目录  -- 默认是不添加
```

##### 标签

```shell
git tag 标签名称   #创建一个标签
```

### 分支

```shell
git branch   #查看分支
git branch -v
#创建新分支
git branch new_branch
git checkout -b <分支名> #创建并切换分支
#切换分支
git checkout <分支名>

#删除分支
	git checkout -d <分支名> #删除分支,不能删除当前所在的分支上
		#其他不能删除的情况： 包含 “未合并” 的内容，删除之前先合并
	git checkout -D <分支名> #强制删除分支
#分支重命名
	git branch -m master master2 #将master改名为masetr2
#合并分支
	git merge <其他分支名>   #将其他分支合并到当前分支上,这个fast forward模式，两个分支处理同一commit id号,会丢失分支信息
	git merge --no-ff <其他分支名> #不使用fast forward模式，合并后不使用同一个commit id号，会再创建一个新的commit id号， 两个分支不会归于一点，不丢失分支信息

#将某个分支的某次提交合并到另一个分支
	git checkout dev #切换到dev分支
	git log  #查看需要合并的commit记录，如commitID: 7fcb3defff;
	git checkout master #切换到master分支
	git cherry-pick 7fcb3defff  #合并dev分支的7fcb3defff记录到master分支上

```

##### 版本穿梭，在多个commit之间穿梭

```shell
git reset --hard HEAD^^    #回退到上二次commit
git reset --hard HEAD~n		#回退到上n次commit
git reset --hard <commit ID> #回退到任意sha1值

git reflog  #查看所有记录,记录所有操作，再用  git reset --hard <commit ID> 回退出任意步骤
```

##### 保存现场

```shell
git stash   #保存现场，后可以切换分支
git checkout <其他分支>
#如果其他分支修改完成后，可以切到这个分支上，再进行恢复现场
git stash pop
git stash apply  #还原内容，不删除原保存内容
	#手动删除保存现场记录， git stash drop stash@{0}
#查看保存现场分支有哪些
git stash list
```

##### submodule:某个git项目A中包含子项目B，单向操作

```shell

```

##### substree ： 某个git项目A中包含子项目B，双向操作

```shell
#创建父工程项目  parent
mkdir parent
cd parent
git init
echo "parent project" > a.txt
git add .
git commit -m "parent project init"
git remote add origin [父工程远程仓库地址]  #指定父工程的远程地址，也就是相当于给  远程仓库地址  起个一个origin名称,这也只需要配置一次，后续可以不用配置
git push -u origin master   #将当前项目的master分支推送到远端，这个只需要配置第一次，后面推送可直接用git push

#创建子工程项目  subtree
mkdir subtree
cd subtree
git init
echo "subtree paroject" > b.txt
git add b.txt
git commit -m "subtree project init"
git remote add origin [子工程远程仓库地址]  #指定子工程的远端地址
git push -u origin master

#在父工程中添加子工程
cd parent
git remote add subtree-origin [子工程远程仓库地址]  #在父工程中配置子工程远程仓库地址的名称
git subtree add -P subtree subtree-origin master  #在父工程中添加子工程，第一个subtree为git命令
git subtree add -P subtree2 subtree-origin master --squash #在父工程中添加子工程2， --squash减少commit次数，凡事与git subtree 命令后如果加了squash，以后每次都加，如果没加，以后每次都不加
git push -u origin master  #将工程和子工程都推向远端,后面都需要添加origin master,因为这个项目中有多个远端地址

#子工程 subtree有更新数据，父工程如何感知子工程的变化
git subtree pull -P subtree subtree-origin master  #父工程去拉取子工程中的数据到本地
git push  #将父工程推送到远端
#核心流程
	# 子工程中修改 -> 父工程感知子工程有反应
	# 1、修改子工程文件上，并且push到远端
	# 2、将远端的子工程更新到父工程的本地项目上
	# 3、父工程的子模块有更新情况，推送到对应的 远端地址

#修改父工程的子工程，再将这个父工程的子工程更新到真正的子工程中
git subtree push -P subtree subtree-origin master

#测试
https://gitee.com/zy853728579/parent.git  #父工程远程地址
https://gitee.com/zy853728579/name_subtree.git  #子工程远程地址

```

##### 给远程仓库起名字

```shell
git remote add [名称] [远程仓库地址]  #名称：可以自己定义

```

##### 将当前分支写好的数据转移到另一个分支

```shell
#cherry-pick :如果写了一半（已经提交），发现写错分支，需要将已提交的commit转移到另一个分支，
	#1、每次只能转移一个commit
	#2、内容会被复制，sha1值也会变
	#3、在复制的时候，不要跨commit ID节点复制
#注意：如果在dev分支中写错了，需要将dev分支中的内容移到master分支上去，需要先切换到master分支上，再在master分支上进行cherry-pick动作
git cherry-pick <commit ID>   #每次只能转移一个commit ID
#转移完成后，将写错分支的内容需要删除
git checkout <commit ID>  #可以在原先写错分支上进行退回，想要退回到哪一步，就填入该步的commit ID值
git branch -D dev  #删除dev分支
git checkout -b dev  #再新建dev分支

#rebase  变基 （衍合），一次将后面所有的commit ID全移过去，与cherry-pick的操作点不一样
	# 1、rebase解决冲突流程： vim修改冲突位置 ----> git add . -----> git rebase --continue
	# 2、rebase忽略冲突(放弃rebase所在分支的修改，直接使用其他分支的): git rebase --skip
	# 3、如果觉得rebase修改错误的，那行可以终止，还原成rebase之前的场景，git rebase --abort
	# 4、建议：rebase分支，只在本机操作，不要推送到远端， 不要在master分支上进行
#注意：操作点与cherry-pick不一样，如果在dev分支中写错了，需要将dev分支中的内容移到master分支上去，那么可以直接在dev分支中将需要移动的commit ID值一次转移过去
git rebase <转移的分支名>
```



### linux命令学习

当命令输入很长有些命令需更改时

ctrl+A --- 回到命令的首

ctrl+E-----回到命令的尾



### ubuntu 22.04命令

```shell
lsb_release -a  #查看系统版本及说明

```



#### 开机默认进入命令行模式

```shell
#开机进入命令行模式
sudo systemctl set-default multi-user.target
reboot
#要进入图形界面,只需要输入命令 startx
#从图形界面切换回命令行： ctrl + alt + F7

#开机默认进入图形用户界面
sudo systemctl set-default graphical.target
reboot
#要进入命令行模式: ctrl + alt + F2
#从命令行切换到图形界面: ctrl + alt + F7
```

