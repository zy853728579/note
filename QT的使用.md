#### 网文链接

```shell
#Qt浅谈之总结(整理)  
https://blog.csdn.net/taiyang1987912/article/details/32713781
#整理：Qt编写窗体应用时用到的一些功能
https://blog.csdn.net/alazyperson/article/details/111823671
#ubuntu18.04 安装qt5.12.8及环境配置的详细教程
https://www.jb51.net/article/186984.htm
#qt网络安装说明(这个说明文档不错)
https://blog.csdn.net/YMGogre/article/details/130565726
#qt官网下载  853728579@qq.com   abcd5845201314
https://www.qt.io/download-qt-installer-oss?hsCtaTracking=99d9dd4f-5681-48d2-b096-470725510d34%7C074ddad0-fdef-4e53-8aa8-5e8a876d6ab4

https://blog.csdn.net/falwat/article/details/130330830 #qt 动态库的创建和使用教程(step by step)
```

### qt文件打包

```shell
windeployqt xxx.exe  #将xxx.exe文件进行打包

https://blog.csdn.net/qq_44094415/article/details/121889028 #【QT】QT生成.exe安装包详细全文（保姆级教程）--打包软件及问题大全
https://www.cnblogs.com/njabsky/p/14054895.html  # NSIS 创建快捷方式

#NSIS打包快捷配置图标配置，需要将图标文件复制到安装目录下
CreateShortCut "$DESKTOP\TestExe.lnk" "$INSTDIR\TestExe.exe" "" "$INSTDIR\logo_64x64.ico" 0

```

### pro文件使用说明

##### 网文链接

```shell
#qt的pro文件上配置说明
https://blog.csdn.net/springvil/article/details/144325423
```

##### 配置说明

```shell
#仅增加日期（推荐方法，但每天需要重新打开该工程一次，确保更新环境变量）
TARGET = sample-$$system(echo "%data:~0,4%%data:~5,2%%date:~8,2%")
#增加时间（不推荐，会导致当天调试困难）
TARGET = sample-$$system(echo "%data:~0,4%%data:~5,2%%date:~8,2%")_$$system(echo "%time:~0,2%%time:~3,2%").exe
```

### QT安装配置

#### QT安装配置图

![](QT的使用/qt安装配置说明图.jpg)

#### QT终端输入安装（自动安装最新版本）

```
sudo apt-get update
sudo apt-get install qt5-default qtcreator
```

#### QT官网安装包下载安装

官网地址：https://download.qt.io/archive/qt/

下载完成后安装

```
sudo chmod +x qt-opensource-linux-x64-x.xx.x.run
sudo ./qt-opensource-linux-x64-x.xx.x.run
```

### QT控件说明

##### QLineEdit

```c++
//限制输入内容
//1.输入int类型
QLineEdit * edit = new QLineEdit(this);
edit->setValidator(new QIntValidator(100, 999, this));
//输入double类型
//bottom为最小范围，top为最大范围，decimal为精确度，精确到第几位小数
QLineEdit *edit=new QLineEdit(this);
edit->setValidator(new QDoubleValidator(double bottom,double top,int decimal,this));
//输入任意匹配类型
QRegExp regExp(“[A-Za-z][1-9][0-9]{0,2}”);//正则表达式，第一位数字为a-z的数字，第二位为1-9的数字，后边是0-2位0-9的数字
lineEdit->setValidator(new QRegExpValidator(regxp,this));
```

##### QComboBox

```c++
//--------设置QComboBox下拉框子项的高度
setStyleSheet("QComboBox QAbstractItemView:item {font-size:12px; min-height:50px; min-width:20px;  }");
//方法一：
QListView* listView = new QListView();
ui->comboBox_1->setView(listView);
//ui->comboBox_2->setView(listView); 不能再使用到第二个comboBox上，下面的可以
//方法二：
QStyledItemDelegate* styledItemDelegate = new QStyledItemDelegate();
ui->comboBox_2->setItemDelegate(styledItemDelegate);
ui->comboBox_3->setItemDelegate(styledItemDelegate);
```

##### horizontalSlider

样式图

![](QT的使用//horizontalSlider_001.png)

效果5

![](QT的使用//horizontalSlider_002.png)

```c++
//-----------水平的QSlider

QSlider
{
    background-color: #363636; 
	border-style: outset; 
	border-radius: 10px; 
}
 
QSlider::groove:horizontal
{
	height: 12px;
	background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
	margin: 2px 0
}
 
QSlider::handle:horizontal 
{
	background: QRadialGradient(cx:0, cy:0, radius: 1, fx:0.5, fy:0.5, stop:0 white, stop:1 green);
	width: 16px;
	height: 16px;
	margin: -5px 6px -5px 6px;
	border-radius:11px; 
	border: 3px solid #ffffff;
}

//---------------竖直的QSlider

QSlider 
{
	background-color: rgba(22, 22, 22, 0.7);
	padding-top: 15px;  /*上面端点离顶部的距离*/
	padding-bottom: 15px;
	border-radius: 5px; /*外边框矩形倒角*/
}
 
QSlider::add-page:vertical 
{
	background-color: #FF7826;
	width:5px;
	border-radius: 2px;
}
 
QSlider::sub-page:vertical 
{
	background-color: #7A7B79;
	width:5px;
	border-radius: 2px;
}
 
QSlider::groove:vertical 
{
	background:transparent;
	width:6px;
}
 
QSlider::handle:vertical    
{
	height: 14px;  
	width: 14px;
	margin: 0px -4px 0px -4px;
	border-radius: 7px;
	background: white;
}

//--------效果2：增加图片

#horizontalSlider::groove:horizontal {
border: 1px solid gray;
border-radius:2px;
height: 4px;
left: 12px; right: 12px;
}

/*滑条*/
#horizontalSlider::handle:horizontal {
border: 1px solid gray;
border-image:url(":/image/images/b.png");
width: 20;
margin: -5px -5px -5px -5px;
}

/*已经划过的*/
#horizontalSlider::sub-page:horizontal{
background: #D1CE21;
}

//----------效果3

QSlider::groove:horizontal {  
border: 1px solid #bbb;  
background: white;  
height: 10px;  
border-radius: 4px;  
}  
  
QSlider::sub-page:horizontal {  
background: qlineargradient(x1: 0, y1: 0.2, x2: 1, y2: 1,  
    stop: 0 #bbf, stop: 1 #55f);  
border: 1px solid #777;  
height: 10px;  
border-radius: 4px;  
}  
  
QSlider::add-page:horizontal {  
background: #fff;  
border: 1px solid #777;  
height: 10px;  
border-radius: 4px;  
}  
  
QSlider::handle:horizontal {  
background: qlineargradient(x1:0, y1:0, x2:1, y2:1,  
    stop:0 #eee, stop:1 #ccc);  
border: 1px solid #777;  
width: 13px;  
margin-top: -2px;  
margin-bottom: -2px;  
border-radius: 4px;  
}  
  
QSlider::handle:horizontal:hover {  
background: qlineargradient(x1:0, y1:0, x2:1, y2:1,  
    stop:0 #fff, stop:1 #ddd);  
border: 1px solid #444;  
border-radius: 4px;  
}  
  
QSlider::sub-page:horizontal:disabled {  
background: #bbb;  
border-color: #999;  
}  
  
QSlider::add-page:horizontal:disabled {  
background: #eee;  
border-color: #999;  
}  
  
QSlider::handle:horizontal:disabled {  
background: #eee;  
border: 1px solid #aaa;  
border-radius: 4px;  
}  

//--------效果4:

/* 一定要先设置groove，不然handle的很多效果将没有*/
QString sliderstyle = QString("QSlider::groove:horizontal{border:none; height:12px; border-radius:6px; background:rgba(0, 19, 39, 100);}"
                              /* 上下边距和左右边距*/
                              "QSlider::handle:horizontal{border:none; margin:-5px 0px; width:22px; height:22px; border-radius:20px;"
                                                         "border-image:url(:/new/prefix1/64.png);}"
                             /*划过部分*/
                             "QSlider::sub-page:horizontal{background:rgba(0, 19, 39, 100); border-radius:6px;}"
                             /*未划过部分*/
                             "QSlider::add-page:horizontal{background:rgba(0, 19, 39, 100); border-radius:6px;}");
        ui->horizontalSlider->setStyleSheet(sliderstyle);

//-----效果5：

    "QSlider::groove:horizontal{border:none; height:73px; border-radius:6px; border-image:url(:/new/prefix1/b1.png);}"
    /* 上下边距和左右边距*/
    "QSlider::handle:horizontal{border:none; margin:20px 0px; width:22px; height:22px; border-radius:20px;"
                               "border-image:url(:/new/prefix1/ZZ64.png);}"
    /*划过部分*/
    "QSlider::sub-page:horizontal{background:rgba(0, 19, 39, 0); border-radius:6px;}"
    /*未划过部分*/
    "QSlider::add-page:horizontal{background:rgba(0, 19, 39, 0); border-radius:6px;}"
    );
```

##### QPropertyAnimation（动画类）

```c++
//定义
QPropertyAnimation* _pAnimation= new QPropertyAnimation(this, "geometry");
//设置动画持续时间
_pAnimation _pAnimation->setDuration(200);
//开始位置
_pAnimation->setKeyValueAt(0, QRect(rect().left(), rect().top(), rect().width(), rect().height()));
//结束位置
 _pAnimation->setKeyValueAt(1, QRect(rect().left(), -rect().height(), rect().width(), rect().height()));
//开始动画效果
 _pAnimation->start();

QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
animation->setDuration(10000);
//开始的位置
animation->setStartValue(QRect(0, 0, 100, 30));
//结束的位置
animation->setEndValue(QRect(250, 250, 100, 30));
/*  设置动画效果  */  
animation->setEasingCurve(QEasingCurve::OutInExpo);
//一直运动直到stop.
pAnimation->setLoopCount(-1);
animation->start();
```

### QT的.pro文件格式

​	Qt += core gui

​	greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#需要加串口

​	QT       += core  gui  serialport  xml

​	greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

\#输出文件名称

​	TARGET = SerialPort

#### .pro文件说明

```shell
#添加全局预编译宏定义
DEFINES += PLATFORM_TYPE
#或者
DEFINES += PLATFORM_TYPE=VALUE   #  DEFINES += PI_MY=3.1415926

#输出可执行文件名称
TARGET = test
#输出可执行文件的路径
DESTDIR = /home/zhangya/test
CONFIG += c++11
#添加文件目录
INCLUDE_PATH = /home/zhangya
INCLUDEPATH +=　$${INCLUDE_PATH}/include
#添加版本号
VERSION = 1.0.0
#Windows
win32{
    message("Windows 32 bit [win32]")
}
#使用宏定义判断
if (contains(DEFINES, PLATFORM_TYPE)){    #contains和{必须在同一行，否则报错

}else{

}
#或者
#定义条件编译宏LAOER
DEFINES += LAOER
#依赖编译宏LAOER的编译选项：
contains(DEFINES, LAOER){
	message(hello Laoer)
}
#与编译宏LAOER冲突的编译选项：
!contains(DEFINES, LAOER){
	message(hello Laoda)
}

QT += core gui serialport multimedia multimediawidgets
greaterThan(QT_MAJOR_VERSION, 4):QT += widgets
```

### QT类型转换

C++的reinterpret_cast，static_cast，dynamic_cast和const_cast是c++强制转换，常规转换都可用static_cast

​	A>    const_cast从const变量中删除const/volatile属性。
​	B>    dynamic_cast在多态类型之间转换时执行运行时有效性检查
​	C>     static_cast在继承层次结构中执行上/下转换，但不进行运行时检查，或显式执行可能隐式的转换(例如，从float到int)
​	D>    reinterpret_cast在不相关的类型之间进行转换。
​	E>     qobject_cast动态转换QObject类的类型

#### 数据转换

```c++
//-------------------------------------------------
//char int, float , double等转成十进制或十六进制字符串
char value = 100;
QString::number(value, 10); //十进制
QString::number(value, 16); //十六进制
//方法二
QString s = QString("%1").arg(value);
//-------------------------------------------------
//QByteArray转成十六进制字符
QByteArray byteArray;
QString tempStr = byteArray.toHex().data();
//每两个字符以空格隔开
QString outStr = "";
for(int i=0; i<tempStr.length(); i+=2)
    outStr += tempStr.mid(i, 2) + " ";
//--------------------------------------------------
//字符串转QByteArray
QByteArray byteArray;
QString outStr = str;
//去掉字符串的空格
outStr.remove(QRegExp("\\s"));
for (int index=0; index<outStr.length(); index+=2)
{
    QString tempStr = outStr.mid(index, 2);
    byteArray.append(static_cast<char>(tempStr.toInt(nullptr, 16)));
}
//--------------------------------------------------
//QString 转换为 int类型
Qstring str="FF";  
bool ok;  
int dec=str.toInt(&ok,10); //dec=255 ; ok=true    //QString 转 int
int hex =str.toInt(&ok,16); //hex=255;ok=true;    //QString 转 int
//QString 转换为 double类型
QString str="123.45";  
double val=str.toDouble(); //val=123.45  //QString 转 double
bool ok;   double d;  
d=QString("1234.56e-02").toDouble(&ok); //ok=true;d=12.3456
//------------------------------------------------------
//QByteArray 转 QString
QByteArray byte;
QString string;
string = QString(byte);
//或
string = QString::fromLatin1(byte);
//QString  转 QByteArray
QByteArray byte;
QString string;
byte = string.toAscii();
//or
byte = string.toLatin1();
//----------------------------------------------------------
//当前时间转QString
QString b_dateTime;
b_dateTime.append(tr("%1").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")));
//QString转时间
QString strBuffer;
QDateTime time;
strBuffer = "2010-07-02 17:35:00";
time = QDateTime::fromString(strBuffer, "yyyy-MM-dd hh:mm:ss");
//------------------------------------------------------------
//QByteArray 中的两个字节合并成一个低八位一个高八位数据
//方法一
int QByteArray2int(quint8 highData, quint8 lowData) {
    return (int)highData * 256 + (int)lowData;
}
QByteArray byte;
QByteArray2int(byte.at(0),byte.at(1));
//方法二
QByteArray handle_Data;  
//  byte0为高8位
// 取出对应位的16进制char型之后转换为16进制的int型（强制转换）
int data1= (unsigned char)handle_Data.at(0);  
//  byte1为低8位
// 取出对应位的16进制char型之后转换为16进制的int型（强制转换）
int data2= (unsigned char)handle_Data.at(1);  
int data_sum= data1<<8|data2;//移位8位是一个字节
// 将16进制data_sum转换为string类型的10进制数便于在text上显示
QString  data_string= QString::number(data_sum,10);
//------------------------------------------------------------
//QString 转 string
QString.toStdString();
//string 转 QString
QString::fromStdString(string);
```

### windows下的U盘检测程序

mainform.h

```c++
#ifndef MAINFORM_H
#define MAINFORM_H

#include <QWidget>
#include <windows.h>
#include <QDebug>
#include <dbt.h>

namespace Ui {
class MainForm;
}

class MainForm : public QWidget
{
    Q_OBJECT

public:
    explicit MainForm(QWidget *parent = nullptr);
    ~MainForm();

protected:
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);

private:
    Ui::MainForm *ui;
    char FirstDriveFromMask (ULONG unitmask);
};

#endif // MAINFORM_H
```

mainform.cpp

```c++
#include "mainform.h"
#include "ui_mainform.h"


MainForm::MainForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);
}

MainForm::~MainForm()
{
    delete ui;
}

bool MainForm::nativeEvent(const QByteArray &, void *message, long *)
{
    Q_ASSERT(message != nullptr);

    MSG * msg = static_cast<MSG *>(message);
    UINT msgType = msg->message;

    if(msgType == WM_DEVICECHANGE)
    {
        qDebug() << "收到事件 " ;
        PDEV_BROADCAST_HDR lpdb = reinterpret_cast<PDEV_BROADCAST_HDR>(msg->lParam);
        switch(msg->wParam)
        {
        case DBT_DEVICETYPESPECIFIC:
            {
                qDebug() << "DBT_DEVICETYPESPECIFIC ";
                break;
            }
        case DBT_DEVICEARRIVAL:
            if (lpdb -> dbch_devicetype == DBT_DEVTYP_VOLUME)
            {
                PDEV_BROADCAST_VOLUME lpdbv = reinterpret_cast<PDEV_BROADCAST_VOLUME>(lpdb);
                if (lpdbv -> dbcv_flags == 0)
                {
                    // 插入U盘，此处可以做你想做的事
                    QString USBDisk = QString(this->FirstDriveFromMask(lpdbv ->dbcv_unitmask));
                    qDebug() << "USB_Arrived and The USBDisk is: "<<USBDisk;
                }
            }
            qDebug() << "DBT_DEVICEARRIVAL";
            break;
        case DBT_DEVICEREMOVECOMPLETE:
            if (lpdb -> dbch_devicetype == DBT_DEVTYP_VOLUME)
            {
                PDEV_BROADCAST_VOLUME lpdbv = reinterpret_cast<PDEV_BROADCAST_VOLUME>(lpdb);
                if (lpdbv -> dbcv_flags == 0)
                {
                    qDebug() << "USB_设备移除";
                }
            }
            qDebug() << "DBT_DEVICEREMOVECOMPLETE" ;
            break;
        }
    }

    return false;
}

char MainForm::FirstDriveFromMask (ULONG unitmask)
{
    char i;

    for (i = 0; i < 26; ++i)
    {
        if (unitmask & 0x1)
            break;
        unitmask = unitmask >> 1;
    }
    return (i + 'A');
}
```

main.cpp

```c++
#include <QApplication>
#include "mainform.h"

int main(int argc, char ** argv)
{
    QApplication app(argc, argv);

    MainForm * mf = new MainForm();
    mf->show();

    return app.exec();
}
```

### linux下的U盘检测程序

udiskqthread.h

```c++
#ifndef UDISKQTHREAD_H
#define UDISKQTHREAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/un.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/unistd.h>
#include <sys/types.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <errno.h>
#include <QThread>
#include <QDebug>
#include <QProcess>

class UDiskQThread : public QThread
{
    Q_OBJECT

public:
    UDiskQThread(QObject * parent = nullptr);
    ~UDiskQThread();
    void run(void);

signals:
    //status  -> 0:add 1:remove
    void UDiskStatusChange(int status,QString udiskFilePath);

private:
    int init_hotplug_sock(void);

private:
    QProcess *myProcess;
    int hotplug_sock;
    int udisk_count;
};

#endif // UDISKQTHREAD_H

```

udiskqthread.cpp

```c++
#include "udiskqthread.h"

#define UEVENT_BUFFER_SIZE (2048)

UDiskQThread::UDiskQThread(QObject *)
{
    hotplug_sock = init_hotplug_sock();
    udisk_count = 0;
}

UDiskQThread::~UDiskQThread()
{

}

int UDiskQThread::init_hotplug_sock()
{
    struct sockaddr_nl snl;
    const int buffersize = 16 * 1024 * 1024;
    int retval;
    memset(&snl, 0x00, sizeof(struct sockaddr_nl));
    snl.nl_family = AF_NETLINK;
    snl.nl_pid = getpid();
    snl.nl_groups = 1;
    int hotplug_sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (hotplug_sock == -1) {
        printf("error getting socket: %s", strerror(errno));
        return -1;
    }
    /* set receive buffersize */
    setsockopt(hotplug_sock, SOL_SOCKET, SO_RCVBUFFORCE, &buffersize, sizeof(buffersize));
    retval = bind(hotplug_sock, (struct sockaddr *) &snl, sizeof(struct sockaddr_nl));
    if (retval < 0) {
        printf("bind failed: %s", strerror(errno));
        close(hotplug_sock);
        hotplug_sock = -1;
        return -1;
    }

    return hotplug_sock;
}

void UDiskQThread::run()
{
    char buf[UEVENT_BUFFER_SIZE*2] = {0};
    QString msg;

    fcntl(hotplug_sock, F_SETFL, fcntl(hotplug_sock, F_GETFL, 0));
    while(1)
    {
        int len = read(hotplug_sock,buf,UEVENT_BUFFER_SIZE*2);
        if(len < 0)
            continue;

        msg = QString::fromLocal8Bit(buf,len);
        if(msg.startsWith("add"))
        {
            if(msg.contains("/block/sd") && (msg.mid(msg.indexOf("/block/sd")+10, 3) == "/sd"))
            {
                QProcess::execute("chmod 777 /mnt");
                QString filepath = QString("mkdir -p -m 777 /mnt/%1").arg(msg.mid(msg.indexOf("/block/sd")+11, 4));
                qDebug()<<__PRETTY_FUNCTION__<<__LINE__<<filepath;
                QProcess::execute(filepath);
                QString shell_method = QString("mount -o iocharset=utf8 -t vfat /dev/%1 /mnt/%2").arg(msg.mid(msg.indexOf("/block/sd")+11, 4)).arg(msg.mid(msg.indexOf("/block/sd")+11, 4));
                qDebug()<<__PRETTY_FUNCTION__<<__LINE__<<shell_method;
                QProcess::execute(shell_method);
                udisk_count++;
                emit UDiskStatusChange(0,QString("/mnt/%1").arg(msg.mid(msg.indexOf("/block/sd")+11, 4)));

                continue;
            }
        }
        else if(msg.startsWith("remove"))
        {
            if(msg.contains("/block/sd") && msg.mid(msg.indexOf("/block/sd")+10, 3) == "/sd")
            {
                qDebug()<<__PRETTY_FUNCTION__<<__LINE__<<"signal removeUDisk send";
                QString shell_method = QString("umount /mnt/%1").arg(msg.mid(msg.indexOf("/block/sd")+11, 4));
                qDebug()<<__PRETTY_FUNCTION__<<__LINE__<<shell_method;
                QProcess::execute(shell_method);

                QString filepath = QString("rm -rf /mnt/%1").arg(msg.mid(msg.indexOf("/block/sd")+11, 4));
                qDebug()<<__PRETTY_FUNCTION__<<__LINE__<<filepath;
                QProcess::execute(filepath);

                emit UDiskStatusChange(1,QString("/mnt/%1").arg(msg.mid(msg.indexOf("/block/sd")+11, 4)));
            }
        }
    }
}

```

初始化启动该线程程序

```c++
//在源文件中添加这些代码
UDiskQThread *thread1 = new UDiskQThread;
thread1->start();
//创建一个连接函数
this->connect(thread1, &UDiskQThread::UDiskStatusChange, this, &MainWidget::DiskStatusChangeSlot);

//当检测到有U盘插入或者拔出时，系统会调用该函数
void MainWidget::DiskStatusChangeSlot(int status, QString)
{

}

//在头文件中添加这些代码
#include "udiskqthread.h"

private slots:
    void DiskStatusChangeSlot(int status, QString path);
```

#### 更新版本

udiskqthread.h

```c++
#ifndef UDISKQTHREAD_H
#define UDISKQTHREAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/un.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/unistd.h>
#include <sys/types.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <errno.h>
#include <QThread>
#include <QDebug>
#include <QProcess>
#include <QRegExp>
#include <QFile>

class UDiskQThread : public QThread
{
    Q_OBJECT

public:
    enum DiskStatus{
      UDISK_ADD,
      UDISK_REMOVE,
      UDISK_ERR
    };

    UDiskQThread(QObject * parent = nullptr);
    ~UDiskQThread();
    void updataUsb();

signals:
    //status  -> 0:add 1:remove
    void UDiskStatusChange(int status,QString udiskFilePath);

private:
    int initHotplugSock(void);
    bool cmdProcess(const QString & cmd, const QStringList & param, QString &output);
    void run(void);
    int  getRegExp(const QString pattern, const QString &text, QStringList &list);
    bool isRegExp(const QString pattern, const QString &text);
    bool mountUsb(const QString & dev);
    bool umountUsb(const QString & dev);

private:
    int hotplug_sock;
    int udisk_count;
};

#endif // UDISKQTHREAD_H

```

udiskqthread.cpp

```c++
#include "udiskqthread.h"

#define UEVENT_BUFFER_SIZE (2048)

UDiskQThread::UDiskQThread(QObject *)
{
    hotplug_sock = initHotplugSock();
}

UDiskQThread::~UDiskQThread()
{

}

int UDiskQThread::initHotplugSock()
{
    struct sockaddr_nl snl;
    const int buffersize = 16 * 1024 * 1024;
    int retval;
    memset(&snl, 0x00, sizeof(struct sockaddr_nl));
    snl.nl_family = AF_NETLINK;
    snl.nl_pid = getpid();
    snl.nl_groups = 1;
    int hotplug_sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (hotplug_sock == -1) {
        printf("error getting socket: %s", strerror(errno));
        return -1;
    }
    /* set receive buffersize */
    setsockopt(hotplug_sock, SOL_SOCKET, SO_RCVBUFFORCE, &buffersize, sizeof(buffersize));
    retval = bind(hotplug_sock, (struct sockaddr *) &snl, sizeof(struct sockaddr_nl));
    if (retval < 0) {
        printf("bind failed: %s", strerror(errno));
        close(hotplug_sock);
        hotplug_sock = -1;
        return -1;
    }

    return hotplug_sock;
}

void UDiskQThread::updataUsb()
{
#ifndef _WIN32
    QString output;
    cmdProcess("fdisk", QStringList()<<"-l", output);

    QStringList itemList;
    getRegExp("/dev/sd[a-z].*\\n", output, itemList);
    for (int i = 0; i < itemList.size(); i++)
    {
        QStringList tempList;
        if (getRegExp("/dev/sd[a-z][1-9]", itemList.at(i), tempList) <= 0){
            continue;
        }
        if (itemList[i].indexOf("FAT32") <= 0){
            //未找到FAT32
            emit UDiskStatusChange(UDISK_ERR, nullptr);
            return ;
        }
        QString path = tempList[0].mid(tempList[0].indexOf("/dev/sd")+5, 4);
        mountUsb(path);
        tempList.clear();
        break;
    }
#endif
}

int UDiskQThread::getRegExp(const QString pattern, const QString &text, QStringList &list)
{
    QRegExp regExp;
    int capCount = 0;
    int pos = 0;

    if (pattern.isEmpty() || text.isEmpty()){
        return -1;
    }
    regExp.setMinimal(true);
    regExp.setPattern(pattern);
    regExp.indexIn(text);

    list.clear();
    while ((pos = regExp.indexIn(text, pos)) != -1)
    {
        list.push_back(regExp.cap(0));
        pos += regExp.matchedLength();
        capCount++;
    }

    return capCount;
}

bool UDiskQThread::isRegExp(const QString pattern, const QString &text)
{
    QRegExp regExp;
    int capCount = 0;
    int pos = 0;

    if (pattern.isEmpty() || text.isEmpty())
    {
        return false;
    }
    regExp.setMinimal(true);
    regExp.setPattern(pattern);
    regExp.indexIn(text);

    while ((pos = regExp.indexIn(text, pos)) != -1)
    {
        pos += regExp.matchedLength();
        capCount++;
    }

    return (capCount>0);
}
//  /dev/sda1     /block/sda1
//  dev = sda1
bool UDiskQThread::mountUsb(const QString &dev)
{
    QString output;
    QString usbName;
    QString devName;

    devName = "/dev/" + dev;
    usbName = "/mnt/" + dev;

    if (QFile::exists(usbName) == false)
    {
        //不存在该目录
        if (cmdProcess("mkdir", QStringList() << "-p" << usbName, output) == false)
        {
            qDebug() << "mkdir usb derectory failed!";
            return false;
        }
    }

    if (!cmdProcess("/bin/mount", QStringList() << "-o" << "sync" << "-o" << "shortname=mixed" << "-o" << "utf8=yes" << devName << usbName, output))
    {
        qDebug() << "mount usb failed!";

        cmdProcess("rmdir", QStringList() << usbName, output);
        emit UDiskStatusChange(UDISK_ERR, nullptr);

        return false;
    }
    emit UDiskStatusChange(UDISK_ADD, usbName);

    return true;
}

bool UDiskQThread::umountUsb(const QString &dev)
{
    QString output;
    QString usbName;

    usbName = "/mnt/" + dev;
    if (!QFile::exists(usbName))
    {
        qDebug() << "derectory has not exist!";
        emit UDiskStatusChange(UDISK_REMOVE, usbName);

        return true;
    }
    if (!cmdProcess("/bin/umount", QStringList() << usbName, output))
    {
        qDebug() << "umount usb failed!";
        return false;
    }
    if (!cmdProcess("rmdir", QStringList() << usbName, output))
    {
        qDebug() << "rmdir usb derectory failed!";
        return false;
    }
    emit UDiskStatusChange(UDISK_REMOVE, usbName);

    return true;
}

bool UDiskQThread::cmdProcess(const QString &cmd, const QStringList &param, QString &output)
{
    QProcess process;

    process.start(cmd, param);
    // 等待进程启动
    if (!process.waitForStarted())
    {
        qDebug() <<  "waitForStarted error";
        return false;
    }
    process.closeWriteChannel();

    // 用于保存进程的控制台输出
    QByteArray procOutput;
    while (!process.waitForFinished(300))
    {
        qDebug() <<  "waitForFinished error";
        return false;
    }
    procOutput = process.readAll();
    output = procOutput;

    return true;
}

void UDiskQThread::run()
{
    char buf[UEVENT_BUFFER_SIZE*2] = {0};
    QString msg;

    updataUsb();

    fcntl(hotplug_sock, F_SETFL, fcntl(hotplug_sock, F_GETFL, 0));
    for (; 1 ;)
    {
        int len = read(hotplug_sock,buf,UEVENT_BUFFER_SIZE*2);
        if(len < 0){
            continue;
        }

        msg = QString::fromLocal8Bit(buf,len);
        if(msg.startsWith("add"))
        {
            if (isRegExp("/block/sd[a-z]/sd[a-z][0-9]", msg) == true)
            {
                QString path = msg.mid(msg.indexOf("/block/sd")+11, 4);
                mountUsb(path);

                continue;
            }
        }
        else if(msg.startsWith("remove"))
        {
            if (isRegExp("/block/sd[a-z]/sd[a-z][0-9]", msg) == true)
            {
                QString path = msg.mid(msg.indexOf("/block/sd")+11, 4);
                umountUsb(path);
            }
        }
    }
}


```

main.cpp

```c++
//在源文件中添加这些代码
UDiskQThread *thread1 = new UDiskQThread;
//创建一个连接函数
this->connect(thread1, &UDiskQThread::UDiskStatusChange, this, &MainWidget::DiskStatusChangeSlot);
//以下这个语句一定要放在connect后执行
thread1->start();

//当检测到有U盘插入或者拔出时，系统会调用该函数
void MainWidget::DiskStatusChangeSlot(int status, QString)
{
}

//在头文件中添加这些代码
#include "udiskqthread.h"

private slots:
    void DiskStatusChangeSlot(int status, QString path);
```

### 窗体控制相关接口

```C++
	//去掉窗口表框
	setWindowFlags(Qt::FramelessWindowHint | windowFlags());
	//全屏显示
	showFullScreen();
	//把窗口背景设置为透明
	setAttribute(Qt::WA_TranslucentBackground);
	//获取屏幕的工作区的大小
	//获取屏幕分辨率
	qDebug()<< "screen width:"<<QApplication::desktop()->width();
	qDebug()<< "screen height:"<<QApplication::desktop()->height();
    //下面方法也可以
	qDebug()<< "screen width:"<<qApp->desktop()->width();
	qDebug()<< "screen height:"<<qApp->desktop()->height();
 
    //获取客户使用区大小
    qDebug()<< "screen avaliabe width:"<<QApplication::desktop()->availableGeometry().width();
    qDebug()<< "screen avaliabe heigth:"<<QApplication::desktop()->availableGeometry().height();
 
    //获取应用程序矩形大小
    qDebug()<< "application avaliabe width:"<<QApplication::desktop()->screenGeometry().width();
    qDebug()<< "application avaliabe heigth:"<<QApplication::desktop()->screenGeometry().height();
	//移动到窗口中央
	move((QApplication::desktop()->width() - width())/2,  (QApplication::desktop()->height() - height())/2);
	//全屏设置
	setWindowState(Qt::WindowFullScreen);

//设置应用程序图标
	QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/images/logo.png")), QIcon::Normal, QIcon::Off);
    win->setWindowIcon(icon);
    win->setIconSize(QSize(256, 256));

//Qt:QWidget设置控件的层次关系
//设置控件置于父窗口的顶部
    widget->raise();
//设置控件层次
    widget->stackUnder(other_widget); 
```

### 按键透明方法

```c++
//第一种方法
QPushButton *mode =new QPushButton(this);
mode->setText("mode");
mode->move(200,100);
mode->setFlat(true);//就是这句实现按钮透明的效果。

//第二种方法
在design视图下，右键点击按钮->Change StyleSheet，add color ->选择background-color，之后把最右下⾓的Alpha通道设置为0(其余RGB没有关系)，就能实现透明的效果，效果和第⼀种⽅法⼀样。也可以通过setStyleSheet函数来实现
    setStyleSheet("background-color: rgba(0, 0, 0, 0)"); 同样前三个0就是RGB值，可以是别的。
我在实现的过程中，虽然⽤这样的⽅法设置了透明的效果，但是按钮的背景⾊是⿊的，并不是透明的，不能显⽰出背景图
⽚，后来就⽤ps做了⼀个和按钮⼤⼩⼀样的png格式的透明图⽚，然后⽤同样的右键，Change styleSheet的⽅式，add background-image，选择了我做的那个透明图⽚，最终透明效果就完全实现了。我猜测之所以还要这么做，应该是⽗窗⼝使⽤了setStyleSheet的⽅式设置了背景⾊的原因。如果不⽤这种⽅式设置背景⾊，应该就不需要经过这⼀步了。

（注：已经确认就是这个问题，⽗窗⼝最好不要采⽤setStyleSheet⽅式设置背景⾊。这种情况下就不需要加那个透明图⽚了。不过在我实验过程中，这两种⽅法还是有区别的，第⼀种⽅法实现透明后，当按下按钮的时候，能显⽰⼀下按钮的轮廓以及按钮的颜⾊；第⼆种⽅法实现透明后，当按下按钮后，显⽰⼀下按钮的轮廓并且按钮是⿊⾊的，就是点击⼀下屏幕，出现⼀闪⽽过的⿊⾊，想要不出现⿊⾊，就要⽤第⼀种⽅法或者第⼆种⽅法加⼀个透明按钮图（和按钮⼤⼩⼀致的透明图⽚，png格式）。）

```

总结⼀下：
以上两种⽅法，在界⾯静态的时候均能实现透明的效果，不过按下按钮的时候会有不同的状态，具体如下：

1. 只⽤第⼀种⽅法的效果是：点击按钮，能显⽰出来按钮本来的轮廓和颜⾊；
2. 只⽤第⼆种⽅法的效果是：点击按钮，显⽰按钮的轮廓，但是按钮是⿊⾊的；
3. 第⼀种和第⼆种⽅法同时都使⽤：点击按钮，不显⽰按钮的轮廓，更没有按钮颜⾊，不过依然有按钮被按下的动态效果；
4. 第⼀种或者第⼆种⽅法，加上⼀个透明按钮图（和按钮⼤⼩⼀致的透明图⽚，png格式),均能实现以上第三种情况效果

### 日志输出到指定文件

```C++
#include <QApplication>
#include <QMutex>
#include <QDateTime>
#include <QFile>
#include <QTextStream>

void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static QMutex mutex;
    mutex.lock();

    QString text;
    switch(type)
    {
    case QtMsgType::QtDebugMsg:
       text = QString("Debug:");
       break;
    case QtMsgType::QtWarningMsg:
       text = QString("Warning:");
       break;
    case QtMsgType::QtCriticalMsg:
       text = QString("Critical:");
       break;
    case QtMsgType::QtFatalMsg:
       text = QString("Fatal:");
       break;
    default: break;
    }

    QString context_info = QString("File:(%1) Line:(%2)").arg(QString(context.file)).arg(context.line);
    QString current_date_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ddd");
    QString current_date = QString("(%1)").arg(current_date_time);
    QString message = QString("%1 %2 %3 %4").arg(text).arg(context_info).arg(msg).arg(current_date);

    QFile file("log.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream text_stream(&file);
    text_stream << message << "\r\n";
    file.flush();
    file.close();

    mutex.unlock();
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qInstallMessageHandler(outputMessage);

    return app.exec();
}
```

### ubuntu下QT安装帮助文档

```shell
<1>命令行 ：
  sudo apt-get install qt5-doc
  sudo apt-get install qtbase-doc-html

<2>在应用中添加帮助文档
   打开QtCreator->tools->Options->Help->Documentation Add   .qch后缀文件 文件路径默认安装在usr/share/qt5/doc/
```

### ubuntu18.04下QT使用QCamera类

.pro文件

```
QT       += multimedia multimediawidgets
```

.h文件

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QCamera>
#include <QCameraImageCapture>
#include <QCameraViewfinder>
#include <QPushButton>
#include <QMainWindow>
#include <QMediaRecorder>
#include <QScopedPointer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private:
    void setCamera(const QCameraInfo & cameraInfo);
    void takeImage(void);
    void recordStart(void);
    void recordStop(void);

private slots:
    void updateCameraState(QCamera::State);
    void displayCameraError(void);
    void updateCaptureMode(QCamera::CaptureModes );
    //video
    void updateRecorderState(QMediaRecorder::State);
    void updateRecordTime();
    void displayRecorderError(void);
    //capture
    void imageSaved(int, const QString &);
    void displayCaptureError(int, QCameraImageCapture::Error, const QString &);

    void on_videoBtn_clicked();
    void on_captureBtn_clicked();

private:
    Ui::MainWidget *ui;
    QCameraViewfinder viewfinder;
    QScopedPointer<QCamera> m_camera;
    QScopedPointer<QMediaRecorder> m_mediaRecorder;
    QScopedPointer<QCameraImageCapture> m_imageCapture;

    QImageEncoderSettings m_imageSettings;
    QAudioEncoderSettings m_audioSettings;
    QVideoEncoderSettings m_videoSettings;

    int imageCnt;
    int videoCnt;
    QString locationDir;
};
#endif // MAINWIDGET_H

```

.cpp文件

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QFileInfo>
#include <QUrl>
#include <QBoxLayout>
#include <QMessageBox>
#include <QCameraInfo>
#include <QMediaMetaData>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    viewfinder.setWindowFlag(Qt::FramelessWindowHint);
    //设置全视角显示
    viewfinder.setAspectRatioMode(Qt::KeepAspectRatio);
    QBoxLayout *hlayout = new QHBoxLayout;
    hlayout->setMargin(0);
    hlayout->addWidget(&viewfinder);

    ui->videoWidget->setLayout(hlayout);
    setCamera(QCameraInfo::defaultCamera());

    locationDir = "/home/zhangya/Pictures/";
    imageCnt = 0;
    videoCnt = 0;
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::setCamera(const QCameraInfo &cameraInfo)
{
    m_camera.reset(new QCamera(cameraInfo));
    connect(m_camera.data(), &QCamera::stateChanged, this, &MainWidget::updateCameraState);
    connect(m_camera.data(), QOverload<QCamera::Error>::of(&QCamera::error), this, &MainWidget::displayCameraError);

    m_mediaRecorder.reset(new QMediaRecorder(m_camera.data()));
    connect(m_mediaRecorder.data(), &QMediaRecorder::stateChanged, this, &MainWidget::updateRecorderState);
    connect(m_mediaRecorder.data(), &QMediaRecorder::durationChanged, this, &MainWidget::updateRecordTime);
    connect(m_mediaRecorder.data(), QOverload<QMediaRecorder::Error>::of(&QMediaRecorder::error), this, &MainWidget::displayRecorderError);
    m_mediaRecorder->setMetaData(QMediaMetaData::Title, QVariant(QLatin1String("Test Title")));

    m_imageCapture.reset(new QCameraImageCapture(m_camera.data()));
    connect(m_imageCapture.data(), &QCameraImageCapture::imageSaved, this, &MainWidget::imageSaved);
    connect(m_imageCapture.data(), QOverload<int, QCameraImageCapture::Error, const QString&>::of(&QCameraImageCapture::error), this, &MainWidget::displayCaptureError);

    //QCameraViewfinderSettings viewFinderSetting;
    //viewFinderSetting.setResolution(800, 600);
    //m_camera->setViewfinderSettings(viewFinderSetting);
    m_camera->setViewfinder(&viewfinder);

    QSize size(640, 480);

    m_imageSettings.setCodec("jpeg");
    m_imageSettings.setQuality(QMultimedia::VeryHighQuality);
    m_imageSettings.setResolution(size);
    m_imageCapture->setEncodingSettings(m_imageSettings);

    m_audioSettings.setCodec("audio/x-adpcm");
    m_audioSettings.setChannelCount(2);
    m_audioSettings.setQuality(QMultimedia::NormalQuality);
    m_mediaRecorder->setAudioSettings(m_audioSettings);

    m_videoSettings.setCodec("video/x-h264");
    m_videoSettings.setResolution(size);
    m_videoSettings.setQuality(QMultimedia::NormalQuality);
    m_mediaRecorder->setVideoSettings(m_videoSettings);

    m_mediaRecorder->setContainerFormat("video/quicktime");

    //updateCaptureMode(QCamera::CaptureVideo);
    m_camera->unload();
    m_camera->setCaptureMode(QCamera::CaptureVideo);
    m_camera->start();
}

void MainWidget::updateCaptureMode(QCamera::CaptureModes mode)
{
    if (m_camera->isCaptureModeSupported(mode)){
        m_camera->unload();
        m_camera->setCaptureMode(mode);
        m_camera->start();
    }
}

void MainWidget::updateRecorderState(QMediaRecorder::State state)
{
    switch (state) {
    case QMediaRecorder::StoppedState:
        ui->videoBtn->setText(tr("video"));
        break;
    case QMediaRecorder::PausedState:break;
    case QMediaRecorder::RecordingState:
        ui->videoBtn->setText(tr("videoing"));
        break;
    }
}

void MainWidget::updateRecordTime(void)
{

}

void MainWidget::updateCameraState(QCamera::State state)
{
    switch (state)
    {
    case QCamera::ActiveState: break;
    case QCamera::UnloadedState:
    case QCamera::LoadedState: break;
    }
}

void MainWidget::displayCameraError(void)
{
    QMessageBox::warning(this, tr("Camera Error"), m_camera->errorString());
}

void MainWidget::displayRecorderError(void)
{
    QMessageBox::warning(this, tr("Capture Error"), m_mediaRecorder->errorString());
}

void MainWidget::imageSaved(int id, const QString & fileName)
{
    Q_UNUSED(id);
    Q_UNUSED(fileName);
    qDebug()<<"image saved!";
}

void MainWidget::displayCaptureError(int id, QCameraImageCapture::Error error, const QString & errorString)
{
    Q_UNUSED(id);
    Q_UNUSED(error);
    QMessageBox::warning(this, tr("Image Capture Error"), errorString);
}

void MainWidget::takeImage()
{
    QFileInfo fi;
    QString lo;

    lo = locationDir + "PIC" + QString::number(imageCnt) + ".jpg";
    fi = QFileInfo(lo);

    while(fi.isFile()){
        imageCnt++;
        lo = locationDir + "PIC" + QString::number(imageCnt) + ".jpg";
        fi = QFileInfo(lo);
    }

    m_imageCapture->capture(lo);
}

void MainWidget::recordStart()
{
    QFileInfo fi;
    QString lo;

    lo = locationDir + "VIDEO" + QString::number(videoCnt) + ".mov";
    fi = QFileInfo(lo);

    while(fi.isFile()){
        videoCnt++;
        lo = locationDir + "VIDEO" + QString::number(videoCnt) + ".mov";
        fi = QFileInfo(lo);
    }

    m_mediaRecorder->setOutputLocation(QUrl::fromLocalFile(lo));
    m_mediaRecorder->record();
}

void MainWidget::recordStop()
{
    m_mediaRecorder->stop();
}

void MainWidget::on_videoBtn_clicked()
{
    if(m_camera->captureMode() != QCamera::CaptureVideo){
        m_camera->setCaptureMode(QCamera::CaptureVideo);
    }
    if (m_mediaRecorder->state() == QMediaRecorder::RecordingState)
        recordStop();
    else
        recordStart();
}

void MainWidget::on_captureBtn_clicked()
{
    if (m_camera->captureMode() != QCamera::CaptureStillImage) {
        m_camera->setCaptureMode(QCamera::CaptureStillImage);
    }
    if(m_imageCapture->isReadyForCapture())
        takeImage();
}

```

### ubuntu18.04下QT添加OpenCV库

OpenCV库下载网站：https://opencv.org/releases/

https://blog.csdn.net/weixin_43793181/article/details/103108670

https://www.jianshu.com/p/1f0550b51cf0

https://www.cnblogs.com/hyb965149985/p/10123453.html

https://zhuanlan.zhihu.com/p/217736040

https://www.it1352.com/466748.html

https://docs.opencv.org/3.0-beta/index.html

### 1、ubuntu18.04下QT安装openCV库

通过网站https://opencv.org/releases/下载需要的openCV版本，如下图，点击直接下载

![](QT的使用/openCV源码图.png)

配置前先下载cmake

```
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get update
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev
```

（如果出现“E: 无法定位软件包 libjasper-dev”报错，是因为缺少依赖，终端继续输入）

```
sudo add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
sudo apt update
sudo apt install libjasper1 libjasper-dev
```

（安装依赖后再一次输入）

```
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev
```

QT+Opencv配置：（使用unzip命令将opencv压缩包解压，进入解压后的opencv文件夹，在该文件路径中打开终端，执行下面的命令）

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
sudo make -j4
sudo make install
sudo make
sudo make install
sudo gedit /etc/ld.so.conf.d/opencv.conf 
#在opencv.conf文档中输入：       /usr/local/lib
sudo ldconfig
```

输入如下图：

![](QT的使用/opencv_conf文档输入.png)

接着配置环境变量

```
sudo gedit /etc/bash.bashrc 
```

在bash.bashrc文件的最底端写入 ，并保存，退出：

```
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig 
export PKG_CONFIG_PATH
```

回到终端，在终端中输入：

```
sudo ldconfig
sudo cp /usr/local/include/opencv4/opencv2 /usr/local/include -r
上面一行，可自行顺着路径查看本机文件夹内容，这个操作是为了方便配置好后，QT的pro文件内容更简便
```

### 2、QT添加openCV库

.pro文件中需要加入下面代码：

```
INCLUDEPATH += /usr/local/include \
               /usr/local/include/opencv \
               /usr/local/include/opencv2

LIBS += /usr/local/lib/libopencv_*
```

.h的头文件

```C++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QImage>
#include <QTimer>
#include <QDebug>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent =nullptr);
    ~MainWidget();

    QImage mat2Image(const cv::Mat & mat);
    cv::Mat imaget2Mat(QImage image);

private slots:
    void readFarme(void);       //
    void on_openBtn_clicked();
    void on_pictureBtn_clicked();
    void on_videoBtn_clicked();

private:
    Ui::MainWidget *ui;
    cv::VideoCapture capture;
    cv::Mat frame;
    cv::VideoWriter write;

    QTimer *timer;

    bool videoFlag;
};

#endif // MAINWIDGET_H

```

.cpp文件

```C++
#include "mainwidget.h"
#include "ui_mainwidget.h"

using namespace cv;

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    videoFlag = false;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(readFarme()));
}

MainWidget::~MainWidget()
{
    delete ui;
}

QImage MainWidget::mat2Image(const cv::Mat &mat)
{
    if (mat.type() == CV_8UC1)                 // 单通道
    {
        QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
        image.setColorCount(256);              // 灰度级数256
        for (int i = 0; i < 256; i++)
        {
            image.setColor(i, qRgb(i, i, i));
        }
        uchar *pSrc = mat.data;                // 复制mat数据
        for (int row = 0; row < mat.rows; row++)
        {
            uchar *pDest = image.scanLine(row);
            memcpy(pDest, pSrc, mat.cols);
            pSrc += mat.step;
        }
        return image;
    }
    else if (mat.type() == CV_8UC3)            // 3通道
    {
        const uchar *pSrc = (const uchar*)mat.data;        // 复制像素
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);   // R, G, B 对应 0,1,2
        return image.rgbSwapped();             // rgbSwapped是为了显示效果色彩好一些。
    }
    else if (mat.type() == CV_8UC4)
    {
        const uchar *pSrc = (const uchar*)mat.data;        // 复制像素

        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);       // B,G,R,A 对应 0,1,2,3
        return image.copy();
    }
    else
    {
        return QImage();
    }
}

cv::Mat MainWidget::imaget2Mat(QImage image)
{
    cv::Mat mat;
    switch (image.format())
    {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32_Premultiplied:
        mat = cv::Mat(image.height(), image.width(), CV_8UC4, (void*)image.constBits(), image.bytesPerLine());
        break;
    case QImage::Format_RGB888:
        mat = cv::Mat(image.height(), image.width(), CV_8UC3, (void*)image.constBits(), image.bytesPerLine());
        cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
        break;
    case QImage::Format_Indexed8:
        mat = cv::Mat(image.height(), image.width(), CV_8UC1, (void*)image.constBits(), image.bytesPerLine());
        break;
    default: break;
    }
    return mat;
}

void MainWidget::readFarme()
{
    if (!capture.isOpened())
    {
        timer->stop();
        return ;
    }
    //capture >> frame;
    capture.read(frame);
    if (frame.empty())
        return ;
    if (videoFlag)
    {
        write.write(frame);
    }

    cvtColor(frame, frame, cv::COLOR_BGR2RGB);
    QImage image = QImage((unsigned char *)(frame.data), frame.cols,frame.rows, QImage::Format_RGB888).rgbSwapped();
    ui->label->setPixmap(QPixmap::fromImage(image));
    ui->label->resize(image.width(), image.height());
}

void MainWidget::on_openBtn_clicked()
{
    //直接打开摄像头，查看摄像头编号命令
    //ls -al /dev/ | grep video
    //对于输出信息以video开头的其数字后缀即为可能的摄像头编号，如果一台电脑有多个摄像头设备，那么将会出现从0开始的多个摄像头编号。
    capture.open(0);
    if (!capture.isOpened())
    {
        qDebug()<<"open failed";
        return ;
    }
    timer->start(1000/30);//1000/10);
}

void MainWidget::on_pictureBtn_clicked()
{

}

void MainWidget::on_videoBtn_clicked()
{
    if (!capture.isOpened())
    {
        videoFlag = false;
        return ;
    }
    int fps = capture.get(cv::CAP_PROP_FPS);
    if (fps <= 0)
        fps = 25;
    if (videoFlag == false)
    {
        /*
         * 编码格式
        cv::VideoWriter::fourcc(‘I’, ‘4’, ‘2’, ‘0’);该选项是一个未压缩的YUV颜色编码，是4:2:0色度子采样。这种编码兼容性很好，但文件较大，文件扩展名.avi。
        cv::VideoWriter::fource(‘P’, ‘I’, ‘M’, ‘1’);该选项是MPEG-1编码类型，文件扩展名为.avi。
        cv::VideoWriter::fource(‘X’, ‘V’, ‘I’, ‘D’);给选项是MPEG-4编码类型，如果希望得到的视频大小为平均值，推荐这个选项，文件扩展名.avi。
        cv::VideoWriter::fource(‘T’, ‘H’, ‘E’, ‘O’);该选项是Ogg Vorbis，文件扩展名应为.ogv。
        cv::VideoWriter::fource(‘F’, ‘L’, ‘V’, ‘1’);该选项是一个Flash视频，文件扩展名.flv。
        cv::VideoWriter::fourcc('M', 'J', 'P', 'G');是motion-jpeg codec等
        cv::VideoWriter::fourcc('X', '2', '6', '4');
        cv::VideoWriter::fourcc('M', 'P', 'E', 'G');
         */
        /*
            Size(capture.get(CAP_PROP_FRAME_WIDTH), capture.get(CAP_PROP_FRAME_HEIGHT));
        */
        write.open("/home/zhangya/Pictures/test.avi", cv::VideoWriter::fourcc('X', '2', '6', '4'), fps, cv::Size(640, 480), true);
        videoFlag = true;
        ui->videoBtn->setText("videoing");
    }else{
        write.release();
        videoFlag = false;
        ui->videoBtn->setText("video");
    }
}
```

### 3、编译出现问题

error: error adding symbols: File in wrong format， 因为x86_64的ld命令链接的库的格式却是ARM格式的库，因此提示file in wrong format.

在   /usr/local/lib   目录下输入：file libopencv_core.so.4.5.3

![](QT的使用/opencv错误_01.png)

库文件的格式不对

### 4、openCV函数说明

获取VideoCapture类实例

```C++
//获取VideoCapture类实例，读取视频文件
cv::VideoCapture * fcap = new cv::VideoCapture("demo.mp4");
//打开摄像头画面
cv::VideoCapture * ccap = new cv::VideoCapture(0);
//读取摄像头画面
cv::Mat frame;
ccap->read(frame);
```

获取视频流信息

```c++
//获取视频帧的宽
double w = ccap->get(cv::CAP_PROP_FRAME_WIDTH);
//获取视频帧的高
double h = ccap->get(cv::CAP_PROP_FRAME_HEIGHT);
//获取视频帧的帧率
double fps = ccap->get(cv::CAP_PROP_FPS);
//获取视频流的总帧数
double fcount = fcap->get(cv::CAP_PROP_FRAME_COUNT);
```

针对一组或多头摄像头特殊处理

```c++
bool success0 = ccap0->grab();
bool success1 = ccap1->grab();
if (success0 && success1)
{
    OutputArray frame0, frame1;
    ccap0->retrieve(frame0);
    ccap1->retrieve(frame1);
}
```

跳到视频流的某一帧

```c++
//跳到某一感兴趣帧并从此帧开始读取,如从第360帧开始读取
fcap->set(cv::CAP_PROP_POS_FRAMES, 360);
cv::Mat frame;
fcap->read(frame);
```

设置摄像头分辨率

```c++
// 设置摄像头分辨率的高
ccap->set(cv::CAP_PROP_FRAME_HEIGHT, 1080);
// 设置摄像头分辨率的宽
ccap->set(cv::CAP_PROP_FRAME_WIDTH, 1920);
```

释放资源

```c++
![01_language](Qt/01_language.png)![01_language](Qt/01_language.png)// 释放VideoCapture资源
fcap->release()
// 释放VideoWriter资源
writer->release()
```

### 5、视频讲解ubuntu下搭建opencv环境步骤

```shell
sudo apt install python3
#通过pip命令安装numpy   matplotlib和opencv库
python3 -m pip install numpy matplotlib opencv
#或者使用
apt-cache search numpy | grep python3
sudo apt install python3-numpy python3-matplotlib python3-opencv
```

### 6、源码方式搭建opencv环境

```sh
#安装必要的库和工具
###安装python3和numpy库

###安装cmake

###安装VS2019

#下载opencv源码
###opencv已经官方验证的原码
git clone https://github.com/opencv/opencv.git
###opencv-contribue有没验证的原码
git clone https://github.com/opencv/opencv_contrib.git
#编译
###mkdir build

###运行cmake,生成编译脚本

###通过VS进行编译
```

### QT添加ffmpeg

1、添加系统环境



2、.pro文件添加库

```
//库文件  libavutil.so   libavformat.so

INCLUDE_PATH = /home/zhangya/rk3288/linux/PET_RK3288_P01_Linux/buildroot/output/rockchip_rk3288/host/arm-buildroot-linux-gnueabihf/sysroot/usr

INCLUDEPATH += $${INCLUDE_PATH}/include/libav*

LIBS += -LF:$${INCLUDE_PATH}/lib  -lavutil -lavformat -lavcodec -lavdevice -lavfilter -lswresample -lswscale
```

videoffmpeg.h

```c++
#ifndef VIDEOFFMPEG_H
#define VIDEOFFMPEG_H

extern "C" {
#include <libavutil/frame.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
}

namespace VideoFfmpeg
{
    void init(void);
    void play(unsigned char* pbuff_in,int nwidth,int nheight);
    void release(void);
    bool YV12ToBGR24_FFmpeg(unsigned char* pYUV,unsigned char* pBGR24,int width,int height);
}
#endif // VIDEOFFMPEG_H

```

videoffmpeg.cpp

```c++
#include "videoffmpeg.h"
#include <QImage>

namespace VideoFfmpeg
{
AVFrame    *m_pFrameRGB,*m_pFrameYUV;
uint8_t *m_rgbBuffer,*m_yuvBuffer;
struct SwsContext *m_img_convert_ctx;
int nwidth = 1920, nheight = 1080;

//分配两个Frame，两段buff，一个转换上下文
void init(void)
{
    //为每帧图像分配内存
    m_pFrameYUV = av_frame_alloc();
    m_pFrameRGB = av_frame_alloc();// width和heigt为传入的分辨率的大小，分辨率有变化时可以以最大标准申请
    int numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, nwidth,nheight);
    m_rgbBuffer = (uint8_t *) av_malloc(numBytes * sizeof(uint8_t));
    int yuvSize = nwidth * nheight * 2;
    m_yuvBuffer = (uint8_t *)av_malloc(yuvSize);
    //特别注意sws_getContext内存泄露问题，
    //注意sws_getContext只能调用一次，在初始化时候调用即可，另外调用完后，在析构函数中使用sws_freeContext，将它的内存释放。
    //设置图像转换上下文
    m_img_convert_ctx = sws_getContext(nwidth, nheight, AV_PIX_FMT_YUYV422,nwidth, nheight, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
}

void play(unsigned char* pbuff_in,int nwidth,int nheight)
{
    //av_image_fill_arrays()
    avpicture_fill((AVPicture *) m_pFrameRGB, m_rgbBuffer, AV_PIX_FMT_RGB24, nwidth, nheight);
    avpicture_fill((AVPicture *) m_pFrameYUV, (uint8_t *)pbuff_in, AV_PIX_FMT_YUYV422, nwidth, nheight);
    //转换图像格式，将解压出来的YUV420P的图像转换为RGB的图像
    sws_scale(m_img_convert_ctx, (uint8_t const * const *) m_pFrameYUV->data,            m_pFrameYUV->linesize, 0, nheight, m_pFrameRGB->data, m_pFrameRGB->linesize);
    //把这个RGB数据 用QImage加载
    QImage tmpImg((uchar *)m_rgbBuffer,nwidth,nheight,QImage::Format_RGB888);
    tmpImg.save("/usr/app/zhangya/123.jpg");
}

void release(void)
{
    av_frame_free(&m_pFrameYUV);
    av_frame_free(&m_pFrameRGB);
    av_free(m_rgbBuffer);
    av_free(m_yuvBuffer);
    sws_freeContext(m_img_convert_ctx);
}

bool YuvToRgb_FFmpeg(unsigned char* pYUV,unsigned char* pRgb24,int width,int height)
{
    if (width < 1 || height < 1 || pYUV == NULL || pRgb24 == NULL) {
        return false;
    }

    AVPicture pFrameYUV,pFrameRGB;
    avpicture_fill(&pFrameYUV,pYUV,AV_PIX_FMT_YUYV422,width,height);

    //U,V互换
    uint8_t * ptmp=pFrameYUV.data[1];
    pFrameYUV.data[1]=pFrameYUV.data[2];
    pFrameYUV.data[2]=ptmp;

    avpicture_fill(&pFrameRGB, pRgb24, AV_PIX_FMT_RGB24, width,height);

    struct SwsContext* imgCtx = NULL;
    imgCtx = sws_getContext(width,height, AV_PIX_FMT_YUYV422,width,height, AV_PIX_FMT_RGB24,SWS_BILINEAR,0,0,0);

    if (imgCtx != NULL){
  		sws_scale(imgCtx,pFrameYUV.data,pFrameYUV.linesize,0,height,pFrameRGB.data,pFrameRGB.linesize);
        if(imgCtx){
            sws_freeContext(imgCtx);
            imgCtx = NULL;
        }
        return true;
    }else{
        sws_freeContext(imgCtx);
        imgCtx = NULL;
        return false;
    }
}
}

```

### QT在新建类添加UI::下的所有东西

可以在该类（.h文件中）的前面添加下面语句：

```c++
namespace Ui{
   class MainForm;
}
```

### QT的多语言动态切换

1、在工程中将所有需要翻译的字符串用tr包起来，如下：

```
    ui->sceneLabel->setText(sceneList->at(sceneNum));
    ui->timeSetBtn->setText(tr("Time setting"));
    ui->menuBtn->setText(tr("Menu"));
```

2、在.pro工程文件中添加

```
TRANSLATIONS = video_en.ts \
               video_zh.ts
```

在工具->外部->Qt语言家->更新翻译lupdate， 当没有.ts 文件时，会创建， 每次点击lupdate 将会更新其内容

<img src="QT的使用/01_language.png" style="zoom: 67%;" />

3、在  工具->外部->Qt语言家->翻译工具(linguist),打开翻译工具。在翻译工具上点   【文件】->【打开】，选择所生成的.ts（video_en.ts，  video_zh.ts）文件，对其进行修改。

<img src="QT的使用/02_language.png" style="zoom:67%;" />

翻译完成后，保存退出

注意：如果点 工具->外部->Qt语言家->翻译工具(linguist)，没有弹出上面的窗口，说明路径不对，可点  工具->选项， 找到下图

<img src="QT的使用/03_language.png" style="zoom:67%;" />

4、工程中 工具->外部->Qt预言家->发布翻译lrelease ，即可生成.qm文件

5、在工程的资源文件  Resources -> res.qrc  中添加生成的.qm文件

6、程序上处理

mcommon.h

```
class MCommon
{
public:
	//设置语言
    static void InitUiByLanguage(const QString strLanguage)
    {
        if (strLanguage.isEmpty()) {
            return;
        }

        QString strLanguageFile;
        if (strLanguage.compare("en") == 0) {
            strLanguageFile = ":/new/prefix1/video_en.qm";
        }
        else if (strLanguage.compare("zh") == 0) {
            strLanguageFile = ":/new/prefix1/video_zh.qm";
        } else {
            return ;
        }

        static QTranslator translator;
        if (QFile(strLanguageFile).exists())
        {
            translator.load(strLanguageFile);
            qApp->installTranslator(&translator);
        }else{
            qDebug() << "[houqd] authclient language file does not exists ...";
        }
    }
}
```

main.c

```
#include <QApplication>
#include "mcommon.h"
int main(int argc, char ** argv)
{
	QApplication app(argc, argv);
	MCommon::InitUiByLanguage("en");
	MainForm  * mainForm = new MainForm();
    mainForm->show();
	
    return app.exec();
}
```

mainform.h

```
#ifndef MAINFORM_H
#define MAINFORM_H

class MainForm : public QWidget
{
    Q_OBJECT
public:
    explicit MainForm(QWidget *parent = nullptr);
    ~MainForm();
    
protected:
    void changeEvent(QEvent * event) override;
};

#endif
```

mainform.cpp

```
#include "mcommon.h"
#include "mainform.h"
MainForm::MainForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);
}

void MainForm::changeEvent(QEvent * event)
{
    if(event->type() == QEvent::LanguageChange) {
        ui->sceneLabel->setText(sceneList->at(sceneNum));
    	ui->timeSetBtn->setText(tr("Time setting"));
    	ui->menuBtn->setText(tr("Menu"));
    }else{
        QWidget::changeEvent(event);
    }
}

void MainForm::on_language_BtnSlot(QAbstractButton * button, bool click)
{
    Q_UNUSED(button)

    if (click == false)
        return ;

    if (ui->englishRedioBtn->isChecked()) {
        MCommon::InitUiByLanguage("en");
        qDebug()<<"language = en";
    }
    else if (ui->chineseRedioBtn->isChecked()) {
        MCommon::InitUiByLanguage("zh");
        qDebug()<<"language = zh";
    }
}
```

### QT之QSettings学习

1、读写配置文件

```
//创建配置文件
QSettings iniFile("./test.ini", QSettings::IniFormat);　　　　//使用相对路径需要删除"项目==》shadow build选项"
iniFile.setIniCodec(QTextCodec::codecForName("utf-8")); //在此添加设置，即可读写ini文件中的中文
//写入数据
iniFile.setValue("/setting/value1", 1);
iniFile.setValue("/setting/value2", 2);
iniFile.beginGroup("setting"); //切换到setting组之下
iniFile.setValue("value3", 3);
iniFile.endGroup(); //关闭分组定位,注意如果需切换分组，必须保证关闭。不然切换无效

 //读取数据
QStringList all = setting.childGroups();　　　　//从配置文件中读取所有的分组
setting.beginGroup("/setting"); 
QString value1=setting.value("/setting/value1").toString();
setting.beginGroup("/setting");
QString value2=setting.value("value2").toString(); 
setting.endGroup();
```

写入test.ini格式如下

```
[setting]
　　value1=1
　　value2=2
　　value3=3
```

注意：因为QVariant是不会提供所有数据类型的转化的，比如有toInt(),toPoint(),toSize(),但却没有对Qcolor，Qimage和Qpixmap等数据类型的转化，此时可以用QVariant.value（）；  QSettings是可重入的，意味着可以同时在不同的线程中使用不同的QSettings对象

若key所对应的value是int型的，也可以toInt()，若没有要找的key，如果用返回0。

```
pos = setting.value("pos", QVariant(QPoint(200, 200))).toPoint(); 
size= setting.value("size", QVariant(QSize(400, 400))).toSize();
```

- contains() 判断一个指定的键是否存在
- remove() 删除相关的键
- allKeys() 获取所有键
- clear() 删除所有键

注意：解决value读写的中文乱码问题，添加代码setting.setIniCodec("GBK")或者setting.setIniCodec(QTextCodec::codecForName("GB2312"));即可。 目前还未解决其键值的中文乱码问题。

### Qt窗口配置

##### 1、QT设置窗口透明度

```c++
//1---屏蔽设置窗口透明
//setAttribute(Qt::WA_TranslucentBackground);

//2---设置界面基本颜色
this->setAutoFillBackground(true);//属性控制小部件背景是否自动填充
QPalette palette = this->palette();
palette.setColor(QPalette::Base,QColor(0,0,0,255));//alpha 0~255;0为不透明，255为透明
palette.setColor(QPalette::Window,QColor(0,0,0,100));//alpha 0~255;0为不透明，255为透明
this->setPalette(palette);    //这样设置就可以了，但是我的需求是窗体要有边框，故需要添加下边一步

//3----使用样式变添加边框，设置透明背景
ui->tableWidget->setStyleSheet(QString("border-width: 2px;"\
                                    "border-style: solid;"\
                                    "border-radius:15px;"\
                                    "border-color: rgb(255,255,255);"\
                                    "background-color:transparent;"));
```

##### 2、窗口大小配置

```c++
//取消最大化按钮
this->setWindowFlags(Qt::WindowMinimizeButtonHint|Qt::WindowCloseButtonHint);
//取消最小化按钮
this->setWindowFlags(Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint);
//取消最大最小化按钮
this->setWindowFlags(Qt::WindowCloseButtonHint);
//关闭窗口时显示提示框
void Widget::closeEvent(QCloseEvent::event){
	int res = QMessageBox::question(this, "确定", "你要关闭吗", "是", "否")；
	if (res ==1){
		event -> ignore();
	}
}
//设置全屏模式
setWindowState(Qt::WindowFullScreen);
//获取屏幕分辨率
#include <QScreen>
QList <QScreen *> list_screen = QGuiApplication::screens();
resize(list_screen.at(0)->geometry().width(), list_screen.at(0)->geometry().height());
//获取有效屏幕大小
#include <QScreen>
QList <QScreen *> list_screen = QGuiApplication::screens();
    QSize size(list_screen.at(0)->availableGeometry().width(), list_screen.at(0)->availableGeometry().height());
qDebug()<<"w="<<size.width()<<"h="<<size.height();
//设置固定窗体大小
setFixedSize(800, 430);
//调整大小事件

```

### QT之QRadioButton的处理

mainform.h

```c++
#ifndef MAINFORM_H
#define MAINFORM_H
class MainForm : public QWidget
{
    Q_OBJECT

public:
	explicit MainForm(QWidget *parent = nullptr);
    ~MainForm();
    
private slots:
	void onRadioBtnSlot(bool);
};
#endif
```

mainform.cpp

```c++
#include "mainform.h"

MainForm::MainForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);
    
    connect(ui->si2RadioBtn, SIGNAL(toggled(bool)), this, SLOT(onRadioBtnSlot(bool)));
    connect(ui->g12RadioBtn, SIGNAL(toggled(bool)), this, SLOT(onRadioBtnSlot(bool)));
}

MainForm::~MainForm()
{
}

void MainForm::onRadioBtnSlot(bool click)
{
    QRadioButton * btn = (QRadioButton *)this->sender();
    if (click == false)
        return ;

    qDebug()<<btn->objectName()<<"-->"<<click;
    
    const char * tempStr = (ui->si2RadioBtn->isChecked())?("sdi_2si"):("sdi_i2g");
}
```

### QT计算程序运行时间

```C++
#include <sys/time.h>

struct timeval tpend1, tpend2;
long msec = 0;

gettimeofday(&tpend1, NULL);
//....需要计算运行时间的代码区

//...............
gettimeofday(&tpend2, NULL);
msec = 1000 * (tpend2.tv_sec - tpend1.tv_sec) + (tpend2.tv_usec - tpend1.tv_usec) / 1000;
qDebug()<<"timer ms= "<<msec;
```

```c++
#include <QElapsedTimer>

QElapsedTimer ElapsedTimer;
ElapsedTimer.start();
   Mat ResultImg = m_ProcessObj->DetectProcess(img);
qDebug()<<"耗时"<<ElapsedTimer.elapsed()<<"毫秒";
```

### opengl

#### 1、顶点属性数量

我们能声明的顶点属性数量是有限的，可以通过下面的代码获取

```
int nrAttributes;
glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
```

openGL确保至少有16个包含4分量的顶点属性可用，但是有些硬件或许允许更多的项点属性

##### GLSL中包含C等其他语言大部分的默认基础数据类型

​		int    float   double     uint 和bool

##### GLSL也有两种容器类型

​	向量(Vector)

​				vecn:    n个默认为float类型向量，例如： vec2 vect = vec2(0.5, 0.7);  //两个float类型向量

​				bvecn:  boolean类型的向量

​				ivecn:    有符号整形向量

​				uvecn:    无符号整形向量

​				dvecn:   double类型向量

​	矩陈

### YUV格式转RGB

(YCrCb)是指将亮度参量Y和色度参量U/V分开表示的像素格式，主要用于优化彩色视频信号的传输。

```
R = Y + 1.403V
G = Y - 0.344U - 0.714V
B = Y + 1.770U

将RGB和YUV的范围均限制在[0, 255]间
R = Y + 1.403(V - 128）
G = Y - 0.344(U - 128) - 0.714(V - 128)
B = Y + 1.770(U - 128)
```

##### YUV采样

YUV相比于RGB格式最大的好处是可以做到在保持图像质量降低不明显的前提下，减小文件大小。TUV格式之所以能够做到，是因为进行了采样操作。

YUV码流的存储格式与其采样方式密切相关，主流的采样方式有三种：YUV 4:4:4**(YUV444)**，YUV 4:2:2**(YUV422)**，YUV 4:2:0**(YUV420)**。

若以以黑点表示采样该像素点的Y分量，以空心圆圈表示采用该像素点的UV分量，则这三种采样方式如下：

![](QT的使用/YUV.png)

即：

- YUV 4:4:4采样，每一个Y对应一组UV分量。
- YUV 4:2:2采样，每两个Y共用一组UV分量。
- YUV 4:2:0采样，每四个Y共用一组UV分量。

##### YUV存储格式

YUV存储可以分为两种：packed（打包）和planar（平面）；

- packed：Y、U、V分量穿插着排列，三个分量存在一个Byte型数组里；
- planar：Y、U、V分量分别存在三个Byte型数组中；

##### 常见的像素格式

1、YUV422：YUYV、YVYU、UYVY、VYUY

![](QT的使用/YUV02.png)

YUYV转YUV422p

```
    int count = 0;
    AVPacket pkt;
    while((ret = av_read_frame(fmt_ctx, &pkt)) == 0 && count++ < 200)
    {
        av_log(NULL, AV_LOG_INFO, "packet size is %d\n", pkt.size);
        //yuyv--->yuv422p
        //ffplay -video_size 640x480 -pixel_format yuv422p ~/Videos/video.yuv
        int len = WIDHT * HEIGHT;
        for(int i = 0;i < len; i++){
            avframe->data[0][i] = pkt.data[i*2];//Y
        }
        len = WIDHT * HEIGHT / 2;
        for(int i = 0;i < len; i++)
        {
            avframe->data[1][i] = pkt.data[i*4 + 1];//Cb
            avframe->data[2][i] = pkt.data[i*4 + 3];//Cr
        }
        fwrite(avframe->data[0], 1, WIDHT * HEIGHT, outfile);
        fwrite(avframe->data[1], 1, WIDHT * HEIGHT/2, outfile);
        fwrite(avframe->data[2], 1, WIDHT * HEIGHT/2, outfile);
        fflush(outfile);
    }
```

YUYV转YUV420p

```
![YUV03](Qt/YUV03.png)    int base = 0;
    while((ret = av_read_frame(fmt_ctx, &pkt)) == 0 && count++ < 200)
    {
        av_log(NULL, AV_LOG_INFO, "packet size is %d\n", pkt.size);
 
        //ffplay -video_size 640x480 -pixel_format yuv420p ~/Videos/video.yuv
 
        int len = WIDHT * HEIGHT;
        for(int i = 0;i < len; i++){
            avframe->data[0][i] = pkt.data[i*2];//Y
        }
 
        //yuyv 序列为YU YV YU YV，一个yuv422帧的长度 width * height * 2 个字节
        //yuyv --- >yuv420p丢弃偶数行 u v
        int cnt = 0;
        for(int i = 0; i < HEIGHT; i += 2)
        {
            for(int j = 0; j < WIDHT/2; j++)
            {
                avframe->data[1][cnt] = pkt.data[i * WIDHT * 2 + j * 4 + 1];//Cb
                avframe->data[2][cnt++] = pkt.data[i * WIDHT * 2 + j * 4 + 3];//Cr
            }
        }
        fwrite(avframe->data[0], 1, WIDHT * HEIGHT, outfile);
        fwrite(avframe->data[1], 1, WIDHT * HEIGHT/4, outfile);
        fwrite(avframe->data[2], 1, WIDHT * HEIGHT/4, outfile);
 
        //important!!!
        avframe->pts = base++;
 
        encode(codec_ctx, avframe, newpkt, encode_file);
        fflush(outfile);
 
    }
```

2、YUV420

![]()![YUV03](QT的使用/YUV03.png)

### QT软件信息

https://blog.csdn.net/u012878602/article/details/119682389

https://www.cnblogs.com/findumars/p/6272235.html

```c++
//设置相关信息
QCoreApplication::setApplicationName("APPNAME");
QCoreApplication::setApplicationVersion("1.0.0.0");
QCoreApplication::setOrganizationName("Qt");
QCoreApplication::setOrganizationDomain("www.qt.com");

//取出相关信息
QString appName = QCoreApplication::applicationName();
QString appVersion = QCoreApplication::applicationVersion();
QString orgDomain = QCoreApplication::organizationDomain();
QString orgName = QCoreApplication::organizationName();
```

### QTCreator出现提示无法覆盖文件

https://www.cnblogs.com/Rainingday/p/13841572.html

提示错误：[/home/xxx/.config/QtProject/Qtcreator/qtversion.xml : Permission denied的错误]

sudo运行qtcreator后，下次不使用sudo打开会报以上错误 /home/xxx/.config/QtProject/Qtcreator/qtversion.xml : Permission denied

因为在sudo下打开qtcreator时，会向用户根目录的.config下写数据，所以这个目录变为管理员权限，使用下述命令就是将.config的管理员权限取消。

举例ubuntu电脑登入名字为zhangya，那么在zhangya目录下输入(注意 . 前面有个空格)
sudo chown -R zhangya:root .config/

### qt远程布局到rk3288(ubuntu)设备上

<img src="QT的使用/qt远程布局文件_01.png" style="zoom: 80%;" />

在.pro文件中设置如下

```
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /home/gzpeite/Documents(这里修改需个部署的文件路径)
!isEmpty(target.path): INSTALLS += target
```

### Qt for Android环境配置

电脑系统说明：ubuntu18.04   64位操作系统

参考安装地址：

https://blog.csdn.net/zhounixing/article/details/89883581

https://blog.csdn.net/fangye945a/article/details/86585026

软件下载地址

| 软件名称    | 下载地址                                                  |
| ----------- | --------------------------------------------------------- |
| Qt          | https://download.qt.io/archive/qt/                        |
| Qt          | https://download.qt.io/new_archive/qt/                    |
| Android SDK | http://sdk.android-studio.org/                            |
| Android NDK | https://developer.android.google.cn/ndk/downloads/        |
| Android JDK | https://www.oracle.com/java/technologies/downloads/#java8 |
| ANT工具     | http://ant.apache.org/bindownload.cgi                     |

##### 1、下载说明

以上的几个软件下载完成后，可将几个软件放到一个目录下，我的软件是放在：~/java_software/ 目录下，下面所说的操作全是在该目录下进行。

Qt：

需要安装Qt for android，则必须要下载下图中的软件，版本可以不同，下载完后，进入下载目录；安装过程有些软件可以跳过注册，不能跳过注册的可以先将网络断开再安装。

输入命令，即可安装（安装流程可百度）：

​	sudo chmod +x qt-opensource-linux-x64-android-5.8.0.run

​	./qt-opensource-linux-x64-android-5.8.0.run  （一直点下一步即可，可以安装在其他目录）

![](QT的使用/qt安装_01.png)

Android SDK：

​	下载完成，解压到当前目录下，解压命令：tar -xzvf android-sdk_r24.4.1-linux.tgz

​	解压后会生成目录： ./android-sdk-linux/  目录

​	生成的目录的用户与用户组最好用当前登入的用户，如果不是当前登入用户(runoob)，用户组（runoobgroup），可按命令配置： chown -R runoob:runoobgroup document 

![](QT的使用/sdk安装_01.png)

Android NDK：

   r14b版本：https://dl.google.com/android/repository/android-ndk-r14b-linux-x86_64.zip

   r18b版本：https://dl.google.com/android/repository/android-ndk-r18b-linux-x86_64.zip

​	r14b版本与r18b版本任意一个即可

​	使用该地址可直接下载，下载高版本的NDK，本人下载最新版本使用会出现问题，所以使用r14b版本

​	下载完成，解压到当前目录下，解压命令：unzip android-ndk-r14b-linux-x86_64.zip

​	解压后会生成目录： ./android-ndk-r14b/  目录

​	生成的目录的用户与用户组最好用当前登入的用户，如果不是当前登入用户(runoob)，用户组（runoobgroup），可按命令配置： chown -R runoob:runoobgroup document 

Android JDK：

   下载完成，解压到当前目录下，解压命令：tar -xzvf jdk-18_linux-x64_bin.tar.gz

​	解压后会生成目录： ./jdk-18.0.1.1/  目录

​	生成的目录的用户与用户组最好用当前登入的用户，如果不是当前登入用户(runoob)，用户组（runoobgroup），可按命令配置： chown -R runoob:runoobgroup document 

​	![](QT的使用/jdk安装_01.png)

ANT工具：

​	可以下载1.9.16版本，也可以下载1.10.12版本，本人下载1.9.16版本

​	下载完成，解压到当前目录下，解压命令：tar -xzvf apache-ant-1.9.16-bin.tar.gz

​	解压后会生成目录： ./apache-ant-1.9.16/  目录

​	生成的目录的用户与用户组最好用当前登入的用户，如果不是当前登入用户(runoob)，用户组（runoobgroup），可按命令配置： chown -R runoob:runoobgroup document 

​		注意：也可通过命令一键安装：apt-get install ant

![](QT的使用/ant安装_01.png)

##### 2、其他工具部署

```shell
#由于是64位的虚拟机所以需要安装安装以下依赖项才能运行adb等32位可执行文件，这样可以让Qt Creator找到要部署的设备
sudo apt-get install libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386
#要运行模拟器，还需要以下依赖项
sudo apt-get install libsdl1.2debian:i386
```

##### 3、配置环境变量

方法一：直接在用户目录中配置，只能当前用户生效

```shell
cd ~
vim .profile

#输入以下代码
export ANT_HOME={你的安装目录}/apache-ant-1.9.16
export JAVA_HOME={你的安装目录}/jdk-18.0.1.1
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:${JRE_HOME}/bin:$PATH:${ANT_HOME}/bin

export NDK_HOME={你的安装目录}/android-ndk-r14b
export PATH=${NDK_HOME}:$PATH

export ANDROID_SDK_ROOT={你的安装目录}/android-sdk-linux
export ANDROID_SDK_HOME={你的安装目录}/android-sdk-linux
export PATH=${ANDROID_SDK_HOME}/tools:${ANDROID_SDK_HOME}/platform-tools:$PATH

#输入完后，保存退出
source .profile
```

方法二：在root目录中进行配置，这个配置后所有用户都生效

```shell
sudo vim /etc/profile
#输入方法一中的代码
{
  export 开头的代码
}
#输入完后，保存退出
source /etc/profile
```

方法三：该方法与方法二是一样的，只是更符合系统规则

```shell
cd /etc/profile.d
sudo vim myProfile.sh
#输入方法一中的代码
{
  export 开头的代码
}
#输入完后，保存退出
source /etc/profile
```

##### 4、验证环境变量

验证android命令:                   java -version

验证ant命令:							ant -version

##### 5、升级Android SDK

升级Android SDK，进入SDK目录下的Tools目录，用android工具更新SDK，命令：  ./android update sdk

![](QT的使用/sdk安装_02.png)

只有升级SDK，才能Qt中的配置问题去掉，这个升级时间会比较久，

##### 6、Qt环境配置

![](QT的使用/qtForAndroid环境配置_01.png)

### QT数据类型

| 名称        | 说明                                                         | 例子                                     |
| ----------- | ------------------------------------------------------------ | ---------------------------------------- |
| QVector     |                                                              |                                          |
| QMap        |                                                              |                                          |
| QHash       |                                                              |                                          |
| QLinkedList | 基于迭代器访问的lish，方便快速插入，删除                     |                                          |
| QList       | 一个列表模块类                                               |                                          |
| QVariant    | 这个变量是没有数据类型的，QVariant类像是最常见的Qt数据类型的一个共用体，一个QVariant对象在一个时间只保存一个单一类型的一个单一值<br />QVector的好处：<br />可以用count()函数获取数组的大小，方便遍历。（普通数组必须先预定义好数组大小，虽然QVector最好也要预定义大小，但是也可以不预定义）<br />可以用append()函数或者<<操作符，来往数组最后添加元素而不用担心溢出问题 |                                          |
| QByteArray  | 可以用count()函数获取数组的大小，方便遍历。（普通数组必须先预定义好数组大小，虽然QVector最好也要预定义大小，但是也可以不预定义） 可以用append()函数或者<<操作符，来往数组最后添加元素而不用担心溢出问题。QByteArray提供了一个【字节】数组，既可以储存原始字节（包括‘\0’），又可以储存惯例上8位以’\0’结尾的字符串（字符数组）。 | QByteArray与char *相互转换，代码参考下面 |
| QBuffer     |                                                              |                                          |

### Qt进程

1、Qt中的进程 --------QProcess

成员函数

```c++
start(命令， 参数);  -------启动进程执行可执行程序
kill();	--------结束进挰
state(); -------获取进程状态
    QProcess::Runing ------正在运行
    QProcess::NotRuning-----没有运行
waitForFinished();----------等待进程结束
    
//创建进程，一般不需要指定父进程
QProcess pro = new QProcess(); 或者  QProcess pro = new QProcess(this);

```

### Qt线程池

#### qt中常用的宏

| 宏名        | 说明 |
| ----------- | ---- |
| Q_OBJECT    |      |
| SIGNAL      |      |
| SLOT        |      |
| Q_SIGNALS   |      |
| Q_SLOTS     |      |
| Q_EMIT      |      |
| Q_INVOKABLE |      |
| Q_PROPERTY  |      |

#### QRunnable

代码

qrunnableme.h

```c++
#ifndef QRUNNABLEME_H
#define QRUNNABLEME_H

#include <QObject>
#include <QRunnable>
#include <QWidget>
#include <QDebug>
#include <QThread>

class QRunnableMe :public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit QRunnableMe(QObject *parent = nullptr);
    ~QRunnableMe();
protected:
    void run() override;
private:
    QObject * m_pObj = nullptr;
};
#endif // QRUNNABLEME_H
```

qrunnableme.cpp

```c++
#include "qrunnableme.h"

QRunnableMe::QRunnableMe(QObject *parent) : m_pObj(parent)
{
    //自动删除QRunnable, 注意这个标志必须在调用QThreadPool::start()之前设置。
    //在QThreadPool::start()之后调用setAutoDelete函数将出现未定义的行为。
    setAutoDelete(true);
}
QRunnableMe::~QRunnableMe()
{
    qDebug()<< __FUNCTION__;
}
void QRunnableMe::run()
{
    qDebug()<< __FUNCTION__ << QThread::currentThreadId();
    QThread::msleep(3000);
    QMetaObject::invokeMethod(m_pObj, "setText", Q_ARG(QString, "hellow runnable!"));
    qDebug()<<"QRunnableMe::run over , bye ...";
}
```

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QThreadPool>
#include "qrunnableme.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
    Q_INVOKABLE void setText(QString msg);
private:
    Ui::MainWidget *ui;
    QRunnableMe * m_pRunnableMe = nullptr;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    QThreadPool::globalInstance()->setMaxThreadCount(5);
    //设置超时时长为30秒
    QThreadPool::globalInstance()->setExpiryTimeout(30000);
    m_pRunnableMe = new QRunnableMe(this);
    qDebug()<< __FUNCTION__ << QThread::currentThreadId();
    QThreadPool::globalInstance()->start(m_pRunnableMe);
}
MainWidget::~MainWidget()
{
    delete ui;
    qDebug()<< __FUNCTION__;
}
void MainWidget::setText(QString msg)
{
    ui->label->setText(msg);
}
```



#### QtConcurrent

提供了高级api，使编写多线程程序时，不需要使用诸如互斥锁、读写锁等待条件或信号量等低级安全线程安全类

QT += concurrent

#### QFuture

QFuture类代表一个异步计算的结果，QFuture允许线程与一个或多个结果同步，这些结果将在稍后的时间点准备就绪，该结果可以是具有默认构造函数的任何类型，如果一个结果在调用result(),resultAt()或results()函数时不可用，QFuture将进行等待，直到结果可用为止，使用isResultReadyAt()函数可以检测结果是否已准备就绪。

代码

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QtConcurrent>
#include <QFuture>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private:
    static void fun1(void);
    static void fun2(QString str1, QString str2);
    static int fun3(int i, int j);

private:
    Ui::MainWidget *ui;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QList>
#include <QByteArray>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    qDebug()<< __FUNCTION__ << QThread::currentThreadId();
    QFuture<void> fut1 = QtConcurrent::run(fun1);
    QFuture<void> fut2 = QtConcurrent::run(fun2, QString("Thread"), QString("2"));
    int i = 1, j = 2;
    QFuture<int> fut3 = QtConcurrent::run(fun3, i, j);
    qDebug()<<"ret:"<<fut3.result();

    //以上的例子，如果要为其指定线程池，可以将线程池的指针作为第一个参数传递进去
    //由线程池的方法来执行fut4
    QThreadPool pool;
    QFuture<void> fut4 = QtConcurrent::run(&pool, fun1);

    fut1.waitForFinished();
    fut2.waitForFinished();
    fut3.waitForFinished();
    fut4.waitForFinished();

    QByteArray bytearray = "hello,world";
    QFuture<QList<QByteArray>> future = QtConcurrent::run(bytearray, &QByteArray::split, ',');
   QList<QByteArray> result = future.result();
   qDebug()<<"result:"<<result;
}
MainWidget::~MainWidget()
{
    delete ui;
}
void MainWidget::fun1()
{
    qDebug()<<__FUNCTION__ << QThread::currentThreadId();
}
void MainWidget::fun2(QString str1, QString str2)
{
    qDebug()<<__FUNCTION__<<str1 + str2 << QThread::currentThreadId();
}
int MainWidget::fun3(int i, int j)
{
    qDebug()<<__FUNCTION__<<QThread::currentThreadId();
    return i+j;
}
```

#### QThreadPool线程池

| 函数                | 说明                     |
| ------------------- | ------------------------ |
| activeThreadCount() | 表示线程池中活动线程数   |
| expiryTimeout()     | 线程活着的时间           |
| maxThreadCount()    | 表示线程池使用最大线程数 |

代码

qthreadpoolme.h

```c++
#ifndef QTHREADPOOLME_H
#define QTHREADPOOLME_H

#include <QObject>
#include <QRunnable>
#include <QDebug>
#include <QThread>

class QThreadPoolMe : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit QThreadPoolMe(QObject *parent = nullptr);
    ~QThreadPoolMe();
protected:
    void run() override;
signals:
    void mySignal();
};
#endif // QTHREADPOOLME_H
```

qthreadpoolme.cpp

```
#include "qthreadpoolme.h"

QThreadPoolMe::QThreadPoolMe(QObject *parent) : QObject(parent)
{
}
QThreadPoolMe::~QThreadPoolMe()
{
}
void QThreadPoolMe::run()
{
    qDebug()<<__FUNCTION__<<QThread::currentThreadId();
}
```

main.cpp

```c++
#include <QThreadPool>
#include <QApplication>
#include "qthreadpoolme.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QThreadPool pool;
    pool.setMaxThreadCount(3);
    for (int i=0; i<20; i++)
        pool.start(new QThreadPoolMe());
    return a.exec();
}
```

### QT使用sqlite

##### 1、相应的命令

```shell
1、安装sqlite3命令
sudo apt-get update
sudo apt-get install sqlite3
2、使用源码进行编译
https://www.sqlite.org/download.html
下载的源码，将源码拷贝到ubuntu的非共享目录解压
tar -xzvf sqlite-snapshot-201708031550.tar.gz
--1>配置
	cd sqlite-snapshot-201708031550
	./configure --prefix=/home/zhangya/sqlite (安装目录)
--2>编译
	make
--3>安装
	make install
	安装完成后sqlite3的可执行文件就会出现在/home/zhangya/sqlite/bin中，如果要直接使用可以将sqlite3文件拷贝到/usr/bin目录下
```

##### 2、sqlite的使用

```sqlite
sqlite3 数据库文件的路路径  //打开/创建
比如
	sqlite3 stutb1.db //打开 stub1.db数据库，如果没有这个数据，就会创建一个数据库文件

//基本操作命令
.exit/.quit  ---------退出数据库命令行
.help ---------帮助说明信息
.tables-------查看当前数据库中所有的表
.dump---------将数据库导入或导出文本文件
比如：
	sqlite3 stutb1.db .dump > stutb1.sql //将数据库导出成.sql文件
	sqlite3 stutb1.db < stutb1.sql	//将.sql文件数据导入到数据库中
```

##### 3、数据库访问的SQL语句

```sqlite
----------1、新建表格
create table 表名(字段名1 字段类型1,字段名2 字段类型2, 字段名3 字段类型3);
比如：
	//创建一个stutb1的表，表中有3个字段
	//分别是整数类型的学号id， 字符类型的name和整数类型的age
create table stutb1(id int, name char[20], age int);
//不存在则建表
create table if not exists stutb1(id int, name char[20], age int);
// unique：关键字字段内容不能重复, 这样id中的数据就不会重复
// primary key:表示ID为主键
// not null：约束表示在表中创建纪录时这些字段不能为 NULL
// autoincrement:自动增加1
create table if not exists stutb2(id int unique primary key not null, name char[20] not null, age int not null);

-----------2、删除表格
drop table 表名;

-----------3、表格中插入数据(插入数据的格式要与创建表格的字段一致)
insert into 表名 values(字段值1, 字段值2, 字段值3);
//字段值如果是字符串，必须用''（单引号）括起来
比如
	insert into stutb1 values(1001, '张飞', 23);
	insert into stutb1 values(1002, '李四', 70);
	insert into stutb1 values(1003, '张洪', 40);
	insert into stutb1 values(1004, '王五', 90);
	
如果id设置成自动增加(autoincrement),那么可以只传两个参数
	insert into stutb1(name,age) values('aa', 20);
	
---------4、查询表中的数据
//查询表中所有数据
select * from 表名
比如：
	select * from stutb1;
----->按条件查找
1>使用where指定查询条件
	select * from stutb1 where age=10; //查询年龄为10的所有字段数据
查询字符在一段范围的:select * from stutb1 where age>=50;//查询年龄大于50的所有字段数据
				select * from stutb1 where age>=80 or age<50;//查询年龄大于等于80或者小于50的所有字段数据
2>指定查询的字段
	select id,age from stutb1;
3>使用where+like实现模糊查询
	select * from stutb1 where name like '张%'; //查询name字段中字符以  张 开头的所有数据
4>使用 order by 实现查询结果按某个字段的值升序/降序输出
	select * from stutb1 order by age desc;	//查看表格按年龄字段数据降序排列
	select * from stutb1 order by age asc;	//升序排列
5>查询用户名密码是否相同 (如果有结果则登入成功，如果没结果则登入失败)
	select * from stutb1 where name='张飞' and age=23;
---------5、删除表中的条目
delete from 表名 where 条件;  //删除符合条件的的条目
比如
	delete from stutb1 where id=1002;
---------6、更新表中的条目
update 表名 set 字段名1=字段值1,字段名2=字段值2 where 条件;
比如
	update stutb1 set age=20,name='sss' where id=1003;
```

##### 4、sqlite中字段类型

```
---数字
int -----整型
smallint -----短整型
tinyint ------微整型(0-255)
bit -------- 0 or 1

float -------单精度浮点数
real---------双精度浮点数

---字符串
char --------非unicode定长字符串  小于8000 长度
varchar------非unicode变长字符串  小于8000 长度
text--------非unicode变长字符串  小于2^32-1长度

nchar --------unicode定长字符串  小于8000 长度
nvarchar------unicode变长字符串  小于8000 长度
ntext--------unicode变长字符串  小于2^32-1长度
```

##### 5、Qt中数据库模块

```
QT += sql
```

(1)数据库的驱动类 -----QSqlDatabase

```c++
1、addDatabase() ---- 添加数据库驱动
	//type : 需要传入数据库类型, 比如使用sqlite就写:  "QSQLITE"
	[static] QSqlDatabase QSqlDatabase::addDatabase(const QString &type, const QString &connectionName = QLatin1String(defaultConnection))
    
2、setDatabaseName() ------设置数据库文件路径
    //传入数据库路径
    void QSqlDatabase::setDatabaseName(const QString &name)
    
3、open() -------打开数据库
```

(2) 执行SQL语句的类--------QSqlQuery

```c++
1、exec()--------执行SQL语句
    //传入需要执行的sql语句(可以不加分号), 如果不传参数，就会用构造时传的SQL语句
    bool QSqlQuery::exec(const QString &query)
2、lastError()------获取错误原因
    QSqlError QSqlQuery::lastError() const
3、next()--------获取结果
4、value()-------取出结果
    QVariant QSqlQuery::value(int index) const
    QVariant QSqlQuery::value(const QString &name) const
```

##### 6、数据加密操作

```c++
1.创建一个加密对象
QCryptographicHash hash(QCryptographicHash::Md5);
2.放入要加密的数据
void QCryptographicHash::addData(const QByteArray & data);
//hash.addData(password.toUtf8());  ---- QString password;
3.获取加密结果（这个加密后是无法解密的，只有将加密数据全部存入数据库中）
QByteArray QCryptographicHash::result() const;
//hash.result();
```

##### 7、Qt数据库的数据模型-------QSqlTableModel

​	注意：建表功能在这个数据模型是没有的，还是需要用QSqlQuery(SQL语句)去进是进行建表

```c++
#include <QSqlTableModel>
1.绑定表（数据库的表）
    //传入表格名
	[virtual] void QSqlTableModel::setTable(const QString &tableName)
2.插入数据
    插入一条记录 --------insertRecord
    bool QSqlTableModel::insertRecord(int row, const QSqlRecord &record)
    插入一行--------insertRow
    bool QAbstractItemModel::insertRow(int row, const QModelIndex &parent = QModelIndex())
3.提交模型到数据库---------submit/submitAll
    [override virtual slot] bool QSqlTableModel::submit()
    [slot] bool QSqlTableModel::submitAll()
4.查询-------------select
    [virtual slot] bool QSqlTableModel::select()
    //查找时，可以设置设置过滤器和排序
    //传入的字符串格式类似于    字段名=字段值    的格式
    [virtual] void QSqlTableModel::setFilter(const QString &filter)
    //排序
    // column:哪一列进行排序
    // order：排序的方式（Qt::AscendingOrder 升序，Qt::DescendingOrder 降序）
    [virtual] void QSqlTableModel::setSort(int column, Qt::SortOrder order)
5、从模型中删除行--------removeRow/removeRows
    
```

##### 8、Qt数据库程序

1>非表格界面使用数据库

.pro文件中加      QT += sql

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QSqlDatabase>  //专用于连接，创建数据库
#include <QSqlQuery>	//专用于DML（数据操作语言）,DDL(数据定义语言)
#include <QSqlTableModel>

#include <QSqlQueryModel>
#include <QSqlError>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private slots:
    void on_pushButton_new_clicked();
    void on_pushButton_add_clicked();
    void on_pushButton_find_clicked();
    void on_pushButton_dele_clicked();
    void on_pushButton_charge_clicked();
    void on_lineEdit_dbname_textChanged(const QString &arg1);
private:
    Ui::MainWidget *ui;
    QSqlTableModel * mode;
    QSqlDatabase db;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QDebug>
#include <QMessageBox>
#include <QSqlRecord>
#include <QSqlError>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    //添加数据库驱动
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("stu.db");
    if (!db.open()) {
        qDebug()<<"database open error!";
    }
    mode = new QSqlTableModel(this);
    ui->tableView->setModel(mode);
}
MainWidget::~MainWidget()
{
    db.close();
    delete ui;
}
//建表
void MainWidget::on_pushButton_new_clicked()
{
    QString sql = QString("create table if not exists %1(id int unique primary key not null, name char[20], age int)").arg(ui->lineEdit_dbname->text());
    QSqlQuery query;
    if (!query.exec(sql))
    {
        qDebug()<<"create table error!";
        return ;
    }
    ui->lineEdit_dbname->setEnabled(false);
    ui->pushButton_new->setEnabled(false);

    ui->pushButton_add->setEnabled(true);
    ui->pushButton_dele->setEnabled(true);
    ui->pushButton_find->setEnabled(true);
    ui->pushButton_charge->setEnabled(true);
}
//插入
void MainWidget::on_pushButton_add_clicked()
{
    mode->setTable(ui->lineEdit_dbname->text());
    QSqlRecord record = mode->record();
    record.setValue("id", ui->spinBox_addid->value());
    record.setValue("name", ui->lineEdit_addname->text());
    record.setValue("age", ui->spinBox_addage->value());
    //下面填入-1表示从表的最后面加数据
    if (!mode->insertRecord(-1, record))
    {
        qDebug()<<"add table error"<<mode->lastError().text();
        return ;
    }
    ui->spinBox_addid->setValue(ui->spinBox_addid->value()+1);
    //将模型中的所有数据与数据库进行同步
    mode->submitAll();
    on_pushButton_find_clicked();
}
void MainWidget::on_pushButton_find_clicked()
{
    mode->setTable(ui->lineEdit_dbname->text());
    //mode->setFilter("id=1001");
    mode->select();
}
void MainWidget::on_pushButton_dele_clicked()
{
    mode->setTable(ui->lineEdit_dbname->text());
    mode->setFilter(QString("id=%1").arg(ui->spinBox_delid->value()));
    mode->select();
    if (mode->rowCount() == 1)
    {
        mode->removeRow(0);
        mode->submitAll();
    }
    on_pushButton_find_clicked();
}
void MainWidget::on_pushButton_charge_clicked()
{
    //先打到再修改后写回
    mode->setTable(ui->lineEdit_dbname->text());
    mode->setFilter(QString("id=%1").arg(ui->spinBox_chargeid->value()));
    mode->select();

    if (mode->rowCount() == 1 && ui->radioButton_chargeid->isChecked())
    {
        QSqlRecord record = mode->record(0);
        record.setValue("id", ui->spinBox_chargeid2->value());
        mode->setRecord(0, record);
    }
    mode->submitAll();
    on_pushButton_find_clicked();
}
void MainWidget::on_lineEdit_dbname_textChanged(const QString &arg1)
{
    if (arg1.isEmpty())
        ui->pushButton_new->setEnabled(false);
    else
        ui->pushButton_new->setEnabled(true);
}
```

> 2表格界面使用数据库

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private slots:
    void on_pushButton_add_clicked();
    void on_pushButton_save_clicked();
    void on_pushButton_redo_clicked();
    void on_pushButton_del_clicked();
    void on_pushButton_select_clicked();
    void on_lineEdit_dbname_textChanged(const QString &arg1);
    void on_pushButton_new_clicked();
private:
    Ui::MainWidget *ui;
    QSqlTableModel * mode;
    QSqlDatabase db;
};
#endif // MAINWIDGET_H

```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QDebug>
#include <QMessageBox>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    //添加数据库驱动
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("stu.db");
    if (!db.open()){
        qDebug()<<"database open error!";
        return ;
    }
    mode = new QSqlTableModel(this);
    //界面上添加一个  tableView 窗体控件
    ui->tableView->setModel(mode);
}
MainWidget::~MainWidget()
{
    db.close();
    delete ui;
}
void MainWidget::on_pushButton_new_clicked()
{
    QString sql = QString("create table if not exists %1(id int unique primary key not null, name char[20] not null, age int not null)").arg(ui->lineEdit_dbname->text());

    QSqlQuery query;
    if (!query.exec(sql))
    {
        qDebug()<<"create table error!";
        return ;
    }
    ui->lineEdit_dbname->setEnabled(false);
    ui->pushButton_new->setEnabled(false);

    mode->setTable(ui->lineEdit_dbname->text());
    //手动保存
    mode->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mode->select();
}
void MainWidget::on_pushButton_add_clicked()
{
    int row = mode->rowCount();
    mode->insertRow(row);
    int id = 100;
    mode->setData(mode->index(row, 0), id);
    mode->submitAll();
}
void MainWidget::on_pushButton_save_clicked()
{
    mode->submitAll();
}
void MainWidget::on_pushButton_redo_clicked()
{
    mode->revertAll();
}
void MainWidget::on_pushButton_del_clicked()
{
    int delrow = ui->tableView->currentIndex().row();
    mode->removeRow(delrow);

    if (QMessageBox::Yes == QMessageBox::warning(this, "删除", "确定删除本行吗?", QMessageBox::Yes|QMessageBox::No))
    {
        mode->submitAll();
    }else{
        mode->revert();
    }
}
void MainWidget::on_pushButton_select_clicked()
{
    mode->setTable(ui->lineEdit_dbname->text());
    mode->select();
}
void MainWidget::on_lineEdit_dbname_textChanged(const QString &arg1)
{
    if (arg1.isEmpty())
        ui->pushButton_new->setEnabled(false);
    else
        ui->pushButton_new->setEnabled(true);
}
```

##### 9、madplay音频播放器

安装命令:  sudo apt-get install madplay

##### 10、qt中的多媒体模块

1>音频播放器

.pro文件加：   QT += multimedia

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaPlaylist>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private slots:
    void on_pushButton_addmusic_clicked();
    void on_pushButton_delmusic_clicked();
    void on_pushButton_clemusic_clicked();
    void on_pushButton_play_clicked();
    void on_pushButton_stop_clicked();
    void on_pushButton_pause_clicked();
    void on_pushButton_up_clicked();
    void on_pushButton_down_clicked();
    void on_verticalSlider_sliderMoved(int position);
    void on_horizontalSlider_sliderMoved(int position);
private:
    Ui::MainWidget *ui;
    QMediaPlayer* player;
    QMediaPlaylist * playerList;
    QString music_duration;
};
#endif // MAINWIDGET_H

```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"
#include <QUrl>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    player = new QMediaPlayer(this);
    playerList = new QMediaPlaylist(this);
    //设置播放列表的播放模式
    playerList->setPlaybackMode(QMediaPlaylist::Loop);
    //设置播放器的播放列表
    player->setPlaylist(playerList);

    QObject::connect(player, &QMediaPlayer::durationChanged, this, [&](quint64 duration){
        ui->horizontalSlider->setRange(0, duration);
        music_duration = QString("%1:%2").arg(duration/1000/60).arg(duration/1000%60);
        ui->label_time->setText(QString("00:00/%1").arg(music_duration));
        ui->listWidget->setCurrentRow(playerList->currentIndex());
    });

    QObject::connect(player, &QMediaPlayer::positionChanged , this, [&](quint64 position){
        ui->horizontalSlider->setValue(position);
        ui->label_time->setText(QString("%1:%2/%3").arg(position/1000/60).arg(position/1000%60).arg(music_duration));
    });
}
MainWidget::~MainWidget()
{
    delete ui;
}
void MainWidget::on_pushButton_addmusic_clicked()
{
    QStringList music_files = QFileDialog::getOpenFileNames(this, "选择音乐文件", "/home/zhangya/", "*.mp3");

    //去重
    for (int i = 0; i<ui->listWidget->count(); i++)
    {
        for (int j=0; j<music_files.count(); j++)
        {
            if (ui->listWidget->item(i)->text() == music_files.at(j))
            {
                music_files.removeAt(j);
                break;
            }
        }
    }
    if (music_files.isEmpty())
        return ;

    //添加到playerList中
    for (int i=0; i<music_files.count(); i++)
    {
        playerList->addMedia(QUrl::fromLocalFile(music_files.at(i)));
    }
    ui->listWidget->addItems(music_files);
    ui->listWidget->setCurrentRow(0);

    ui->pushButton_delmusic->setEnabled(true);
    ui->pushButton_play->setEnabled(true);

    if (ui->listWidget->count() > 1)
    {
        ui->pushButton_up->setEnabled(true);
        ui->pushButton_down->setEnabled(true);
    }
}
void MainWidget::on_pushButton_delmusic_clicked()
{
    int row = ui->listWidget->currentRow();
    QListWidgetItem * item = ui->listWidget->takeItem(row);
    if (item)
        delete item;
    playerList->removeMedia(row);
    if (ui->listWidget->count() <= 1)
    {
        ui->pushButton_up->setEnabled(false);
        ui->pushButton_down->setEnabled(false);
    }
}
void MainWidget::on_pushButton_clemusic_clicked()
{
    ui->listWidget->clear();
    playerList->clear();
    ui->pushButton_delmusic->setEnabled(false);
    ui->pushButton_play->setEnabled(false);
    ui->pushButton_stop->setEnabled(false);
    ui->pushButton_pause->setEnabled(false);
    ui->pushButton_up->setEnabled(false);
    ui->pushButton_down->setEnabled(false);
}
void MainWidget::on_pushButton_play_clicked()
{
    int row = ui->listWidget->currentRow();
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_stop->setEnabled(true);
    ui->pushButton_pause->setEnabled(true);
}
void MainWidget::on_pushButton_stop_clicked()
{
    player->stop();
    ui->pushButton_stop->setEnabled(false);
    ui->pushButton_pause->setEnabled(false);
}
void MainWidget::on_pushButton_pause_clicked()
{
    if (player->state() == QMediaPlayer::PlayingState)
    {
        player->pause();
        ui->pushButton_pause->setText("继续");
    }
    else if (player->state() == QMediaPlayer::PausedState)
    {
        player->play();
        ui->pushButton_pause->setText("暂停");
    }
}
void MainWidget::on_pushButton_up_clicked()
{
    int row = ui->listWidget->currentRow();
    if (row == 0)
        row = ui->listWidget->count();
    row--;
    ui->listWidget->setCurrentRow(row);
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_pause->setText("暂停");
    ui->pushButton_pause->setEnabled(true);
}
void MainWidget::on_pushButton_down_clicked()
{
    int row = ui->listWidget->currentRow();
    row++;
    if (row > ui->listWidget->count()-1)
        row = 0;
    ui->listWidget->setCurrentRow(row);
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_pause->setText("暂停");
    ui->pushButton_pause->setEnabled(true);
}
void MainWidget::on_verticalSlider_sliderMoved(int position)
{
    player->setVolume(position);
    ui->label_vol->setText(QString("音量:%1").arg(position));
}
void MainWidget::on_horizontalSlider_sliderMoved(int position)
{
    player->setPosition(position);
}
```

1>视频频播放器

QMediaPlayer视频显示设置

```c++
void QMediaPlayer::setVideoOutput(QVideoWidget *output)
void QMediaPlayer::setVideoOutput(QGraphicsVideoItem *output)
void QMediaPlayer::setVideoOutput(QAbstractVideoSurface *surface)
```

需要在.pro文件中添加

```
QT += multimedia multimediawidgets
//头文件
#include <QVideoWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>
```

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QVideoWidget>
#include <QListWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private:
    bool eventFilter(QObject * watched, QEvent * event);
private slots:
    void on_pushButton_addvideo_clicked();
    void on_pushButton_delvideo_clicked();
    void on_pushButton_clevideo_clicked();
    void on_pushButton_play_clicked();
    void on_pushButton_stop_clicked();
    void on_pushButton_pause_clicked();
    void on_pushButton_up_clicked();
    void on_pushButton_down_clicked();
    void on_pushButton_full_clicked();
    void on_verticalSlider_sliderMoved(int position);
    void on_horizontalSlider_sliderMoved(int position);
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
private:
    Ui::MainWidget *ui;
    QMediaPlayer* player;
    QMediaPlaylist * playerList;
    QString video_duration;
    QVideoWidget * vw;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QKeyEvent>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    player = new QMediaPlayer(this);
    playerList = new QMediaPlaylist(this);
    vw = new QVideoWidget(this);
    //安装过滤器
    vw->installEventFilter(this);
    ui->horizontalLayout->addWidget(vw);
    player->setVideoOutput(vw);
    //设置播放列表的播放模式
    playerList->setPlaybackMode(QMediaPlaylist::Loop);
    //设置播放器的播放列表
    player->setPlaylist(playerList);
    QObject::connect(player, &QMediaPlayer::durationChanged, this, [&](quint64 duration){
        ui->horizontalSlider->setRange(0, duration);
        video_duration = QString("%1:%2:%3").arg(duration/1000/60/60).arg(duration/1000/60%60).arg(duration/1000%60);
        ui->label_time->setText(QString("00:00:00/%1").arg(video_duration));
        ui->listWidget->setCurrentRow(playerList->currentIndex());
    });
    QObject::connect(player, &QMediaPlayer::positionChanged , this, [&](quint64 position){
        ui->horizontalSlider->setValue(position);
        ui->label_time->setText(QString("%1:%2:%3/%4").arg(position/1000/60/60).arg(position/1000/60%60).arg(position/1000%60).arg(video_duration));
    });
}
MainWidget::~MainWidget()
{
    delete ui;
}
bool MainWidget::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent * keyEvent = dynamic_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Escape)
        {
            vw->setWindowFlag(Qt::SubWindow);
            vw->showNormal();
            vw->setParent(this);
            ui->horizontalLayout->addWidget(vw);
        }
    }
    return QWidget::eventFilter(watched, event);
}
void MainWidget::on_pushButton_addvideo_clicked()
{
    QStringList video_files = QFileDialog::getOpenFileNames(this, "选择视频文件", "/home/zhangya/Videos/", "*.mp4 *.avi");

    //去重
    for (int i = 0; i<ui->listWidget->count(); i++)
    {
        for (int j=0; j<video_files.count(); j++)
        {
            if (ui->listWidget->item(i)->text() == video_files.at(j))
            {
                video_files.removeAt(j);
                break;
            }
        }
    }
    if (video_files.isEmpty())
        return ;
    //添加到playerList中
    for (int i=0; i<video_files.count(); i++)
    {
        playerList->addMedia(QUrl::fromLocalFile(video_files.at(i)));
    }
    ui->listWidget->addItems(video_files);
    ui->listWidget->setCurrentRow(0);

    ui->pushButton_delvideo->setEnabled(true);
    ui->pushButton_play->setEnabled(true);

    if (ui->listWidget->count() > 1)
    {
        ui->pushButton_up->setEnabled(true);
        ui->pushButton_down->setEnabled(true);
    }
}
void MainWidget::on_pushButton_delvideo_clicked()
{
    int row = ui->listWidget->currentRow();
    QListWidgetItem * item = ui->listWidget->takeItem(row);
    if (item)
        delete item;
    playerList->removeMedia(row);
    if (ui->listWidget->count() <= 1)
    {
        ui->pushButton_up->setEnabled(false);
        ui->pushButton_down->setEnabled(false);
    }
}
void MainWidget::on_pushButton_clevideo_clicked()
{
    ui->listWidget->clear();
    playerList->clear();
    ui->pushButton_delvideo->setEnabled(false);
    ui->pushButton_play->setEnabled(false);
    ui->pushButton_stop->setEnabled(false);
    ui->pushButton_pause->setEnabled(false);
    ui->pushButton_up->setEnabled(false);
    ui->pushButton_down->setEnabled(false);
}
void MainWidget::on_pushButton_play_clicked()
{
    int row = ui->listWidget->currentRow();
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_stop->setEnabled(true);
    ui->pushButton_pause->setEnabled(true);
    ui->pushButton_full->setEnabled(true);
}
void MainWidget::on_pushButton_stop_clicked()
{
    player->stop();
    ui->pushButton_stop->setEnabled(false);
    ui->pushButton_pause->setEnabled(false);
}
void MainWidget::on_pushButton_pause_clicked()
{
    if (player->state() == QMediaPlayer::PlayingState)
    {
        player->pause();
        ui->pushButton_pause->setText("继续");
    }
    else if (player->state() == QMediaPlayer::PausedState)
    {
        player->play();
        ui->pushButton_pause->setText("暂停");
    }
}
void MainWidget::on_pushButton_up_clicked()
{
    int row = ui->listWidget->currentRow();
    if (row == 0)
        row = ui->listWidget->count();
    row--;
    ui->listWidget->setCurrentRow(row);
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_pause->setText("暂停");
    ui->pushButton_pause->setEnabled(true);
}
void MainWidget::on_pushButton_down_clicked()
{
    int row = ui->listWidget->currentRow();
    row++;
    if (row > ui->listWidget->count()-1)
        row = 0;
    ui->listWidget->setCurrentRow(row);
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_pause->setText("暂停");
    ui->pushButton_pause->setEnabled(true);
}
void MainWidget::on_pushButton_full_clicked()
{
    vw->setWindowFlag(Qt::Window);
    vw->showFullScreen();
}
void MainWidget::on_verticalSlider_sliderMoved(int position)
{
    player->setVolume(position);
    ui->label_vol->setText(QString("音量:%1").arg(position));
}
void MainWidget::on_horizontalSlider_sliderMoved(int position)
{
    player->setPosition(position);
}
void MainWidget::on_listWidget_itemDoubleClicked(QListWidgetItem *)
{
    int row = ui->listWidget->currentRow();
    playerList->setCurrentIndex(row);
    player->play();
    //设置音量
    player->setVolume(ui->verticalSlider->value());
    ui->pushButton_stop->setEnabled(true);
    ui->pushButton_pause->setEnabled(true);
}
```

ubuntu解码问题

出现问题：Warning: "找不到‘video/mpeg, mpegversion=(int)4, systemstream=(boolean)false, framerate=(fraction)2997/125, width=(int)720, height=(int)528’类型可用的解码器。

解决：sudo apt-get update 

​			sudo apt-get install gstreamer1.0-libav

windows解码问题

​	下载 LAV Filters : https://www.filehorse.com/download-lav-filters/download/

​	https://gitee.com/shanewfx/LAVFilters/repository/blazearchive/master.zip?Expires=1660535809&Signature=Key64eMwRsVjKc%2BLW7NfKPGldHPwF2lfetIO%2Frkz2%2BM%3D

​	安装LAV Filters时，需要将该软件安装到与QT安装目录同级下

​	<img src="QT的使用/qt_视频解码问题.png" style="zoom:80%;" />

##### 11、音频输入（alsa库）

```
alsa-lib-1.2.2.tar.bz2     ------alsa的核心支持库
alsa-utils-1.2.2.tar.bz2	-------alsa的工具库
```

移植alsa库

```
tar -xjvf xxx.tar.bz2
```

将文件拷贝到非共享目录下解压

1>核心库配置

```sh
cd alsa-lib-1.2.2
./configure --host=arm-linux-gnueabihf --prefix=/home/zhangya/software/qt/alsa --disable-python
make 
make install
```

2>工具库配置

```sh
cd alsa-utils-1.2.2
./configure --host=arm-linux-gnueabihf --prefix=/home/zhangya/software/qt/alsa --with-alsa-prefix=/home/zhangya/software/qt/alsa/lib --with-alsa-inc-prefix=/home/zhangya/software/qt/alsa/include --disable-alsamixer --disable-xmlto
make
make install
```

注意：如果编译安装时缺少某个文件的错误，直接手动在对应目录下创建文件

```
t-ja.gmo    ------alsaconf/po/t-ja.gmo
t-ru.gmo	------alsaconf/po/t-ru.gmo
```

3>将生成的/home/zhangya/software/qt/alsa 打包成  alsa.tar.xz

```
tar -cJvf alsa.tar.xz alsa/
```

4>将alsa.tar.xz文件拷贝到开发板上

![](QT的使用/alse库配置.png)

```shell
tar -Jxf alsa.tar.xz
##将三个库文件拷贝到开发板 /usr/lib  目录下
cp alsa/lib/libasound.so* /usr/lib -r
##将两个命令文件拷贝到开发板 /usr/bin  目录下
cp alsa/bin/arecord alsa/bin/aplay /usr/bin -r
##将配置文件拷贝到和ubuntu中安装相同的目录下
mkdir -p /home/zhangya/software/qt/alsa
cp alsa/share /home/zhangya/software/qt/alsa -r
```

5>使用录音和播放

```
arecord -d10 -c1 -r16000 -twav -fS16_LE xxx.WAV
	-d:录音时间
	-c：音轨
	-r：采样频率
	-t：封装格式
	-f:量化位数
aplay xxx.WAV
```

##### 12、Qt中的文字转语音 ------QTextToSpeech

QT版本要高于5.9

```
QT += texttospeech
#include <QTextToSpeech>
```

使用方法

1>选择语音引擎

```
[static] QStringList QTextToSpeech::availableEngines()
//构造时，需要有引擎对像输入
QTextToSpeech::QTextToSpeech(const QString &engine, QObject *parent = nullptr)
```

2>选择语言、声音类型、音量、音频和语速

```
void setLocale(const QLocale &locale)------语言
void setPitch(double pitch)
void setRate(double rate)------语速
void setVoice(const QVoice &voice)-----声音类型
void setVolume(double volume)--------音量
```

3>转换 （将文字转换成语音）

```
[slot] void QTextToSpeech::say(const QString &text)
```

##### 13、视频播放  -------mplayer

用法与madplay音频播放器类似

```
-quiet  ------不输出变化信息
-slave--------后台命令处理模式
-geometry-----指定播放位置（开发板无效）
-zoom -x 400 -y 800 ------指定显示大小为400*800
-af volume=xxx --------设置音量 （-200 到 +60）
```

##### 14、挂载开发板的空闲分区

```
1、格式化分区（只进行一次）
	mkfs.vfat /dev/mmcblk0p7(这个一定不要写错了)
2、挂载分区(创建一个空文件夹作为挂载点，每次启动都要执行)
	mount -t vfat /dev/mmcblk0p7 挂载点(/Even)
3、每次启动挂载
	vi /etc/profile
	添加   mount -t vfat /dev/mmcblk0p7 挂载点(/Even)
```

##### 15、Linux 开发Qt视频播放器遇到的坑

https://www.pudn.com/news/6294ca12bf399b7f353e4aa5.html

##### 16、qt摄像头

1>windows摄像头

```
QT += multimedia multimediawidgets
#include <QCameraInfo>
#include <QCamera>
```

摄像头信息类----------QCameraInfor

```
//获取有效摄像头信息
[static] QList<QCameraInfo> QCameraInfo::availableCameras(QCamera::Position position = QCamera::UnspecifiedPosition)
//获取摄像头的描述信息
QString QCameraInfo::description() const
//获取摄像关设备名称
QString QCameraInfo::deviceName() const
```

摄像头类--------QCamera

```
//构造函数
QCamera::QCamera(const QByteArray &deviceName, QObject *parent = Q_NULLPTR)
QCamera::QCamera(const QCameraInfo &cameraInfo, QObject *parent = Q_NULLPTR)
QCamera::QCamera(QCamera::Position position, QObject *parent = Q_NULLPTR)

//成员函数
//设置显示窗口
void QCamera::setViewfinder(QVideoWidget *viewfinder)
void QCamera::setViewfinder(QGraphicsVideoItem *viewfinder)
void QCamera::setViewfinder(QAbstractVideoSurface *surface)
//
```

摄像头截屏类---------QCameraImageCapture

```
QCameraImageCapture::QCameraImageCapture(QMediaObject *mediaObject, QObject *parent = Q_NULLPTR)
//截屏   file图片保存路径
[slot] int QCameraImageCapture::capture(const QString &file = QString())
//id表示截屏的编号
//preview表示截屏的图像
[signal] void QCameraImageCapture::imageCaptured(int id, const QImage &preview)
```

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QCameraInfo>
#include <QCamera>
#include <QMessageBox>
#include <QDebug>
#include <QVideoWidget>
#include <QCameraImageCapture>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private slots:
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
    void on_pushButton_cap_clicked();
private:
    Ui::MainWidget *ui;
    QCamera * ca;
    QVideoWidget * vw;
    QCameraImageCapture * cap;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    QList<QCameraInfo> infos = QCameraInfo::availableCameras();
    foreach (QCameraInfo info, infos) {
        ui->comboBox->addItem(info.deviceName());
    }
}
MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::on_pushButton_start_clicked()
{
    ca = new QCamera(ui->comboBox->currentText().toUtf8(), this);
    cap = new QCameraImageCapture(ca, this);
    QObject::connect(cap, &QCameraImageCapture::imageCaptured, this, [&](int id, const QImage &preview){
        qDebug()<<"id = "<<id;
        QPixmap map = QPixmap::fromImage(preview);
        map = map.scaled(ui->label_zp->size());
        ui->label_zp->setPixmap(map);
        //也可以在这个保存图片
    });
    vw = new QVideoWidget(ui->widget);
    vw->resize(ui->widget->size());
    ca->setViewfinder(vw);
    vw->show();
    ca->start();
    ui->pushButton_start->setEnabled(false);
    ui->pushButton_stop->setEnabled(true);
    ui->pushButton_cap->setEnabled(true);
}
void MainWidget::on_pushButton_stop_clicked()
{
    ca->stop();
    vw->hide();
    delete ca;
    delete vw;
    delete cap;
    ui->pushButton_start->setEnabled(true);
    ui->pushButton_stop->setEnabled(false);
    ui->pushButton_cap->setEnabled(false);
}
void MainWidget::on_pushButton_cap_clicked()
{
    //可以写入图片保存位置
    cap->capture();
}
```

##### 17、使用系统自带的V4L2摄像头库--------/dev/video6

```
使用命令查看摄像头设备
ls /dev/video*
```

V4L2摄像头的访问

```c++
//需要的头文件
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>   ------/usr/inlucde/linux/videodev2.h
----打开摄像头
int fd = open("/dev/video6", O_RDWR);
----获取摄像头功能参数
struct v4l2_capability {
        __u8    driver[16];
        __u8    card[32];
        __u8    bus_info[32];
        __u32   version;		---内核版本
        __u32   capabilities;	---功能参数
        __u32   device_caps;
        __u32   reserved[3];
};
struct v4l2_capability cap = {};
ioctl(fd, VIDIOC_QUERYCAP, &cap);
//判断是否为摄像头设备
if (cap.capabilities & V4L2_CAP_VIDEO_CAPTURE){
	//是一个摄像头
}
-----获取摄像头支持的格式
struct v4l2_fmtdesc {
        __u32               index;             /* 格式编号 */
        __u32               type;              /* 摄像头格式  V4L2_BUF_TYPE_VIDEO_CAPTURE */
        __u32               flags;
        __u8                description[32];   /* 描述信息 */
        __u32               pixelformat;       /* 类型格式      */
        __u32               reserved[4];
};
//类型格式
#define v4l2_fourcc(a, b, c, d)\
        ((__u32)(a) | ((__u32)(b) << 8) | ((__u32)(c) << 16) | ((__u32)(d) << 24))

struct v4l2_fmtdesc fmt = {};
fmt.index = 0;
fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  //获取摄像头格式
ioctl(fd, VIDIOC_ENUM_FMT, &fmt);
-------设置摄像头的采集通道
int index = 0;//使用通道0
ioctl(fd, VIDIOC_S_INPUT, &index);
-------设置/获取摄像头的采集格式和参数
struct v4l2_format {
        __u32    type;  //传入摄像头格式   V4L2_BUF_TYPE_VIDEO_CAPTURE
        union {
                struct v4l2_pix_format          pix;     /* V4L2_BUF_TYPE_VIDEO_CAPTURE */
                struct v4l2_pix_format_mplane   pix_mp;  /* V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE */
                struct v4l2_window              win;     /* V4L2_BUF_TYPE_VIDEO_OVERLAY */
                struct v4l2_vbi_format          vbi;     /* V4L2_BUF_TYPE_VBI_CAPTURE */
                struct v4l2_sliced_vbi_format   sliced;  /* V4L2_BUF_TYPE_SLICED_VBI_CAPTURE */
                struct v4l2_sdr_format          sdr;     /* V4L2_BUF_TYPE_SDR_CAPTURE */
                struct v4l2_meta_format         meta;    /* V4L2_BUF_TYPE_META_CAPTURE */
                __u8    raw_data[200];                   /* user-defined */
        } fmt;
};
//上面的struct v4l2_format的第二个参数里
struct v4l2_pix_format {
        __u32                   width;		//像素宽度
        __u32                   height;		//像素高度
        __u32                   pixelformat;//采集格式(摄像头支持的格式)  V4L2_PIX_FMT_YUYV 
        __u32                   field;          /* V4L2_FIELD_NONE */
        __u32                   bytesperline;   /* for padding, zero if unused */
        __u32                   sizeimage;
        __u32                   colorspace;     /* enum v4l2_colorspace */
        __u32                   priv;           /* private data, depends on pixelformat */
        __u32                   flags;          /* format flags (V4L2_PIX_FMT_FLAG_*) */
        union {
                /* enum v4l2_ycbcr_encoding */
                __u32                   ycbcr_enc;
                /* enum v4l2_hsv_encoding */
                __u32                   hsv_enc;
        };
        __u32                   quantization;   /* enum v4l2_quantization */
        __u32                   xfer_func;      /* enum v4l2_xfer_func */
};

struct v4l2_format format= {};
format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
format.fmt.pix.width = 640;
format.fmt.pix.height = 480;
format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
format.fmt.pix.field = V4L2_FIELD_NONE;
//设置格式
ioctl(fd, VIDIOC_S_FMT, &format);
//获取格式
ioctl(fd, VIDIOC_G_FMT, &format);
------流相关（如帧率）
/*	Stream type-dependent parameters
 */
struct v4l2_streamparm {
	__u32	 type;			/* enum v4l2_buf_type */
	union {
		struct v4l2_captureparm	capture;
		struct v4l2_outputparm	output;
		__u8	raw_data[200];  /* user-defined */
	} parm;
};
struct v4l2_streamparm stream_parm;
stream_parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//获取帧率
ioctl(fd, VIDIOC_G_PARM, &stream_parm);
//设置帧率
ioctl(fd, VIDIOC_S_PARM, &stream_parm);  
-------申请缓冲空间
struct v4l2_requestbuffers {
        __u32                   count;	//缓冲区的块数   一般给4块
        __u32                   type;           /* enum v4l2_buf_type */
        __u32                   memory;         /* enum v4l2_memory */
        __u32                   reserved[2];
};
struct v4l2_requestbuffers req = {};
req.count = 4;
req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
req.memory = V4L2_MEMORY_MMAP;  //表示申请的这块空间能够通过mmap映射
ioctl(fd, VIDIOC_REQBUFS, &req);
--------将缓冲区分配并映射到用户空间
struct v4l2_buffer {
        __u32                   index;//编号
        __u32                   type;//V4L2_BUF_TYPE_VIDEO_CAPTURE
        __u32                   bytesused;//使用的字节数，也就是一帧数据有多大
        __u32                   flags;
        __u32                   field;
        struct timeval          timestamp;
        struct v4l2_timecode    timecode;
        __u32                   sequence;
        /* memory location */
        __u32                   memory;  //V4L2_MEMORY_MMAP
        union {
                __u32           offset;//偏移
                unsigned long   userptr;
                struct v4l2_plane *planes;
                __s32           fd;
        } m;
        __u32                   length;//长度
        __u32                   reserved2;
        __u32                   reserved;
};

struct v4l2_buffer buf = {};
buf.index = xxx;//0-3  一共有4块
buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
buf.memory = V4L2_MEMORY_MMAP;
ioctl(fd, VIDIOC_QUERYBUF, &buf);

---------映射到用户空间
mmap(NULL, buf.length, ......., fd, buf.m.offset);

---------将申请的缓冲区加入到v4l2的采集队列中
ioctl(fd, VIDIOC_QBUF, &buf);

---------启动摄像头采集
enum v4l2_buf_type buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
ioctl(fd, VIDIOC_STREAMON, &buf_type);

---------从采集队列中获取摄像头采集的数据
while(1)
{
    //从采集队列中取出一帧数据
    ioctl(fd, VIDIOC_DQBUF, &buf);
    //将该帧数据拷贝出来
    memcpy(....);
    //将取出的帧放回队列
    ioctl(fd, VIDIOC_QBUF, &buf);
    //其他处理
    //显示
}

--------关闭摄像头采集
enum v4l2_buf_type buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
ioctl(fd, VIDIOC_STREAMOFF, &buf_type);
//解除映射
munmap();
//关闭设备文件
close(fd);
```

camera_test.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string.h>

#include <linux/videodev2.h>

typedef struct {
	char * start;
	size_t length;
}buffer_t;

buffer_t buffer[4];

buffer_t current;//保存取出的当前帧

int lcd_fd;
int * memp;
unsigned int sign3;

#define CAMERA_WIDTH        (320)
#define CAMERA_HEITHT       (240)

int yuyv2rgb(int y, int u, int v)
{
    unsigned int pixel24 = 0;
    unsigned char *pixel = (unsigned char *)& pixel24;
    int r, g, b;
    static int ruv, guv, buv;

    if (sign3)
    {
        sign3 = 0;
        ruv = 1159*(v-128);
        guv = 380*(u-128) + 813*(v-128);
        buv = 2018*(u-128);
    }
    r = (1164*(y-16)+ruv) / 1000;
    g = (1164*(y-16)-guv) / 1000;
    b = (1164*(y-16)+buv) / 1000;

    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;
    if (r < 0) r = 0;
    if (g < 0) g = 0;
    if (b < 0) b = 0;

    pixel[0] = r;
    pixel[1] = g;
    pixel[2] = b;

    return pixel24;
}

int yuyv2rgb0(unsigned char *yuv, unsigned char *rgb, unsigned int width, unsigned int height)
{
    unsigned int in, out;
    int y0, u, y1, v;
    unsigned int pixel24;
    unsigned char * pixel = (unsigned char *)&pixel24;
    unsigned int size = width * height * 2;

    for (in=0, out=0; in<size; in+=4, out+=6)
    {
        y0 = yuv[in+0];
        u = yuv[in+1];
        y1 = yuv[in+2];
        v = yuv[in+3];

        sign3 = 1;
        pixel24 = yuyv2rgb(y0, u, v);
        rgb[out+0] = pixel[0];
        rgb[out+1] = pixel[1];
        rgb[out+2] = pixel[2];

        pixel24 = yuyv2rgb(y1, u, v);
        rgb[out+3] = pixel[0];
        rgb[out+4] = pixel[1];
        rgb[out+5] = pixel[2];
    }

    return 0;
}

//LCD初始化
void lcd_init()
{
	lcd_fd = open("/dev/fb0", O_RDWR);
	if (lcd_fd == -1)
	{
		perror("open lcd");
		exit(-1);
	}
	
	memp = mmap(NULL, 800*480*4, PROT_READ|PROT_WRITE, MAP_SHARED, lcd_fd, 0);
	if (memp == MAP_FAILED)
	{
		perror("lcd mmap");
		exit(-1);
	}
}

void lcd_uninit()
{
	munmap(memp, 800*480*4);
	close(lcd_fd);
}

int main(int arc, char * arv[])
{
	lcd_init();
	
	int fd = open("/dev/video5", O_RDWR);
	if (fd == -1)
	{
		perror("open");
		exit(-1);
	}
	
	struct v4l2_capability cap = {};
	int res = ioctl(fd, VIDIOC_QUERYCAP, &cap);
	if (res == -1)
	{
		perror("open");
		exit(-1);
	}
	//判断是否为摄像头设备
	if (cap.capabilities & V4L2_CAP_VIDEO_CAPTURE){
		//是一个摄像头
		printf("is a capture device!\n");
	}else{
		printf("is not a capture device!\n");
		exit(-1);
	}
	
	struct v4l2_fmtdesc fmt = {};
	fmt.index = 0;
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  //获取摄像头格式
	while(res = ioctl(fd, VIDIOC_ENUM_FMT, &fmt) == 0)
	{
		fmt.index++;
		printf("pixelformat=%c%c%c%c, description=%s\n", fmt.pixelformat&0xff, (fmt.pixelformat>>8)&0xff, (fmt.pixelformat>>16)&0xff, (fmt.pixelformat>>24)&0xff, fmt.description);
	}
	
	int index = 0;//使用通道0
	res = ioctl(fd, VIDIOC_S_INPUT, &index);
	if (res == -1)
	{
		perror("vidioc s input");
		exit(-1);
	}
	
	struct v4l2_format format= {};
	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	format.fmt.pix.width = CAMERA_WIDTH;
	format.fmt.pix.height = CAMERA_HEITHT;
	format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	format.fmt.pix.field = V4L2_FIELD_NONE;
	//设置格式
	res = ioctl(fd, VIDIOC_S_FMT, &format);
	if (res == -1)
	{
		perror("vidioc s fmt");
		exit(-1);
	}
	
	struct v4l2_requestbuffers req = {};
	req.count = 4;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;  //表示申请的这块空间能够通过mmap映射
	res = ioctl(fd, VIDIOC_REQBUFS, &req);
	if (res == -1)
	{
		perror("vidioc reqbuffs");
		exit(-1);
	}

	size_t i = 0, max_len = 0;
	for (i=0; i<4; i++)
	{
		struct v4l2_buffer buf = {};
		buf.index = i;//0-3  一共有4块
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		res = ioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (res == -1)
		{
			perror("vidioc s querybuf");
			exit(-1);
		}
		//记录最大长度
		if (buf.length > max_len)
			max_len = buf.length;
		
		//映射
		buffer[i].length = buf.length;
		buffer[i].start = mmap(NULL, buf.length, PROT_READ|PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
		if (buffer[i].start == MAP_FAILED)
		{
			perror("map failed!\n");
			exit(-1);
		}
		//入队
		res = ioctl(fd, VIDIOC_QBUF, &buf);
		if (res == -1)
		{
			perror("vidioc qbuf");
			exit(-1);
		}
	}
	
	//申请临时缓冲区，用于存放摄像头数据
	current.start = malloc(max_len);
	if (current.start == NULL)
	{
		perror("malloc");
		exit(-1);
	}
	
	//启动摄像头
	enum v4l2_buf_type buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	res = ioctl(fd, VIDIOC_STREAMON, &buf_type);
	if (res == -1)
	{
		perror("vidioc stream on");
		exit(-1);
	}
	
	sleep(1);  //1s
	
	char rgb[CAMERA_WIDTH*CAMERA_HEITHT*3];
	//采集数据
	while(1)
	{
		struct v4l2_buffer buf = {};
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;

		//从采集队列中取出一帧数据
		res = ioctl(fd, VIDIOC_DQBUF, &buf);
		if (res == -1){
			perror("vidioc dqbuf");
		}
		//将该帧数据拷贝出来
		memcpy(current.start, buffer[buf.index].start, buf.bytesused);
		current.length = buf.bytesused;
		//将取出的帧放回队列
		res = ioctl(fd, VIDIOC_QBUF, &buf);
		if (res == -1){
			perror("vidioc qbuf");
		}
		
		//其他处理
		//显示
		//YUYV转RGB，需添加这段代码（可从网上搜）
		yuyv2rgb0(current.start, rgb, CAMERA_WIDTH, CAMERA_HEITHT);
		//显示到LCD
		int x = 0, y = 0;
		for (y=0; y<CAMERA_HEITHT; y++)
		{
			for (x=0; x<CAMERA_WIDTH; x++)
			{
				//800: 是整个屏幕的像素
				*(memp+y*800+x) = (rgb[3*(y*CAMERA_WIDTH+x)]<<16) | (rgb[3*(y*CAMERA_WIDTH+x)+1]<<8) | (rgb[3*(y*CAMERA_WIDTH+x)+2]);
			}
		}
	}
	//关闭摄像头
	res = ioctl(fd, VIDIOC_STREAMOFF, &buf_type);
	if (res == -1)
	{
		perror("vidioc stream off");
		exit(-1);
	}
	for (i=0; i<4; i++){
		munmap(buffer[i].start, buffer[i].length);
	}
	free(current.start);
	close(fd);
	lcd_uninit();

	return 0;
}
```

##### 18、v4l2与QT界面结合

方法1：使用Qt进程运行camera_test程序

方法2：使用Qt线程运行camera_test代码，获取到的数据通过信号传给Qt界面显示

camerathread.h

```c++
#ifndef CAMERATHREAD_H
#define CAMERATHREAD_H

#include <QObject>
#include <QThread>
#include <QImage>
#include <QDebug>

typedef struct {
    char * start;
    size_t length;
}buffer_t;

#define CAMERA_WIDTH        (320)
#define CAMERA_HEITHT       (240)

class CameraThread : public QThread
{
    Q_OBJECT
public:
    //explicit 防止意外转换
    explicit CameraThread(QObject *parent = nullptr);
    ~CameraThread();
public slots:
    void set_flag_slot();
protected:
    void run() override;
private:
    int yuyv2rgb(int y, int u, int v);
    int yuyv2rgb0(unsigned char *yuv, unsigned char * rgb, unsigned int width, unsigned int height);
    void camera_init();
    void camera_free();
signals:
    void send_image_signal(QImage image);
private:
    bool threadFlag;
    buffer_t buffer[4];
    buffer_t current;
    int fd;
    unsigned int sign3;
};

#endif // CAMERATHREAD_H
```

camerathread.cpp

```c++
#include "camerathread.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string.h>
#include <linux/videodev2.h>

CameraThread::CameraThread(QObject *parent) : QThread(parent)
{
    sign3 = 0;
    camera_init();
}

CameraThread::~CameraThread()
{
    camera_free();
}

void CameraThread::set_flag_slot()
{
    threadFlag = true;
}

void CameraThread::run()
{
    threadFlag = false;
    char *rgb = (char *)malloc(CAMERA_WIDTH*CAMERA_HEITHT*3);
    while (1)
    {
        struct v4l2_buffer buf = {};
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;

        //从采集队列中取出一帧数据
        int res = ioctl(fd, VIDIOC_DQBUF, &buf);
        if (res == -1)
        {
            perror("vidioc dqbuf");
        }
        //将该帧数据拷贝出来
        memcpy(current.start, buffer[buf.index].start, buf.bytesused);
        current.length = buf.bytesused;
        //将取出的帧放回队列
        res = ioctl(fd, VIDIOC_QBUF, &buf);
        if (res == -1){
            perror("vidioc qbuf");
        }

        //其他处理
        //YUYV转RGB，需添加这段代码（可从网上搜）
        yuyv2rgb0((unsigned char *)current.start, (unsigned char *)rgb, CAMERA_WIDTH, CAMERA_HEITHT);

        //发送信号
        QImage image = QImage((uchar *)rgb, CAMERA_WIDTH, CAMERA_HEITHT, QImage::Format_RGB888);
        emit send_image_signal(image);

        msleep(30);
        if (threadFlag)
        {
            delete rgb;
            break;
        }
    }
}

int CameraThread::yuyv2rgb(int y, int u, int v)
{
    unsigned int pixel24 = 0;
    unsigned char *pixel = (unsigned char *)&pixel24;
    int r, g, b;
    static int ruv, guv, buv;

    if (sign3)
    {
        sign3 = 0;
        ruv = 1159*(v-128);
        guv = 380*(u-128) + 813*(v-128);
        buv = 2018*(u-128);
    }
    r = (1164*(y-16)+ruv) / 1000;
    g = (1164*(y-16)-guv) / 1000;
    b = (1164*(y-16)+buv) / 1000;

    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;
    if (r < 0) r = 0;
    if (g < 0) g = 0;
    if (b < 0) b = 0;

    pixel[0] = r;
    pixel[1] = g;
    pixel[2] = b;

    return pixel24;
}

int CameraThread::yuyv2rgb0(unsigned char *yuv, unsigned char *rgb, unsigned int width, unsigned int height)
{
    unsigned int in, out;
    int y0, u, y1, v;
    unsigned int pixel24;
    unsigned char * pixel = (unsigned char *)&pixel24;
    unsigned int size = width * height * 2;

    for (in=0, out=0; in<size; in+=4, out+=6)
    {
        y0 = yuv[in+0];
        u = yuv[in+1];
        y1 = yuv[in+2];
        v = yuv[in+3];

        sign3 = 1;
        pixel24 = yuyv2rgb(y0, u, v);
        rgb[out+0] = pixel[0];
        rgb[out+1] = pixel[1];
        rgb[out+2] = pixel[2];

        pixel24 = yuyv2rgb(y1, u, v);
        rgb[out+3] = pixel[0];
        rgb[out+4] = pixel[1];
        rgb[out+5] = pixel[2];
    }

    return 0;
}

void CameraThread::camera_init()
{
    fd = open("/dev/video5", O_RDWR);
    if (fd == -1)
    {
        perror("open");
        exit(-1);
    }

    struct v4l2_capability cap = {};
    int res = ioctl(fd, VIDIOC_QUERYCAP, &cap);
    if (res == -1)
    {
        perror("open");
        exit(-1);
    }
    //判断是否为摄像头设备
    if (cap.capabilities & V4L2_CAP_VIDEO_CAPTURE){
        //是一个摄像头
        printf("is a capture device!\n");
    }else{
        printf("is not a capture device!\n");
        exit(-1);
    }

    struct v4l2_fmtdesc fmt = {};
    fmt.index = 0;
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  //获取摄像头格式
    while((res = ioctl(fd, VIDIOC_ENUM_FMT, &fmt)) == 0)
    {
        printf("pixelformat=%c%c%c%c, description=%s\n", fmt.pixelformat&0xff, (fmt.pixelformat>>8)&0xff, (fmt.pixelformat>>16)&0xff, (fmt.pixelformat>>24)&0xff, fmt.description);
        fmt.index++;
    }

    int index = 0;//使用通道0
    res = ioctl(fd, VIDIOC_S_INPUT, &index);
    if (res == -1)
    {
        perror("vidioc s input");
        exit(-1);
    }

    struct v4l2_format format= {};
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = CAMERA_WIDTH;
    format.fmt.pix.height = CAMERA_HEITHT;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    format.fmt.pix.field = V4L2_FIELD_NONE;
    //设置格式
    res = ioctl(fd, VIDIOC_S_FMT, &format);
    if (res == -1)
    {
        perror("vidioc s fmt");
        exit(-1);
    }

    struct v4l2_requestbuffers req = {};
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;  //表示申请的这块空间能够通过mmap映射
    res = ioctl(fd, VIDIOC_REQBUFS, &req);
    if (res == -1)
    {
        perror("vidioc reqbuffs");
        exit(-1);
    }

    size_t i = 0, max_len = 0;
    for (i=0; i<4; i++)
    {
        struct v4l2_buffer buf = {};
        buf.index = i;//0-3  一共有4块
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        res = ioctl(fd, VIDIOC_QUERYBUF, &buf);
        if (res == -1)
        {
            perror("vidioc s querybuf");
            exit(-1);
        }
        //记录最大长度
        if (buf.length > max_len)
            max_len = buf.length;

        //映射
        buffer[i].length = buf.length;
        buffer[i].start = (char *)mmap(NULL, buf.length, PROT_READ|PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
        if (buffer[i].start == MAP_FAILED)
        {
            perror("map failed!\n");
            exit(-1);
        }
        //入队
        res = ioctl(fd, VIDIOC_QBUF, &buf);
        if (res == -1)
        {
            perror("vidioc qbuf");
            exit(-1);
        }
    }

    //申请临时缓冲区，用于存放摄像头数据
    current.start = (char *)malloc(max_len);
    qDebug()<<"max_len = "<<max_len;
    if (current.start == NULL)
    {
        perror("malloc");
        exit(-1);
    }

    //启动摄像头
    enum v4l2_buf_type buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    res = ioctl(fd, VIDIOC_STREAMON, &buf_type);
    if (res == -1)
    {
        perror("vidioc stream on");
        exit(-1);
    }
}

void CameraThread::camera_free()
{
    enum v4l2_buf_type buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    int res = ioctl(fd, VIDIOC_STREAMOFF, &buf_type);
    if (res == -1)
    {
        perror("vidioc stream off");
        exit(-1);
    }
    for (size_t i=0; i<4; i++)
    {
        munmap(buffer[i].start, buffer[i].length);
    }
    free(current.start);
    close(fd);
}
```

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

#include "camerathread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
signals:
    void send_flag_signal();
private slots:
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
private:
    Ui::MainWidget *ui;
    CameraThread * thread;
};
#endif // MAINWIDGET_H
```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    thread = new CameraThread(this);
    QObject::connect(thread, &CameraThread::send_image_signal, this, [&](QImage image){
        QPixmap map = QPixmap::fromImage(image);
        map.scaled(ui->label->size());
        ui->label->setPixmap(map);
    });
    QObject::connect(this, &MainWidget::send_flag_signal, thread, &CameraThread::set_flag_slot);
}
MainWidget::~MainWidget()
{
    //需要关闭上面开辟的线程  thread
    thread->terminate();
    delete ui;
}
void MainWidget::on_pushButton_start_clicked()
{
    thread->start();
}
void MainWidget::on_pushButton_stop_clicked()
{
    emit send_flag_signal();
}
```

##### 19、系统串口实现

```c++
#include <termios.h>		//  /usr/include/termios.h
#include <unistd.h>

-------------结构体
#include <bits/termios.h>	//   /usr/include/x86_64-linux-gnu/bits
    						//开发板上的位置： /usr/include/arm-linux-gnueabihf/bits
struct termios
{
     tcflag_t c_iflag;   /* 输入模式标志 */
     tcflag_t c_oflag;   /* 输出模式标志 */
     tcflag_t c_cflag;   /* 控制模式标志 */
     tcflag_t c_lflag;   /* 本地模式标志 */
     cc_t c_line;      /* 线路规程 */
     cc_t c_cc[NCCS];    /* 控制属性 */
     speed_t c_ispeed;   /* 输入速度 */
     speed_t c_ospeed;   /* 输出速度 */
#define _HAVE_STRUCT_TERMIOS_C_ISPEED 1
#define _HAVE_STRUCT_TERMIOS_C_OSPEED 1
};

------------成员函数
命令： man tcgetattr
int tcgetattr(int fd, struct termios *termios_p);
int tcsetattr(int fd, int optional_actions, const struct termios *termios_p);
int tcsendbreak(int fd, int duration);
int tcdrain(int fd);
int tcflush(int fd, int queue_selector);
int tcflow(int fd, int action);
void cfmakeraw(struct termios *termios_p);
speed_t cfgetispeed(const struct termios *termios_p);
speed_t cfgetospeed(const struct termios *termios_p);
int cfsetispeed(struct termios *termios_p, speed_t speed);
int cfsetospeed(struct termios *termios_p, speed_t speed);
int cfsetspeed(struct termios *termios_p, speed_t speed);
```

开发板串口设备文件---------/dev/ttyS0..1..2..3..4

开发板串口配置流程

```
----------1、打开串口设备文件
//O_NOCTTY:打开的串口不作为控制终端
serial_fd = open("/dev/ttyS0", O_RDWR|O_NOCTTY);

----------2、备份串的配置参数（非必须操作步骤）
struct termios old_cfg;
struct termios new_cfg;

tcgetattr(serial_fd, &old_cfg);
----------3、配置串口为原始模式
new_cfg = old_cfg;
cfmakeraw(&new_cfg);
----------4、配置输入/输出波特率
cfsetispeed(&new_cfg, B115200);
cfsetospeed(&new_cfg, B115200);
----------5、配置控制标志
new_cfg.c_cflag |= CREAD|CLOCAL;  //使能数据接收和本地模式
----------6、配置帧结构
new_cfg.c_cflag &= ~CSTOPB;		//1位停止位
new_cfg.c_cflag &= ~CSIZE;		//去掉数据位屏蔽
new_cfg.c_cflag |= CS8;			//8位数据位
new_cfg.c_cflag &= ~PARENB;		//无校验
----------7、配置阻塞模式
tcflush(serial_fd, TCIOFLUSH);	//清除缓冲区

//收到1字节清除阻塞
new_cfg.c_cc[VTIME] = 0;	//等待时间，ms
new_cfg.c_cc[VMIN] = 1;		//接收到几个字节清除阻塞

tcflush(serial_fd, TCIOFLUSH);
----------8、将配置结构写入系统
//TCSANOW:立即生效
//TCSADRAIN:输入输出完成后生效
//TCSAFLUSH:刷新缓冲区后生效
tcsetattr(serial_fd, TCSANOW, &new_cfg);
----------9、使用read/write发送接收数据
```

serial_test.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <termios.h>
#include <unistd.h>

int serial_fd;
struct termios old_cfg;
struct termios new_cfg;

void usrt0_init(void)
{
	serial_fd = open("/dev/ttyS0", O_RDWR|O_NOCTTY);
	if (serial_fd == -1)
	{
		perror("open");
		exit(-1);
	}
	
	int res = tcgetattr(serial_fd, &old_cfg);
	if (res == -1)
	{
		perror("tcgetattr");
		exit(-1);
	}
	
	new_cfg = old_cfg;
	cfmakeraw(&new_cfg);
	
	res = cfsetispeed(&new_cfg, B115200);
	if (res == -1)
	{
		perror("cfsetispeed");
		exit(-1);
	}
	res = cfsetospeed(&new_cfg, B115200);
	if (res == -1)
	{
		perror("cfsetospeed");
		exit(-1);
	}
	
	new_cfg.c_cflag |= CREAD|CLOCAL;
	new_cfg.c_cflag &= ~CSTOPB;		//1位停止位
	new_cfg.c_cflag &= ~CSIZE;		//去掉数据位屏蔽
	new_cfg.c_cflag |= CS8;			//8位数据位
	new_cfg.c_cflag &= ~PARENB;		//无校验
	
	tcflush(serial_fd, TCIOFLUSH);
	new_cfg.c_cc[VTIME] = 0;	//等待时间，ms
	new_cfg.c_cc[VMIN] = 1;		//接收到几个字节清除阻塞
	tcflush(serial_fd, TCIOFLUSH);
	
	res = tcsetattr(serial_fd, TCSANOW, &new_cfg);
	if (res == -1)
	{
		perror("tcsetattr");
		exit(-1);
	}
}

int main(int argc, char * argv[])
{
	usrt0_init();
	
	int i = 0;
	while(i < 20)
	{
		write(serial_fd, "A", 1);
		sleep(1);
		i++;
	}
	
	int res = tcsetattr(serial_fd, TCSANOW, &old_cfg);
	if (res == -1)
	{
		perror("tcsetattr old_cfg");
		exit(-1);
	}
	close(serial_fd);
	
	return 0;
}
```

##### 20、Qt串口类使用

mainwidget.h

```c++
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QMessageBox>
#include <QDebug>
#include <QSerialPort>
#include <QSerialPortInfo>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();
private slots:
    void on_pushButton_open_clicked();
    void on_pushButton_send_clicked();
private:
    Ui::MainWidget *ui;
    QSerialPort * ser;
};
#endif // MAINWIDGET_H

```

mainwidget.cpp

```c++
#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    foreach (const QSerialPortInfo & info, QSerialPortInfo::availablePorts()) {
        QSerialPort serial(info);
        qDebug()<<"port name:"<<info.portName();

        if (serial.open(QIODevice::ReadWrite))
        {
            ui->comboBox_port->addItem(info.portName());
            serial.close();
        }else{
            ui->comboBox_port->addItem(info.portName()+"used");
        }
    }
}
MainWidget::~MainWidget()
{
    delete ui;
}
void MainWidget::on_pushButton_open_clicked()
{
    if (ui->pushButton_open->text() == "打开串口")
    {
        ser = new QSerialPort(ui->comboBox_port->currentText());
        if (!ser->open(QIODevice::ReadWrite))
        {
            QMessageBox::warning(this, "警告", "串口打开失败");
            ser->deleteLater();
            return ;
        }
        switch (ui->comboBox_baudrate->currentIndex()) {
            case 0:ser->setBaudRate(QSerialPort::Baud115200);break;
            case 1:ser->setBaudRate(QSerialPort::Baud2400);break;
            case 2:ser->setBaudRate(QSerialPort::Baud4800);break;
            case 3:ser->setBaudRate(QSerialPort::Baud9600);break;
            case 4:ser->setBaudRate(QSerialPort::Baud38400);break;
        }
        switch (ui->comboBox_databit->currentIndex()) {
            case 0:ser->setDataBits(QSerialPort::Data8);break;
            case 1:ser->setDataBits(QSerialPort::Data7);break;
            case 2:ser->setDataBits(QSerialPort::Data6);break;
            case 3:ser->setDataBits(QSerialPort::Data5);break;
        }
        switch (ui->comboBox_checkbit->currentIndex()) {
            case 0:ser->setParity(QSerialPort::NoParity);break;
            case 1:ser->setParity(QSerialPort::OddParity);break;
            case 2:ser->setParity(QSerialPort::EvenParity);break;
        }
        switch (ui->comboBox_stopbit->currentIndex()) {
            case 0:ser->setStopBits(QSerialPort::OneStop);break;
            case 1:ser->setStopBits(QSerialPort::OneAndHalfStop);break;
            case 2:ser->setStopBits(QSerialPort::TwoStop);break;
        }
        ser->setFlowControl(QSerialPort::NoFlowControl);
        QObject::connect(ser, &QSerialPort::readyRead, this, [&](){
            QByteArray arr = ser->readAll();
            if (arr.isEmpty())
                return ;
            ui->textBrowser->append(arr);
        });

        ui->comboBox_port->setEnabled(false);
        ui->comboBox_databit->setEnabled(false);
        ui->comboBox_stopbit->setEnabled(false);
        ui->comboBox_baudrate->setEnabled(false);
        ui->comboBox_checkbit->setEnabled(false);
        ui->pushButton_send->setEnabled(true);
        ui->pushButton_open->setText("关闭串口");
    }else{
        ser->clear();
        ser->close();
        ser->deleteLater();
        ui->comboBox_port->setEnabled(true);
        ui->comboBox_databit->setEnabled(true);
        ui->comboBox_stopbit->setEnabled(true);
        ui->comboBox_baudrate->setEnabled(true);
        ui->comboBox_checkbit->setEnabled(true);
        ui->pushButton_send->setEnabled(false);
        ui->pushButton_open->setText("打开串口");
    }
}

void MainWidget::on_pushButton_send_clicked()
{
    QByteArray arr = ui->textEdit->toPlainText().toUtf8();
    ser->write(arr);
}
```

##### 21、配置opencv

https://github.com/liuruoze/EasyPR

https://www.jianshu.com/p/22617e584f28

##### 22、使用TCP

```
QT += network
```

头文件

```c++
#include <QTcpServer>
#include <QTcpSocket>
```

服务器程序

mainwidget.h

```
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private:
    void init(void);

private slots:
    void on_pushButton_bind_clicked();
    void on_pushButton_send_clicked();

    void doProcessNewConnection();
    void doProcessAcceptError(QAbstractSocket::SocketError socketError);
    void doProcessDisconnected();
    void doProcessReadyRead();
    void doProcessError(QAbstractSocket::SocketError err);

private:
    Ui::MainWidget *ui;

    QTcpServer * m_server;

    QList<QTcpSocket *> m_arrayClient;
};
#endif // MAINWIDGET_H

```

mainwidget.cpp

```
#include "mainwidget.h"
#include "ui_mainwidget.h"

#define MAXNUM      20

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    init();
    setWindowTitle("服务器");
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::init()
{
    m_server = new QTcpServer(this);
}


void MainWidget::on_pushButton_bind_clicked()
{
    /*
     *方法一：获取本机所有ip地址
     */
#if 0
    QString addrStr;
    QList<QHostAddress> addrs = QNetworkInterface::allAddresses();
    for (int i=0; i<addrs.length(); i++)
    {
        QHostAddress addr = addrs.at(i);
        qDebug()<<addr.toString();
        if (addr.toString().contains("192.")){
            addrStr = addr.toString();
            break;
        }
    }
#endif

    /*
     * 方法二：
     */
    QString serverIP = ui->lineEdit_server_ip->text();
    QString serverPort = ui->lineEdit_server_port->text();

    QString msg;
    bool ret = m_server->listen(QHostAddress(serverIP), serverPort.toUInt());
    if (!ret) {
        msg = "绑定失败!";
    }else{
        msg = "绑定成功!";
        ui->pushButton_bind->setEnabled(false);
    }
    ui->textEdit_show->append(msg);

    m_server->setMaxPendingConnections(MAXNUM);

    QObject::connect(m_server, &QTcpServer::newConnection, this, &MainWidget::doProcessNewConnection);
    QObject::connect(m_server, &QTcpServer::acceptError, this, &MainWidget::doProcessAcceptError);
}

void MainWidget::on_pushButton_send_clicked()
{
    QString clientIP = ui->lineEdit_client_ip->text();
    QString clientPort = ui->lineEdit_client_port->text();

    for (int i=0; i<m_arrayClient.length(); i++)
    {
        if ((m_arrayClient.at(i)->peerAddress().toString() == clientIP) || (m_arrayClient.at(i)->peerPort() == clientPort.toUInt())){
            QString msg = ui->textEdit_send->toPlainText();
            m_arrayClient.at(i)->write(msg.toUtf8());
            ui->textEdit_send->clear();
            break;
        }
    }
}

void MainWidget::doProcessNewConnection()
{
    QTcpSocket * client = m_server->nextPendingConnection();
    m_arrayClient.append(client);
    QString msg = QString("客户端[%1:%2] 连入!").arg(client->peerAddress().toString()).arg(client->peerPort());
    ui->textEdit_show->append(msg);

    //读取数据
    QObject::connect(client, &QTcpSocket::disconnected, this, &MainWidget::doProcessDisconnected);
    //读取内容
    QObject::connect(client, &QTcpSocket::readyRead, this, &MainWidget::doProcessReadyRead);
    QObject::connect(client, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error), this, &MainWidget::doProcessError);
}

void MainWidget::doProcessAcceptError(QAbstractSocket::SocketError socketError)
{
    qDebug()<<socketError;
}

void MainWidget::doProcessDisconnected()
{
    QTcpSocket * client = (QTcpSocket *)this->sender();
    QString msg = QString("客户端[%1:%2] 退出!").arg(client->peerAddress().toString()).arg(client->peerPort());
    ui->textEdit_show->append(msg);

    for (int i=0; i<m_arrayClient.length(); i++)
    {
        if ((m_arrayClient.at(i)->peerAddress() == client->peerAddress()) && (m_arrayClient.at(i)->peerPort() == client->peerPort())){
            m_arrayClient.removeAt(i);
            break;
        }
    }
}

void MainWidget::doProcessReadyRead()
{
    QTcpSocket * client = static_cast<QTcpSocket *>(this->sender());
    QString msg = QString("客户端[%1:%2] 说：").arg(client->peerAddress().toString()).arg(client->peerPort());
    while (!client->atEnd()) {
        msg.append(client->readAll());
    }
    ui->textEdit_show->append(msg);
}

void MainWidget::doProcessError(QAbstractSocket::SocketError err)
{
    qDebug()<<err;
}

```

客户端程序

mainwidget.h

```
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private slots:
    void on_pushButton_connect_clicked();
    void on_pushButton_send_clicked();

    void doProcessConnected();
    void doProcessReadyRead();
    void doProcessDisconnected();
    void doProcessError(QAbstractSocket::SocketError);

private:
    void init();

private:
    Ui::MainWidget *ui;

    QTcpSocket * m_client;
};
#endif // MAINWIDGET_H

```

mainwidget.cpp

```
#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    init();
    setWindowTitle("客户端");
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::init()
{
    m_client = new QTcpSocket(this);
}

void MainWidget::on_pushButton_connect_clicked()
{
    QString serverIP = ui->lineEdit_server_ip->text();
    QString serverPort = ui->lineEdit_server_port->text();

    m_client->connectToHost(QHostAddress(serverIP), serverPort.toUInt());

    QObject::connect(m_client, &QTcpSocket::connected, this, &MainWidget::doProcessConnected);
    QObject::connect(m_client, &QTcpSocket::readyRead, this, &MainWidget::doProcessReadyRead);
    QObject::connect(m_client, &QTcpSocket::disconnected, this, &MainWidget::doProcessDisconnected);
    QObject::connect(m_client, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error), this, &MainWidget::doProcessError);
}

void MainWidget::on_pushButton_send_clicked()
{
    QString msg = ui->textEdit_send->toPlainText();
    int ret = m_client->write(msg.toUtf8());
    if (ret <= 0)
        return ;
    ui->textEdit_send->clear();
}

void MainWidget::doProcessConnected()
{
    QString msg = QString("连接服务器成功!");
    ui->textEdit_show->append(msg);
    ui->pushButton_connect->setEnabled(false);
}

void MainWidget::doProcessReadyRead()
{
    QString msg = QString("服务器[%1:%2] 说:").arg(m_client->peerAddress().toString()).arg(m_client->peerPort());
    while (!m_client->atEnd())
    {
        msg.append(m_client->readAll());
    }
    ui->textEdit_show->append(msg);
}

void MainWidget::doProcessDisconnected()
{
    QString msg = QString("服务器断开!");
    ui->textEdit_show->append(msg);
    ui->pushButton_connect->setEnabled(true);
}

void MainWidget::doProcessError(QAbstractSocket::SocketError err)
{
    qDebug()<<err;
}

```

### QString的常见用法

QString类，提供一个unicode字符串。在QT软件开发平台中常用到的一种变量类型，其提供了很多方便的应用方法。下面介绍一些关于QString的常见用法。

***\*1.字符串末尾追加\****

```cpp
//example 1
   QString str="hello";
   str.append("world");
   qDebug() <<str;    //str=hello world
//example 2
   QString str="hello";
   str=str+"world";
   qDebug() <<str;     //str=hello world
//example 3
   QString str="hello";
   str+="world";
   qDebug() <<str;     //str=hello world
```

**2.取出字符串中指定位置的字符**

```cpp
    QString str="hello world";
    char ch;
    ch=str.at(1).unicode();
    qDebug() <<ch;  //ch=e
```

***\*3.从字符串末尾删除若干个字符\****

```cpp
    QString str="hello world";
    str.chop(2);
    qDebug() <<str; //str= hello wor
```

***\*4.清空字符串\****

```cpp
    //example 1
    QString str="hello world";
    str.clear();
    qDebug() <<str.size(); //str=""
    //example 2
    QString str="hello world";
    str="";
    qDebug() <<str.size(); //str=""
```

**5.字符串比较**

```cpp
    int x = QString::compare("aUtO", "AuTo", Qt::CaseInsensitive);  // x == 0
    int y = QString::compare("aUtO", "AuTo", Qt::CaseSensitive);    // x > 0
    int z = QString::compare("auto", "Car", Qt::CaseSensitive);     // y > 0
    int k = QString::compare("auto", "Car", Qt::CaseInsensitive);   // z < 0
    //CaseInsensitive：不区分大小写  CaseSensitive：区分大小写
```

 **6.字符串str1是否包含str2字符串**

```cpp
    QString str1 = "Peter Pan";
    QString str2 = "peter";
    bool rel;
    rel=str1.contains(str2, Qt::CaseInsensitive);   //CaseInsensitive:不区分大小写
    qDebug() <<rel; // returns true
```

 **7.字符串str2在字符串str1中出现过几次**

```cpp
    QString str1 = "HELLO world hello world";
    QString str2 = "hello";
    int rel;
    rel=str1.count(str2, Qt::CaseInsensitive); //搜索不区分大小写
    qDebug() <<rel; // rel=2
```

 **8.字符串str1是否以str2结尾**

```cpp
    QString str = "Bananas";
    str.endsWith("anas");         // returns true
    str.endsWith("pple");         // returns false
```

**9.搜索字符串str2在字符串str1第一次出现的位置**

```cpp
    QString str1 = "sticky question"; //包含了2个 “sti”
    QString str2 = "sti";
    //从位置0开始搜索
    str1.indexOf(str2);               // returns 0  (第一次出现位置0))
    //从位置1开始搜索
    str1.indexOf(str2, 1);            // returns 10 (第一次出现位置10))
    //从位置10开始搜索
    str1.indexOf(str2, 10);           // returns 10 (第一次出现位置10)
    //从位置11开始搜索
    str1.indexOf(str2, 11);           // returns -1 (不存在)
```

***\*10.在字符串str1指定位置中插入str2\****

```cpp
    QString str1 = "Meal";
    QString str2 = "ontr";
    str1.insert(1, str2); //从位置1开始插入
    // str1 == "Montreal"
```

**11.判断字符串str是否为空，是否没有字符**

```cpp
    //字符串没有字符
    QString().isEmpty();            // returns true
    QString("").isEmpty();          // returns true
    QString("x").isEmpty();         // returns false
    QString("abc").isEmpty();       // returns false
    //字符串为空
    QString().isNull();             // returns true
    QString("").isNull();           // returns false
    QString("abc").isNull();        // returns false
```

**12.计算字符串str长度**

```cpp
    QString str = "hello";
    int rel=str.length();
    qDebug() <<rel; // rel=5
```

**13.从字符串str1中指定位置截取字符**

```cpp
    QString str1 = "Nine pineapples";
    //从位置5开始截取，截取4个字符
    QString str2 = str1.mid(5, 4);      // str2 == "pine"
     //从位置5开始截取，截取到最后
    QString str3 = str1.mid(5);         // str2 == "pineapples"
```

**14.从字符串str中指定位置开始移除若干个字符**

```cpp
    QString str = "Montreal";
    //从位置1开始，移除4个字符
    str.remove(1, 4);// str == "Meal"
```

**15.从str1字符串中指定位置开始替换若干个字符**

```cpp
    QString str1 = "Say yes!";
    QString str2 = "no";
    //从位置4开始，替换3个字符
    str1.replace(4, 3, str2); // str1 == "Say no!"
```

**16.以指定字符对字符串进行分割，将分割后的某一段取出** 

```cpp
    QString str;
    QString csv = "forename,middlename,surname,phone";
    QString path = "/usr/local/bin/myapp"; // 第一个字段是空的
    QString::SectionFlag flag = QString::SectionSkipEmpty;
    //以“,”为分隔符，将从左往右的第2段返回
    str = csv.section(',', 2, 2);   // str == "surname"
    qDebug() <<str;
    //以“/”为分隔符，将从左往右的第3段到第4段返回
    str = path.section('/', 3, 4);  // str == "bin/myapp"
    qDebug() <<str;
    //以“/”为分隔符，将从左往右的第3段(忽略前面的空字段)
    str = path.section('/', 3, 3, flag); // str == "myapp"
    qDebug() <<str;
    //前面符号表示从右往左计数
    str = csv.section(',', -3, -2);  // str == "middlename,surname"
    str = path.section('/', -1); // str == "myapp"
    //这里除了可以以单个字符','为分割符外，还可以以"**"字符串为分割符号，可自行实验
```

**17.以某个字符切割字符串**

```c++
QString str = "a,b,c,d"
QStringList strList = str.split(',');
//strList内保存为a b c d四个字符
```

**18.数字转字符串**

```cpp
    QString str;
    //10代表以10进制转换成字符串（允许范围2~36）
    str.setNum(1234,10);       // str == "1234"
    //'g'代表精度
    str.setNum(12.34,'g',10);       // str == "12.34"
```

**19.格式化转化为字符串**

```cpp
    QString str;
    int value=1998;
    str.sprintf("value=%d",value);
    qDebug() <<str; //str="value=1998"
```

**20.判断字符串str是否以字符串str2开头**

```cpp
    QString str = "Bananas";
    str.startsWith("Ban",Qt::CaseSensitive);         // returns true
    str.startsWith("ban",Qt::CaseInsensitive);       // returns true
    str.startsWith("Car",Qt::CaseSensitive);         // returns false
```

**21.字符串str转化为数值**

```cpp
    //example 1      QString >> double
    bool d;
    d = QString( "1234,56" ).toDouble(&ok); // ok == false
    d = QString( "1234.56" ).toDouble(&ok); // ok == true, d == 1234.56
    //exapmle 2      QString >> folat
    QString str1 = "1234.56";
    str1.toFloat();             // returns 1234.56
    bool ok;
    QString str2 = "R2D2";
    str2.toFloat(&ok);          // returns 0.0, sets ok to false
    //exapmle 3     QString >> int 
    QString str = "FF";
    bool ok;
    //16表示十六进制
    int hex = str.toInt(&ok, 16);       // hex == 255, ok == true
    //10表示十进制 转换失败
    int dec = str.toInt(&ok, 10);       // dec == 0, ok == false
	QString str = "0xFF";
    bool ok;
    int hex = str.toInt(&ok);       // hex == 255, ok == true
```

**22.将字符串str中所有的字母转化为小写字母或大写字母**

```cpp
    //example 1   大写转小写
    QString str = "The Qt PROJECT";
    str = str.toLower();        // str == "the qt project"
    //example 2   小写转大写
    QString str = "TeXt";
    str = str.toUpper();        // str == "TEXT"
```

**23.从指定位置截断字符串str**

```cpp
    QString str = "Vladivostok";
    str.truncate(4);    // str == "Vlad"
```



**24.QString支持的操作符号有：**

> 用于字符串之间比较："!=" "<" "<=" "==" ">="
>
> 用于字符串之间传递："+=" "="

### QByteArray常见用法

**1、构造函数**

```c++
//
QByteArray ba("123.txt");
const char * p = ba.constData();
//
const char * p = "123.txt";
QString s = QString(p);
QByteArray ba = s.toUtf8();
//
const char * p = "123.txt";
QByteArray ba(QByteArray::fromRawData(p, 7));
//包括 \0 也会输入到数据内
const char * p = "123.txt\0123.txt\0";
QByteArray ba(QByteArray::fromRawData(p, 16));
```

**2、数据操作**

```c++
// 在尾部追加数据
// 其他重载的同名函数可参考Qt帮助文档, 此处略
QByteArray &QByteArray::append(const QByteArray &ba);
void QByteArray::push_back(const QByteArray &other);

// 头部添加数据
// 其他重载的同名函数可参考Qt帮助文档, 此处略
QByteArray &QByteArray::prepend(const QByteArray &ba);
void QByteArray::push_front(const QByteArray &other);

// 插入数据, 将ba插入到数组第 i 个字节的位置(从0开始)
// 其他重载的同名函数可参考Qt帮助文档, 此处略
QByteArray &QByteArray::insert(int i, const QByteArray &ba);

// 删除数据
// 从大字符串中删除len个字符, 从第pos个字符的位置开始删除
QByteArray &QByteArray::remove(int pos, int len);
// 从字符数组的尾部删除 n 个字节
void QByteArray::chop(int n);
// 从字节数组的 pos 位置将数组截断 (前边部分留下, 后边部分被删除)
void QByteArray::truncate(int pos);
// 将对象中的数据清空, 使其为null
void QByteArray::clear();

// 字符串替换
// 将字节数组中的 子字符串 before 替换为 after
// 其他重载的同名函数可参考Qt帮助文档, 此处略
QByteArray &QByteArray::replace(const QByteArray &before, const QByteArray &after);
```

**3、子字符串查找和判断**

```c++
// 判断字节数组中是否包含子字符串 ba, 包含返回true, 否则返回false
bool QByteArray::contains(const QByteArray &ba) const;
bool QByteArray::contains(const char *ba) const;
// 判断字节数组中是否包含子字符 ch, 包含返回true, 否则返回false
bool QByteArray::contains(char ch) const;

// 判断字节数组是否以字符串 ba 开始, 是返回true, 不是返回false
bool QByteArray::startsWith(const QByteArray &ba) const;
bool QByteArray::startsWith(const char *ba) const;
// 判断字节数组是否以字符 ch 开始, 是返回true, 不是返回false
bool QByteArray::startsWith(char ch) const;

// 判断字节数组是否以字符串 ba 结尾, 是返回true, 不是返回false
bool QByteArray::endsWith(const QByteArray &ba) const;
bool QByteArray::endsWith(const char *ba) const;
// 判断字节数组是否以字符 ch 结尾, 是返回true, 不是返回false
bool QByteArray::endsWith(char ch) const;
```

**4、格式转换**



### Qt之QCharts类

.pro文件

```
QT += charts
```

头文件

```c++
#include <QtCharts>
QT_CHARTS_USE_NAMESPACE
```

### Qt之自动添加编译时间

.pro文件

```
OBJECTS_DIR = /home/zhangya/qt_project/build-ui_test_01-Desktop_Qt_5_14_2_GCC_64bit-Debug
#删除文件
defineTest(delFile) {
    file = $$1
    # 替换Windows源路径中的斜杠
    win32:file ~= s,/,\\,g
    # 删除命令
    QMAKE_POST_LINK += $$QMAKE_DEL_FILE $$shell_quote($$file)  $$escape_expand(\\n\\t)
    export(QMAKE_POST_LINK)
}
#删除文件，实现每次编译都会更新编译时间
delFile($$OBJECTS_DIR/systemsetwidget.o)

message($$OBJECTS_DIR)
```

CPP文件

```c++
//获取编译的日期+时间
QString getBulidDateTime()
{
    QString dateTime;
    dateTime += __DATE__;  //  
    dateTime += __TIME__;  //   
    dateTime.replace("  "," ");
    return QLocale(QLocale::English).toDateTime(dateTime, "MMM dd yyyyhh:mm:ss").toString("yyyy-MM-dd hh:mm:ss"); //转换成日期类型
}
```

### QTabWidget 隐藏Tab 或 显示Tab

tabwidgetplus.h

```c++
#ifndef __GUI_TABWIDGETPLUS_H__
#define __GUI_TABWIDGETPLUS_H__
 
#include <qtabwidget.h>
/*********************************************************************************
  *Copyright(C),MountCloud.org
  *FileName: tabwidgetplus.h
  *Author:  MountCloud
  *Version:  1.0
  *Date:  2021-07-30
  *Description:  //简易的显示和隐藏tab。适用于非动态改变tab数量，如果需要动态改变tab数量需要自行根据业务调用reloadPlusTabs()。
  *				//reloadPlusTabs在设计上是私有的的，所以自行重写add或者insert方法来更新plusTabs，我只是满足我非动态更新tab数量的需求。
**********************************************************************************/
class TabWidgetPlusTab {
public:
	QString text;
	QIcon icon;
	QWidget* widget;
	bool status = true;
};
class TabWidgetPlus : public QTabWidget{
	Q_OBJECT
public:
	TabWidgetPlus(QWidget* parent = nullptr);
	~TabWidgetPlus();
	//隐藏tab
	void hideTab(int index);
	//显示tab
	void showTab(int index);
private:
	//映射的tabs
	QList<TabWidgetPlusTab> plusTabs;
	//刷新映射列表
	void reloadPlusTabs();
	//返回真实的index
	int getRealIndex(int index);
};
 
#endif
```

tabwidgetplus.cpp

```c++
#include "tabwidgetplus.h"
#include <qtabbar.h>
TabWidgetPlus::TabWidgetPlus(QWidget* parent) : QTabWidget(parent) {
 
}
 
TabWidgetPlus::~TabWidgetPlus() {
}
 
void TabWidgetPlus::showTab(int index) {
 
	if (this->plusTabs.size() == 0) {
		this->reloadPlusTabs();
	}
	if (index > this->plusTabs.size() - 1) {
		return;
	}
	if (!plusTabs[index].status) {
		//获取当前真正要处理的index
		int realIndex = getRealIndex(index);
		if (realIndex == -1) {
			return;
		}
		//添加tab
		TabWidgetPlusTab tempPt = plusTabs[index];
		QString text = tempPt.text;
		QIcon icon = tempPt.icon;
		QWidget* tabWidget = tempPt.widget;
		insertTab(realIndex, tabWidget, icon, text);
		//设置映射的状态
		(&plusTabs[index])->status = true;
	}
}
 
void TabWidgetPlus::hideTab(int index) {
	if (this->plusTabs.size() == 0) {
		this->reloadPlusTabs();
	}
	if (index > this->plusTabs.size() - 1) {
		return;
	}
	if (plusTabs[index].status) {
		//获取当前真正要处理的index
		int realIndex = getRealIndex(index);
		if (realIndex == -1) {
			return;
		}
		//删掉tab
		this->removeTab(realIndex);
		//设置映射的状态
		(&plusTabs[index])->status = false;
	}
}
 
void TabWidgetPlus::reloadPlusTabs() {
	if(this->plusTabs.size()>0){
		this->plusTabs.clear();
	}
	int tabcount = this->count();
	if (tabcount>0) {
		for (int i = 0; i < tabcount; i++) {
			TabWidgetPlusTab tempPt;
			QWidget* tabWidget = this->widget(i);
			QString text =this->tabBar()->tabText(i);
			QIcon icon = this->tabBar()->tabIcon(i);
			tempPt.text = text;
			tempPt.icon = icon;
			tempPt.widget = tabWidget;
			tempPt.status = true;
			this->plusTabs.append(tempPt);
		}
	}
}
 
int TabWidgetPlus::getRealIndex(int index) {
	if (this->plusTabs.size() == 0) {
		reloadPlusTabs();
	}
	if (index == 0) {
		return 0;
	}
	if (index > this->plusTabs.size() - 1) {
		return -1;
	}
	int realIndex = 0;
	for (int i = 0; i < index; i++) {
		TabWidgetPlusTab twpt = plusTabs[i];
		if (twpt.status) {
			realIndex = realIndex + 1;
		}
	}
	return realIndex;
}
```

### qt生成二维码

#### 链接

```shell
https://github.com/nayuki/QR-Code-generator/
https://www.nayuki.io/page/qr-code-generator-library
```

### 配置系统托盘

```c++
m_hideIcon = new QSystemTrayIcon(this);  //隐藏时任务栏图标
m_hideIcon->setIcon(QIcon(":/new/prefix1/image/logo.png"));
m_menu = new QMenu(this);
m_quit = new QAction(this);
m_showWindow = new QAction(this);
m_showWindow->setText(tr("show"));
m_quit->setText(tr("quit"));
m_menu->addAction(m_showWindow);
m_menu->addAction(m_quit);
m_hideIcon->setContextMenu(m_menu);  //添加右键菜单
m_hideIcon->show();
connect(m_showWindow, &QAction::triggered, this, &Dialog::show);
connect(m_quit, &QAction::triggered, this, [this](){
        m_hideIcon->hide();
        qApp->quit();
    });
connect(m_hideIcon, &QSystemTrayIcon::activated, this, [this](QSystemTrayIcon::ActivationReason reason){
    switch (reason) {
    case QSystemTrayIcon::Trigger:
#ifdef Q_OS_WIN32
        this->show();
#endif
        break;
    default:
        break;
    }
})
```

