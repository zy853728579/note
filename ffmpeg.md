

### 网络链接

```shell
https://blog.csdn.net/zhangpengzp/article/details/88632455
https://blog.csdn.net/qq_43627907/article/details/128184141
https://blog.csdn.net/wang_chao118/article/details/143233340
https://www.jianshu.com/p/a70afe5bf229   #FFmpeg采集设备(dshow, vfwcap等)
https://www.cnblogs.com/IntelligencePointer/p/17299040.html
https://www.cnblogs.com/feiyangqingyun/p/18288208
https://blog.csdn.net/weixin_44520287/article/details/119801852  #ffmpeg对av_dict_set参数设置解析
https://blog.csdn.net/qq_25704799/article/details/138134846 #Qt+ffmpeg不错硬件编解码应用
https://gitee.com/wu-yue-0924/wyshared #这个原代码
https://github.com/EZreal-zhangxing/rk_ffmpeg  #rk3588+ffmpeg+硬编解码
```

### 学习工具

```shell
#文件信息查看 mediainfo
#封装格式：Elecard Format Analyer
#编解码数据: Elecard Stream Eye
#视频  YUV player
#音频  Adobe Audition
#协议查看数据和跨平台数据  Vlc 和 ffplay
```

### ffmpge库下载说明

```shell
#下载说明
https://ffmpeg.org/download.html #ffmpeg下载
https://github.com/BtbN/FFmpeg-Builds/releases  #所有下载版本
      #window下载名称: ffmpeg-master-latest-win64-gpl-shared.zip
      #ubuntu下载名称: ffmpeg-master-latest-linux64-gpl-shared.tar.xz

#windows配置说明
	#将 ffmpeg-master-latest-win64-gpl-shared.zip 文件解压到 E:\c_project\ffmpeg
	#在windows的环境变量->用户变量->Path->添加路径: E:\c_project\ffmpeg\bin ; 这个目录有对应的动态库文件

#.pro文件添加如下代码
FFMPEG_PATH = E:/c_project/ffmpeg
INCLUDEPATH += $${FFMPEG_PATH}/include
LIBS += -L$${FFMPEG_PATH}/lib -lavcodec -lavdevice -lavfilter -lavformat -lavutil -lpostproc -lswresample -lswscale

#头文件添加如下代码
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavdevice/avdevice.h"
}

#ubuntu添加动态库路径
	#方法一：临时添加库目录
	export LD_LIBRARY_PATH=<your-lib-path>:$LD_LIBRARY_PATH
	#例如
	export LD_LIBRARY_PATH=/home/zhangya/software/ubuntu_app/ffmpeg/ffmpeg-master-latest-linux64-gpl-shared/lib:$LD_LIBRARY_PATH
	#查看LD_LIBRARY_PATH是否设置成功
	echo $LD_LIBRARY_PATH
	#使配置生效
	source ~/.bashrc

	#方法二：永久添加库目录,将库目录添加到/etc/ld.so.conf.d/文件中
	cd /etc/ld.so.conf.d/
	sudo vim zhangya_libc.conf  #添加库目录路径：/home/zhangya/software/ubuntu_app/ffmpeg/ffmpeg-master-latest-linux64-gpl-shared/lib  保存
	#运行
	sudo ldconfig
```

### ffmpeg基本命令

```shell
ffmpeg -version  #查看ffmpeg版本
ffmpeg -hwaccels #查看ffmpeg支持硬件编解码
ffmpeg -codecs | findstr "h264"  #windows下查看可是用的对应格式解码器
ffmpeg -codecs | grep h264       #linux下查看可是用的对应格式解码器

#ffmpeg 硬编解码命令
cd ~/Videos
ffmpeg -y -hwaccel qsv -c:v h264_qsv -i ./VIDEO1.mp4 -q:v 30 -c:v h264_qsv 2.mp4

#ffmpeg 软编解码命令
ffmpeg -rtsp_transport tcp -i rtsp://169.254.36.1:1254 -vf zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p -c:v libx265 -crf 22 -preset medium -tune fastdecode -t 90 video-output.mp4

ffmpeg -rtsp_transport tcp -i rtsp://169.254.36.1:1254 -c:v copy output.mp4

#根据您的 硬件平台，选择适合的命令：
	#Intel CPU + QSV：使用 -c:v hevc_qsv
	#NVIDIA GPU + CUDA/NVDEC：使用 -c:v hevc_cuvid
	#AMD GPU + VAAPI：使用 -c:v hevc_vaapi
	#不重新编码，直接存储流：使用 -c:v copy






#测试命令，下面命令未成功
#4K无线连接   Endovision_xxx  12345678	rtsp://169.254.36.1:1254
#ENDOVISION_F6C2FF  endovision88   rtsp://192.168.99.1/media/stream2
ffmpeg -rtsp_transport tcp -hwaccel qsv -c:v hevc_qsv -i rtsp://169.254.36.1:1254 -c:v h264_qsv output.mp4

ffmpeg -loglevel debug -rtsp_transport tcp -hwaccel qsv -c:v hevc_qsv -i rtsp://169.254.36.1:1254 -an -pix_fmt yuv420p output.mp4

ffmpeg -loglevel debug -rtsp_transport tcp -hwaccel qsv -c:v hevc_qsv -i rtsp://169.254.36.1:1254 -pix_fmt yuv420p output.mp4

ffprobe -rtsp_transport tcp rtsp://169.254.36.1:1254

ffmpeg -rtsp_transport udp -hwaccel qsv -vcodec hevc_qsv -i rtsp://192.168.99.1/media/stream2 output.yuv

ffmpeg -hwaccel qsv -c:v hevc_qsv -rtsp_transport tcp -i rtsp://169.254.36.1:1254 -vf "format=nv12,hwdownload" -pix_fmt yuv420p -an -f mp4 output.mp4

ffmpeg -hwaccel qsv -c:v h264_qsv -rtsp_transport udp -i rtsp://192.168.99.1/media/stream2 -vf "format=nv12,hwdownload" -pix_fmt yuv420p -an -f mp4 output.mp4

ffmpeg -hwaccel vaapi -hwaccel_device /dev/dri/renderD128 -c:v h264_vaapi -rtsp_transport tcp -i rtsp://192.168.99.1/media/stream2 -vf "format=nv12|vaapi,hwdownload,format=yuv420p" -an -f mp4 output.mp4

ffmpeg -hwaccel qsv -hwaccel_output_format qsv -rtsp_transport udp -c:v h264_qsv -i rtsp://192.168.99.1/media/stream2 -c:v copy output.mp4

ffmpeg -rtsp_transport tcp -hwaccel qsv -hwaccel_output_format qsv -fflags nobuffer -c:v hevc_qsv -i rtsp://169.254.36.1:1254 -c:v rawvideo -pix_fmt yuv420p output.yuv

```

### ffmpeg函数说明

```c++
avcodec_version(); //查看版本号
avcodec_configuration();  //查看ffmpeg有哪些配置
```

#### 查看采集设备

```shell
#Windows采集设备的主要方式是dshow、vfwcap、gdigrab，其中dshow可以用来抓取摄像头、采集卡、麦克风等，vfwcap主要用来采集摄像头类设备，gdigrab则是抓取Windows窗口程序
ffmpeg -hide_banner -devices
	# -hide_banner  : 隐藏ffmpeg的功能状态(使能或者非使能)
```

#### 查看摄像头参数

```shell
# dshow 为采集设备: https://www.jianshu.com/p/a70afe5bf229,参考
#windows查看命令 "FHD USB Camera",摄像头名称
	ffmpeg -list_options true -f dshow -i video="FHD USB Camera"
#linux查看命令
	ffmpeg -list_formats all -i /dev/video0
#或者
	ffplay -f video4linux2 -list_formats all /dev/video0

```

## windows下ffmpeg命令

dshow查询所有采集音视频设备

```shell
ffmpeg -list_devices true -f dshow -i dummy #查看电脑有哪些音视频设备
#可通过  计算机管理 --> 设备管理器， 查看有哪些设备

#qt自带的接口查看所有摄像头
	#.pro文件
	QT += multimedia
	#include <QCameraInfo>
	void getCameraName(void){
		QList<QCameraInfo> cameraInfoList = QCameraInfo::availableCameras();
		foreach (auto camera, cameraInfoList){
			QString cameraName = camera.description();
			qDebug()<<"camera name = "<<cameraName;
		}
	}
```

![image-20241227212647058](E:\DJYL_pro\note\ffmpeg\查询本机音视频设备.png)

打开摄像头

```shell
ffplay -f dshow -i video="Integrated Webcam"  #可直接打开摄像头画面
```

### qt for ffmpeg

##### 查看所有设备名称

```c++
	const AVInputFormat * inFmt = nullptr;
#if defined (Q_OS_WIN)
    inFmt = av_find_input_format("dshow");
#elif defined (Q_OS_LINUX)
    inFmt = av_find_input_format("video4linux2");
#elif defined (Q_OS_MAC)
    inFmt = av_find_input_format("avfoundation");
#endif
    if (inFmt == nullptr){
        av_log(nullptr, AV_LOG_ERROR, "Font input format failed\n");
        return false;
    }

    AVDeviceInfoList * devList = nullptr;
    avdevice_list_input_sources(inFmt, nullptr, nullptr, &devList);
    if (devList != nullptr){
        qDebug()<<"device num = "<<QString::number(devList->nb_devices);
        for (int i=0; i<devList->nb_devices; i++){
            AVDeviceInfo * devInfo = devList->devices[i];
            enum AVMediaType type = *devInfo->media_types;
            qDebug()<<"type = "<<type<<"name = "<<devInfo->device_description;
        }
    }
	avdevice_free_list_devices(&devList);
```
