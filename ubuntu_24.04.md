### 网文链接

```shell

```

### 命令说明

##### 基础命令

```shell
lsb_release -a   #查看系统版本
uname -a   #查看系统信息
#查看CPU型号， 或者使用lscpu命令
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
#查看操作系统内核信息
uname -a
#查看操作系统发行版本信息
cat /etc/issue
#查看物理CPU数据
cat /proc/cpuinfo | grep physical | uniq -c

```

##### wifi命令

```shell
nmcli dev wifi		#查看wifi
sudo nmcli dev wifi connect zhangya<网络名称> password 871991<密码>  #连接wifi

#wifi名称密码
2K无线：ENDOVISION_F6C2FF  endovision88   rtsp://192.168.99.1/media/stream2
sudo nmcli dev wifi connect "ENDOVISION_F6C2FF" password "endovision88"

4K无线: Endovision_xxx  12345678	rtsp://169.254.36.1:1254

```

### 防火墙

```shell
sudo ufw status   #查看防火墙状态
sudo ufw enable   #开启防火墙
sudo ufw disable  #关闭防火墙
sudo ufw reload		#重新加载防火墙规则
sudo ufw delete allow 21  #删除防火墙规则
#永久关闭防火墙
sudo systemctl stop ufw #停止防火墙服务
sudo systemctl disable ufw #禁用防火墙服务自动启动
#添加防火墙规则
sudo ufw allow 80/tcp   #添加80端口,http
sudo ufw allow 433/tcp	#https
sudo ufw allow 22/tcp   #ssh
```

